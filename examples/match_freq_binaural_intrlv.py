#!/usr/bin/env python3

"""Measurement of binaural frequency matching as a function of reference frequency
   and ear side using an interleaved experiment paradigm
    
    Copyright (C) 2023 by Martin Hansen, Jade Hochschule, Oldenburg
    Author :  Martin Hansen <martin.hansen AT jade-hs.de>
    Date   :  30 Aug 2023

    This file is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation; either version 2 of the License, 
    or (at your option) any later version.
    See the GNU General Public License for more details:
    http://www.gnu.org/licenses/gpl
"""

from pypercept.experiments import AdaptMatchingExperiment
from pypercept.order import Interleaved
#from pypercept.order import Sequential
from pypercept.utils import gensin, hanwin
import numpy as np
import pypercept

class MatchFreqBinauralIntrlv(AdaptMatchingExperiment, Interleaved):
#class MatchFreqBinauralIntrlv(AdaptMatchingExperiment, Sequential):
    
    def init_experiment(self, exp):
        exp.description = """
        This experiment measures binaural pitch-matching for
        sinusoids as a function of frequency and ear side.
        """
        exp.set_variable('rel_freq_incr', -8, 'cent', 'relative frequency increment of test tone')
        exp.add_parameter('ref_freq',  [ 250, 1000, 4000], 'Hz', 'reference frequency')
        exp.add_parameter('ear_side', [ 1, 2], '1=l,2=r', 'side of test ear')
        exp.add_adapt_setting('1up1down', max_reversals=8, start_step=16, min_step=2)
        
        exp.ref_position = 1

        exp.task = ''   # the task gets set in init_run()

        exp.ui_blink_label = ['1', '2']
        exp.ui_response_label = ['up', 'down']
        exp.user_answers = 'ud'

        exp.debug = False
        exp.debug_plot_signal = False
        exp.feedback = True

        exp.sample_rate = 48000


    def init_run(self, run):
            
        run.tone_dur   = 0.4    # in s
        tone_level     = -20    # in dB FS
        run.tone_ampl  = np.sqrt(2) * 10**(tone_level/20)
        run.ramp_nsamp = round(0.05 * run.sample_rate)

        run.tone1 = gensin(run.ref_freq , run.tone_ampl, run.tone_dur, run.sample_rate)
        run.tone1 = hanwin(run.tone1, run.ramp_nsamp)

        isi_len = 0.25           # Inter stimulus interval length
        run.isi_signal = isi_len
        run.post_signal = isi_len
        run.pre_signal = isi_len
        
        if run.ear_side == 1:
            run.task = 'For equal pitch, the left tone needs to go ... (d)down | (u)p ? '
        elif run.ear_side == 2:
            run.task = 'For equal pitch, the right tone needs to go ... (d)down | (u)p ? '
        else:
            print('wrong value of run.ear_side: ' + str(run.earside))


    def init_trial(self, trial, run):

        test_freq = run.ref_freq * 2**(trial.variable/1200)
        tone2 = gensin(test_freq , run.tone_ampl, run.tone_dur, run.sample_rate)
        tone2 = hanwin(tone2, run.ramp_nsamp)

        if run.ear_side == 1:
            trial.test_signal      = np.column_stack((tone2, 0*tone2))
            trial.reference_signal = np.column_stack((0*run.tone1, run.tone1))
        elif run.ear_side == 2:
            trial.test_signal      = np.column_stack((0*tone2, tone2))        
            trial.reference_signal = np.column_stack((run.tone1, 0*run.tone1))
        else:
            print('wrong value of run.ear_side: ' + str(run.earside))


if __name__ == '__main__':
    pypercept.start(MatchFreqBinauralIntrlv)
