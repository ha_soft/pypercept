"""This file is part of pypercept:
a package for developing psychoacoustical listening experiments.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
"""

"""This module provides functions often used for psychoacoustical modeling.
"""

import numpy as np
from scipy.signal import lfilter, butter
from pypercept.utils import gammatone_crit_filter, gammatone_crit_filterbank


def modfilterbank(x, fs, fc, Q_mfb = 2.0):
    """modfilterbank applies a modulation filterbank to the input signals in x
    which are sampled with a frequency of fs Hz. Each column/channel in x is
    assumed to have passed a bandpass filter with a center frequency as stored
    in vector fc, and subsequently to have undergone some envelope-extraction,
    e.g. like through adaptation loops.

    The modulation filters have center frequencies 0, 5, 10, 16.66, 27.77, ...
    where each next center frequency is 5/3 times the previous if the filters
    have constant relative bandwidth with Q=2. For center frequencies below 10
    Hz, the absolute bandwidths of the filters will be constant at 5 Hz.
    For modulation frequencies below (and including) 10 Hz, the real value of
    the complex filter output signals are returned, and for higher modulation
    center frequencies, the absolute value of the complex filter output
    (i.e. the envelope) is returned.
    
    The upper modulation center frequency umf for the modulation filterbank(s)
    applied to any channel in x is determined by the corresponding frequency in
    fc by using umf = minimum(0.25*fc, 1000).
 
    Parameters
    ----------
    x : array_like
        The input signal(s) which can be a vector or a matrix. The input signal(s)
        are assumed to have envelope-like properties which result from envelope
        extraction after a band pass filtering from a filter bank which models
        the auditory peripheral filtering.  In case of a matrix, time runs
        along axis 0, and (auditory) center frequency runs along axis 1.
    fs : float
        Sampling frequency of the input signal, in Hz.
    fc : array_like
        Vector of center frequencies, in Hz, of (auditory) band pass filters of
        the signals in x. The value(s) in fc will determine the upper
        modulation center frequency of the separate filterbank(s).
    Q_mfb : float, optional
        The constant quality factor of the modulation band pass filters above
        10 Hz center frequency. This value also determines the higher center
        frequencies, as adjacent filter shapes are chosen to overlap at their
        -3dB points.  Default: 2.

    Returns
    ----------
    y : array_like
        Output signals after modulation filter bank processing.  The highest
        modulation center frequency depends on the audio center frequency of
        the signal.  For every signal in x, the modulation filtered versions
        are arranged along the highest (new) axis number.  
    mfc : array_like
        Vector of modulation center frequencies, in Hz.  

    References
    ----------
    %     T. Dau, B. Kollmeier, and A. Kohlrausch. Modeling auditory processing
    %     of amplitude modulation. I. Detection and masking with narrow-band
    %     carriers. J. Acoust. Soc. Am., 102:2892--2905, 1997a.
    %     
    %     R. Fassel and D. Pueschel. Modulation detection and masking using
    %     deterministic and random maskers. Contributions to Psychological
    %     Acoustics, edited by A. Schick (Universitaetsgesellschaft Oldenburg,
    %     Oldenburg), pages 419--429, 1993.
    %     
    %     M. Jepsen, S. Ewert, and T. Dau. A computational model of human
    %     auditory signal processing and perception. J. Acoust. Soc. Am.,
    %     124(1):422--438, 2008.
    %     
    %     A. Kohlrausch, R. Fassel, and D. Torsten. The influence of carrier
    %     level and frequency on modulation and beat-detection thresholds for
    %     sinusoidal carriers. J. Acoust. Soc. Am., 108:723--734, 2000.
    %     
    %     J. Verhey, T. Dau, and B. Kollmeier. Within-channel cues in
    %     comodulation masking release (cmr): experiments and model predictions
    %     using a modulation-filterbank model. J. Acoust. Soc. Am.,
    %     106:2733--2745, 1999.
    
    Original authors: See below in copyright information.
    
    Author of porting to Python within pypercept:  Martin Hansen, Dec 2023.

    """

    """This function is based on the Matlab file "modfilterbank.m" as found in
    the Auditory Model Toolbox (AMT), which are accompanied by the following
    
    Copyright information --------------------
        %   Url: http://amtoolbox.org/amt-1.5.0/doc/common/modfilterbank.php
        %
        %   #Author: Stephan Ewert (1999-2004) and Morten L. Jepsen: Original version
        %   #Author: Peter L. Søndergaard (2009-2013): adapted to AMT
        %   #Author: Piotr Majdak (2013-): adapted to AMT 1.0
        %   #Author: Alejandro Osses (2020): extended with LP_150_Hz
        %
        % This file is licensed unter the GNU General Public License (GPL) either 
        % version 3 of the license, or any later version as published by the Free Software 
        % Foundation. Details of the GPLv3 can be found in the AMT directory "licences" and 
        % at <https://www.gnu.org/licenses/gpl-3.0.html>. 
        % You can redistribute this file and/or modify it under the terms of the GPLv3. 
        % This file is distributed without any warranty; without even the implied warranty 
        % of merchantability or fitness for a particular purpose. 
    End of Copyright information --------------------
    """

    flag_LP_150_Hz = True
    flag_mfc_upper_limit = True
    flag_att_factor = True
    flag_phase_insense_hilbert = True
    
    Q = Q_mfb   # The constant quality factor for all center frequencies >= 10 Hz
    bw_mfb = 5  # Default constant bandwidth for all center frequencies < 10 Hz

    # The maximum modulation center frequency
    mfc_upper_limit_max = 1000

    
    # The ratio of consecutive center frequencies for which their transfer
    # functions overlap at their -3 dB points:
    ex = (1+1/(2*Q))/(1-1/(2*Q))
    
    # The lowest modulation filter is a 2nd order Butterworth lowpass with
    # cut-off frequency 2.5 Hz.  This corresponds to a band pass filter with
    # center frequency 0 Hz and bandwidth 5 Hz.
    b_lowpass,a_lowpass = butter(2, 2.5/(fs/2), btype='lowpass')

    if flag_LP_150_Hz == True:
        # Additional 150 Hz low pass filter to model the data in Kohlrausch et al. (2000)
        b_lp_150_Hz,a_lp_150_Hz = butter(1, 150/(fs/2))

    if flag_mfc_upper_limit:
        # The upper modulation center frequency depends on individual audio center frequency
        umf = np.minimum(0.25*fc, mfc_upper_limit_max)
    else:
        # The upper modulation center frequency is constant at its maximum value
        # for all audio center frequencies
        umf = 0*fc + mfc_upper_limit_max

    if flag_att_factor:
        # The attenuation factor according to Jepsen et al. (2008), page 426
        att_factor = 1/np.sqrt(2)
    else:
        att_factor = 1

    
    if len(x.shape) == 1:
        # Convert a 1-D array input signal into a one-column (Nx1) matrix
        x = x.reshape(len(x), 1)

    outsig_list = []
    mfc_list = []
    for freqchannel in range(len(fc)):

        if flag_LP_150_Hz == True:
            # Damp the highest modulation frequencies above 150 Hz
            outtmp = lfilter(b_lp_150_Hz, a_lp_150_Hz, x[:,freqchannel])
        else:
            outtmp = x[:,freqchannel]

        if umf[freqchannel] == 0:
            # ---------- Apply only a modulation low pass ----------
            outsigblock = lfilter(b_lowpass, a_lowpass, outtmp)
            mfc = 0
        else:
            # ---------- Apply a modulation filter bank ----------
            # #### The following code deviates from the code of the AMT version: ####
            # The three lowest modulation center frequencies with constant absolute spacing
            mfc1 = np.array([0, 5, 10])
            
            # The number of modulation center frequencies above 10 Hz and below
            # the upper modulation frequency, with constant relative spacing
            n_mfc = int(np.log(umf[freqchannel]/10)/np.log(ex))
            # The higher modulation center frequencies above 10 Hz themselves
            mfc2 = 10 * ex**(1+np.arange(n_mfc))
            mfc = np.concatenate((mfc1, mfc2))
            # #### End of deviation from the code of the AMT version ####

            # Allocate a complex valued output block
            outsigblock = np.zeros((x.shape[0], len(mfc))) + 0j
            
            # The modulation low pass:
            outsigblock[:,0] = lfilter(b_lowpass, a_lowpass, outtmp)

            # The higher modulation band pass filters
            for nmfc in range(1, len(mfc)):
                w0 = 2*np.pi*mfc[nmfc]/fs
                if mfc[nmfc] < 10:
                    b3,a3 = _efilt(w0, 2*np.pi*bw_mfb/fs)
                else:
                    b3,a3 = _efilt(w0, w0/Q)

                # ------------ filtering including post-processing --------------------
                if flag_phase_insense_hilbert:
                    # Apply an emphasis of 6 dB 
                    outsigblock[:,nmfc] = 2 * lfilter(b3, a3, outtmp) # 6 dB emphasis 
                    # After filtering, the phase information for
                    # modulation filters with mfc below or equal to 10 Hz is
                    # kept, and above 10 Hz the envelope is calculated:
                    if mfc[nmfc] <= 10:
                        outsigblock[:,nmfc] = np.real(outsigblock[:,nmfc])
                    else:
                        outsigblock[:,nmfc] = att_factor * np.abs(outsigblock[:,nmfc])
                else:
                    # Do the filtering without an emphasis and leave the output complex valued
                    outsigblock[:,nmfc] = lfilter(b3, a3, outtmp)  # no emphasis 


        outsig_list.append(outsigblock)
        mfc_list.append(mfc)

    # Note that the two lists outsig_list and mfc_list cannot, in general, be
    # transformed into a matrix-shaped array, because their different list
    # entries can have different lengths.  This is due to the different center
    # frequencies of the input signals which determine the upper modulation
    # frequencies for each signal.

    return outsig_list, mfc_list


def modulation_lowpass(x, fs, tau=0.02, axis=0):
    """Apply a 1st order lowpass filter with given exponential time constant.

    A 'modulation lowpass' was used in the version of the Dau et al.
    (1996) model, prior to the use of a modulation filter bank as in
    1997, to filter/smooth the output of the nonlinear adaptation loops.

    Parameters
    ----------
    x : array_like
        The input signal.  This can be, e.g., the output of adaptloop.
        In case of a matrix, time (and filtering) runs along the given
        axis, see below.
    fs : float
        Sampling frequency of the input signal, in Hz.
    tau : float
        The exponential time constant of the low pass filter, in s.
        Default: 20 ms.
    axis : int (optional)
        The axis along which to filter.  Default: 0.

    Returns
    ----------
    y : array_like
        The filtered output signal, with the same shape as x.

    Author:  MH, Jan 2024.
    """

    # Calculate the filter coefficients for a simple 1st order lowpass
    # with 1 pole only according to y[n] = b0 * x[n] + a1 * y[n-1]
    a1 = np.exp(-1.0/(tau*fs))
    b0 = 1 - a1

    # The arrays of the filter coefficients
    a = np.array([1, -a1])
    b = np.array([b0, 0])  # Note the second coefficient '0'. It is without function but is needed for lfilter

    y = lfilter(b, a, x, axis=axis)

    return y


def _efilt(w0, bw):
    """efilt calculates the filter coefficients for a frequency shifted first order
    lowpass.  For center frequencies w0 > 0, the filter coefficients are
    complex valued, and the complex valued output signal can be viewed as the
    analytical signal.  

    This function is based on the subfunction "efilt" within the Matlab file
    "modfilterbank.m" as found in the Auditory Model Toolbox (AMT).

    """

    """Original authors: See above in the copyright information for modfilterbank.m
 
    Author of porting to Python within pypercept:  Martin Hansen, Dec 2023.
    """

    # Calculate the filter coefficients for a simple 1st order lowpass
    # with 1 pole only according to y[n] = b0 * x[n] + a1 * y[n-1]
    e0 = np.exp(-bw/2)

    b = [1-e0, 0]   # Note the second coefficient '0'. It is without function but is needed for lfilter 
    a = [1, -e0*np.exp(w0*1j)]

    return (b,a)


def adaptloop(x, fs, limit=10, minspl=0, tau=[0.005, 0.050, 0.129, 0.253, 0.500], version_flag=''):
    """adaptloop applies a non-linear adaptation to the
    input signal x sampled at a sampling frequency of fs Hz. 
    limit (in arbitrary units) is used to limit the overshoot of the output.
    minspl determines the lowest audible SPL of the signal (in dB).
    tau is a vector with time constants involved in the adaptation loops. 
    The number of adaptation loops is determined by the length of tau.

    Parameters
    ----------
    x : array_like
        Input signal.
    fs : float
        Sampling frequency of the input signal, in Hz.
    limit : float, optional
        The limit (in arbitrary units) used to limit the overshoot of the output.
        Default: 10.
    minspl : float, optional
        The lowest audible SPL of the signal (in dB).
        Default: 0.
    tau = array_like, optional
        A vector with time constants involved in the adaptation loops. 
        The number of adaptation loops is determined by the length of tau.
        Default: [0.005, 0.050, 0.129, 0.253, 0.500
    version_flag : str
        A string specifying a certain version of the adaption loops.
        If this is set, then any values given to the other optional parameters
        (limit, minspl, and tau) will be ignored resp. overwritten (!).
        Possible values are:
        'adt_dau1997':  This consists of 5 adaptation loops with the default values
                        for tau, the default overshoot limit of 10 and the default
                        minimum SPL of 0 dB.  The adaptation loops have an exponential
                        delay.
        'adt_dau1996':  This is as in adt_dau1997 but without any overshoot
                        limiting.
        'adt_puschel1988':  This consists of 5 adaptation loops without overshoot
                            limiting. The adaptation loops have a linear spacing.
        'adt_breebaart2001':  As 'adt_puschel1998'
        'adt_relanoiborra2019':  As 'adt_dau1997' but with minspl of -34 dB.
        'adt_osses2021':  As 'adt_dau1997' but with limit of 5
        Default: ''.  
    
    Returns
    ----------
    y : array_like
        Output signal after nonlinear adaption.
    
    References
    ----------
    %     H. Relaño-Iborra, J. Zaar, and T. Dau. A speech-based computational
    %     auditory signal processing and perception model. J. Acoust. Soc. Am.,
    %     146(5), 2019.
    %     
    %     J. Breebaart, S. van de Par, and A. Kohlrausch. Binaural processing
    %     model based on contralateral inhibition. I. Model structure. J. Acoust.
    %     Soc. Am., 110:1074--1088, August 2001.
    %     
    %     T. Dau, D. Pueschel, and A. Kohlrausch. A quantitative model of the
    %     effective signal processing in the auditory system. I. Model structure.
    %     J. Acoust. Soc. Am., 99(6):3615--3622, 1996a.
    %     
    %     T. Dau, B. Kollmeier, and A. Kohlrausch. Modeling auditory processing
    %     of amplitude modulation. I. Detection and masking with narrow-band
    %     carriers. J. Acoust. Soc. Am., 102:2892--2905, 1997a.
    %     
    %     T. Dau, B. Kollmeier, and A. Kohlrausch. Modeling auditory processing
    %     of amplitude modulation. II. Spectral and temporal integration. J.
    %     Acoust. Soc. Am., 102:2906--2919, 1997b.
    %     
    %     D. Pueschel. Prinzipien der zeitlichen Analyse beim Hören. PhD thesis,
    %     Universitaet Goettingen, 1988.
    %     
    %
    %   Url: http://amtoolbox.org/amt-1.5.0/doc/common/adaptloop.php
    
    Original authors: See below in copyright information.
    
    Author of porting to Python within pypercept:  Martin Hansen, Nov 2023.
    """
    
    """This function is based on the Matlab files "adaptloop.m", "arg_adaptloop.m",
    and "comp_adaptloop.m" as found in the Auditory Model Toolbox (AMT), which are
    accompanied by the following
    Copyright information --------------------
        %   Url: http://amtoolbox.org/amt-1.5.0/doc/common/adaptloop.php
        %
        %   #Author: Stephan Ewert (1999-2004) and Morten L. Jepsen: Original version
        %   #Author: Peter L. Søndergaard (2009-2013): adapted to AMT
        %   #Author: Piotr Majdak (2013-2021): adapted to AMT 1.0
        %   #Author: Alejandro Osses (2021): bug fixes for AMT 1.0.x
        %   #Author: Piotr Majdak (2021): adaptation for AMT 1.1
        %
        % This file is licensed unter the GNU General Public License (GPL) either 
        % version 3 of the license, or any later version as published by the Free Software 
        % Foundation. Details of the GPLv3 can be found in the AMT directory "licences" and 
        % at <https://www.gnu.org/licenses/gpl-3.0.html>. 
        % You can redistribute this file and/or modify it under the terms of the GPLv3. 
        % This file is distributed without any warranty; without even the implied warranty 
        % of merchantability or fitness for a particular purpose.

        %   Url: http://amtoolbox.org/amt-1.5.0/doc/mex/comp_adaptloop.php
        %
        %   #Author: Stephan Ewert (2004)
        %   #Author: Morten L. Jepsen (2004)
        %   #Author: Peter L. Søndergaard (2004)
        % (Same GNU GPLv3 as above)
    End of Copyright information --------------------
    """


    if version_flag == 'adt_dau1996':
        tau = [0.005, 0.050, 0.129, 0.253, 0.500]
        limit = 0
        minspl = 0
    if version_flag == 'adt_dau1997':
        tau = [0.005, 0.050, 0.129, 0.253, 0.500]
        limit = 10
        minspl = 0
    if version_flag == 'adt_pueschel1988' or version_flag == 'adt_breebaart2001':
        tau = np.linspace(0.005,0.5,5)
        limit = 0
        minspl = 0
    if version_flag == 'adt_relanoiborra2019':
        tau = [0.005, 0.050, 0.129, 0.253, 0.500]
        limit = 10
        minspl = 20*np.log10(2e-7) + 100
    if version_flag == 'adt_osses2021':
        tau = [0.005, 0.050, 0.129, 0.253, 0.500]
        limit = 5
        minspl = 0


    # Scale the minimal level to linear amplitude, using the convention resp.
    # calibration constant C = 100 dB (SPL) corresponding to a digital RMS of 1:
    minlvl_lin = 10**(-(100-minspl)/20)

    
    # -------- Computation ------------------

    # Determine the signal length and width
    # If the signal is a 1-column vector of length N, cast it into a Nx1 matrix:
    if len(x.shape) == 1:
        x = x.reshape(len(x), 1)
    siglen = x.shape[0]
    nsigs = (x.shape[1] if len(x.shape) > 1 else 1)

    # Determine the number of adaptation loops
    nloops=len(tau)

    # Calculate filter coefficients for the loops.

    # b0 from RC-lowpass recursion relation y(n)=b0*x(n)+a1*y(n-1)
    # a1 coefficient of the upper IIR-filter
    a1 = np.exp(-1.0/(np.array(tau)*fs))
    b0 = 1 - a1

    # To get a range from 0 to 100 model units
    corr = minlvl_lin**(1/(2**nloops))
    mult = 100/(1-corr)

    # Apply minimum linear amplitude to the input signal
    x = np.maximum(x, minlvl_lin)

    # Determine steady-state levels corresponding to minimum input amplitude.
    # The values are repeated to fit the number of input signals.
    state = minlvl_lin**(1/(2**(1+np.arange(nloops))))    

    # Back up the state value(s), because state is overwritten
    stateinit = state


    if limit <= 1: 
        # ---------- NO overshoot limitation --------------------

        for w in range(nsigs):
            # Reset the divisor states to the initial values
            state = stateinit

            for ii in range(siglen):
                tmp1 = x[ii,w]

                # Compute the adaptation loops
                for jj in range(nloops):
                    # Input sample tmp1 is divided by current divisor
                    tmp1 = tmp1/state[jj]
                    # The division stage output is low-pass filtered, ...
                    state[jj] = a1[jj]*state[jj] + b0[jj]*tmp1;
                    # ... and will then serve as the divisor for the next input sample.

                # store the result.
                x[ii,w] = tmp1
        # ------------------------------------------------------- 

    else:
        # ---------- Overshoot limitation --------------------
  
        # Max. possible output value
        maxvalue = (1 - state**2) * limit - 1
  
        # Factor in formula to speed it up 
        factor = maxvalue * 2
  
        # Exponential factor in output limiting function
        expfac = -2 / maxvalue
        offset = maxvalue - 1

        for w in range(nsigs):
            # Reset the divisor states to the initial values
            state = stateinit

            for ii in range(siglen):
                tmp1 = x[ii,w]

                # Compute the adaptation loops
                for jj in range(nloops):
                    # Input sample tmp1 is divided by current divisor
                    tmp1 = tmp1/state[jj]

                    if (tmp1 > 1):
                        # Limit the current division stage output
                        tmp1 = factor[jj]/(1+np.exp(expfac[jj]*(tmp1-1)))-offset[jj]

                    # The division stage output is low-pass filtered, ...
                    state[jj] = a1[jj]*state[jj] + b0[jj]*tmp1;
                    # ... and will then serve as the divisor for the next input sample.

                # store the result.
                x[ii,w] = tmp1
        # ------------------------------------------------------- 


    # Scale to model units
    x = (x-corr)*mult

    return x


def ihc_envelope(x, fs, cutoff_freq=1000, filter_order=1, axis=-1):
    """Calculate the envelope including phase locking due to IHC
    transduction.  This is done by half-wave rectification followed by
    low pass filtering, which is a common easy model of the signal
    transduction of the inner hair cells.

    Parameters
    ----------
    x : array_like
        Input signal.
    fs : float
        Sampling frequency of the input signal, in Hz.
    cutoff_freq : float
        The cutoff frequency of the lowpass filter, in Hz.
    filter_order : int
        The order of the lowpass filter.
    axis : int, optional
        The axis of the input signal along which to perform the filtering.
        Default=-1.

    Returns
    ----------
    y : array_like
        Output signal after IHC envelope calculation.

    References
    ----------
    T. Dau, D. Pueschel, and A. Kohlrausch. A quantitative model of
    the effective signal processing in the auditory system. I. Model
    structure.  J. Acoust. Soc. Am., 99(6):3615--3622, 1996a.

    Author:  MH, Nov 2023.
    """

    b,a = butter(filter_order, cutoff_freq*2/fs, btype='lowpass')
    x = np.maximum(x, 0)
    y = lfilter(b, a, x, axis=axis)

    return y


def internal_representation(x, fs, cf):
    """Calculate the internal representation of stimulus x.

    Parameters
    ----------
    x : array_like
        The input signal.
        This can be, e.g., the supra threshold test signal, or the current test signal,
        or the reference signal.
    fs : float
        Sampling frequency of the input signal, in Hz.
    cf : float, or list of float
        Auditory band center frequency/ies, in Hz.  If cf is a scalar, this is
        center frequency of the one peripheral auditory band pass filter.  If
        cf is a two-element list, this is the minimal and maximal auditory band
        center frequencies, in Hz, of the peripheral auditory band pass filters
        with a filter spacing of 1 filter/ERB.

    Returns
    ----------
    y : array_like
        The internal representation according to auditory model, i.e., after
        auditory band pass filtering, IHC transduction, adaptation, and
        modulation filtering.
    mfc : array_like
        Vector of modulation center frequencies, in Hz.

    Author:  MH, Dec 2023.
    """

    if not hasattr(cf, "__len__"):
        y,_ = gammatone_crit_filter(x, fs, centerfreq=cf)
        afc = cf
    elif len(cf) == 2:
        y,_,afc = gammatone_crit_filterbank(x, fs, min_centerfreq=cf[0], max_centerfreq=cf[1])
    else:
        print('error, wrong number of elements in cf')
        return

    # Take only the real part of the analytic output signal of the
    # gammatone filter(s) as the input for inner hair cell stage
    y = ihc_envelope(np.real(y), fs)

    y = adaptloop(y, fs)

    ##return y, -1  # use -1 as a flag for "no modulation filtering"

    # amplitude expansion (CASP)  
    ## y = y**2

    ## y, mfcs = modfilterbank(y, fs, afc)
    ## y, mfcs = modfilterbank(y, fs, np.array([0]))     # 0 -> only modulation lowpass

    ## return y, mfcs

    y = modulation_lowpass(y, fs)

    return y, 0     # use 0 as a flag for "only modulation lowpass


def optdet(cur_diff_repr, template):
    """Calculate the decision variable in optimal detection process

    Parameters
    ----------
    cur_diff_repr : array_like
        Difference of representation of current interval 
        and representation of reference interval
    template : array_like
        The "template", i.e. normalized difference of representations
        of supra-threshold interval and reference interval

    Returns
    ----------
    mue : float
        Cross correlation value between the two input vectors,
        considered the mean of a Gaussian random variable

    Original authors: See below in copyright information.
    
    Author of porting to Python within pypercept:  Martin Hansen, Jan 2024.
    """

    """This function is based on the Matlab file "optimaldetector.m" as found
    in the Auditory Model Toolbox (AMT), which is accompanied by the following

    Copyright information --------------------
        %   Url: http://amtoolbox.org/amt-1.5.0/doc/common/optimaldetector.php
        %
        %   #Author: Peter L. Soendergaard (2011)
        %
        % This file is licensed unter the GNU General Public License (GPL) either 
        % version 3 of the license, or any later version as published by the Free Software 
        % Foundation. Details of the GPLv3 can be found in the AMT directory "licences" and 
        % at <https://www.gnu.org/licenses/gpl-3.0.html>. 
        % You can redistribute this file and/or modify it under the terms of the GPLv3. 
        % This file is distributed without any warranty; without even the implied warranty 
        % of merchantability or fitness for a particular purpose. 
    End of Copyright information --------------------
    """
    
    # Calculate cross correlation (zero lag) and consider its result
    # the mean ("mue") of a Gaussian random variable, see Dau et al. (1996)
    corr_mue = cur_diff_repr * template

    # Use total number of elements in the representation for normalization 
    optfactor = np.sqrt(corr_mue.size)

    # Take the mean over all dimensions of the internal representation and
    # multiply by optimality factor
    mue = np.mean(corr_mue.flatten()) * optfactor

    return mue


# End of file:  model_utils.py

