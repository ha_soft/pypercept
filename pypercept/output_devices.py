import soundcard as sc

# Displays output devices detected by `soundcard`:
#  
# Set `device` to <ID> or <Name> in your experiment, 
# otherwise pypercept uses the system's default device. 

devices = sc.all_speakers()
default_device = sc.default_speaker()

print(f'Output devices detected by "soundcard": ')

for i, device in enumerate(devices):
    print(f'#{i:02}:\n \
    ID: {device.id}\n \
    Name: {device.name}\n \
    Channels: {device.channels} \n')

print(f'The default device is currently:\n     {default_device.name}') 
