"""This file is part of pypercept:
a package for developing psychoacoustical listening experiments.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
"""

"""This module implements several user interfaces."""


from pypercept.result import Result
import pypercept.server as svr
import pypercept.exception as expt
import matplotlib.pyplot as plt

from tornado.ioloop import IOLoop
import webbrowser as wb
import os
import os.path
from pypercept.save import json_export, plot_current_run
import numpy as np
import time


class Ui():

    def __init__(self, experiment, extern=False, debug=True, audio_flag=1):
        self.extern = extern
        self.debug = experiment.debug
        self.audio_flag = audio_flag
        self.start(experiment)

        
    def start(self, exp):
        raise NotImplementedError

    
class Tui(Ui):

    welcome_screen = """

    """

    def start(self, exp):
        message(self.welcome_screen)
        message("Your experiment: " + exp.cls + "\n")
        if exp.subject_name == '':
            exp.subject_name = input("To start, please type your name or abbreviation: ")
        message("\n" + exp.description + "\n")

        try:
            if exp.order_type == 'interleaved':
                if not self.confirm('Start of interleaved runs. Are you ready'):
                    raise StopIteration
        except StopIteration:
            message("Experiment quit.")
            return
        
        while True:
            try:
                run = exp.next_run()
                # is this a new run (trials attribute of run is empty)?
                if not len(run.trials):
                    # messages/prompts for new runs for sequential experiments only
                    if exp.order_type == 'sequential': 
                        time.sleep(1)
                        if exp.info:
                            message("\nNext run's parameter(s):")
                            for param, values in run._parameters.items():
                                message(f'{param}: {values["value"]} {values["unit"]}')
                        if not self.confirm('Are you ready for this run'):
                            exp.skip_run(run)
                            continue
                    run.started = time.strftime('%d-%b-%Y__%H:%M:%S')

            except StopIteration:
                message("Experiment finished.")
                if exp.debug:
                    try:
                        # Try this to detect whether we are in Spyder ...
                        get_ipython().__class__.__name__ == 'SpyderShell'
                        # When in Spyder, do nothing
                    except:
                        # ... or in a regular shell
                        message("Close figures to exit pypercept.")
                        plt.show(block=True)
                return
            
            trial, signals  = exp.next_trial(run)

            # Prepare/preprocess signals
            signals = exp.audio.preprocess_signals(signals)

            # Plot signals
            if exp.debug_plot_signal:
                self.plot_signal(signals, exp.sample_rate)

            # Present the signal acoustically
            exp.audio.present_signal(signals)

            while trial.user_response == None:
                
                # Get the user's/listener's response
                try:
                    user_response = self.get_user_response(run.task, exp.meta_answers, exp.user_answers)
                    if not user_response:
                        continue
                except expt.RunAbortException:
                    exp.skip_run(run)
                    break
                except expt.ExperimentAbortException:
                    message("\nQuit experiment.")
                    return
                except expt.ToggleDebugException:
                    if exp.allow_debug:
                        self.debug = not self.debug
                        state = "on" if self.debug else "off"
                        message("\nPlotting is '%s' now." % state)
                    else:
                        message("\nPlotting is disabled.")
                    continue
                    
                # Apparently we have an answer
                exp.eval_response(run, trial, user_response)

                # Feedback for matching experiments
                if exp.feedback and hasattr(exp, 'test_position'):
                    message(trial.user_response) 

                # Feedback for AFC-experiments
                elif exp.feedback and hasattr(exp, 'num_afc'):
                    if trial.result == Result.UNDECIDED:
                        message('undecided') 
                    elif trial.result == Result.CORRECT:
                        message('correct')
                    elif trial.result == Result.WRONG:
                        message('not correct')

                try:
                    exp.adapt(run)

                except expt.RunFinishedException:
                    plot_current_run(run)
                    message("\nRun completed.")
                    save_run(run)

                    # skip prompts if there isn't another run
                    try: 
                        exp.next_run()
                    except StopIteration:
                        message("Experiment finished.")
                        return

                    if self.confirm("\nContinue?"):
                        continue
                    else:
                        message("\nQuit experiment.")
                        return

                except expt.RunStartMeasurement:
                    message('Start of measurement phase')

                # Plot the current run's track
                if exp.allow_debug and self.debug :
                    plot_current_run(run)


    def get_user_response(self, task, meta_answers, user_answers):
        print ('\r')
        user_response = input(task)
        while user_response not in (meta_answers + user_answers):
            print ('\nInvalid answer. Please enter one of these characters: ' + user_answers)
            user_response = input(task)
        if user_response == 'n':
            if self.confirm("\nReally skip run?"):
                raise expt.RunAbortException
            else: 
                return None
        elif user_response == 'q':
            if self.confirm("\nReally quit experiment?"):
                raise expt.ExperimentAbortException
            else:
                return None
        elif user_response == 'g':
            raise expt.ToggleDebugException
        else:
            return user_response


    def confirm(self, quest):
        user_response = input(quest+" (y/n)")
        while user_response not in 'yn':
            print('\rInvalid answer. Please enter y or n. ')
            user_response = input(quest+" (y/n)")
        if user_response == "y":
            return True
        else:
            return False


    def plot_signal(self, signal, sample_rate):
        """Plot the time signal (debugging)"""

        data = np.concatenate(signal)

        time_vector = np.arange(0, len(data))/sample_rate
        
        plt.ion()
        f = plt.figure(112)
        f.clear()

        plt.plot(time_vector, data)
        plt.xlabel('Time in seconds')
        plt.ylabel('Amplitude')
        plt.title('Current trial')
        plt.draw()


class Gui(Ui):

    def start(self, exp):
        app = svr.PyperceptServer()
        cid = app.eh.add_existing_experiment(exp, self.debug, self.audio_flag)
        port = 8889
        app.listen(port)
        if not self.extern:
            wb.open('http://localhost:8889/select/?cid='+cid)
        IOLoop.instance().start()


class Model(Ui):

    def start(self, exp):

        message("This experiment: " + exp.cls + "\n")
        message("This model/subject name: " + exp.subject_name + "\n")

        while True:
            try:
                run = exp.next_run()
                # Is this a new run (trials attribute of run is empty)?
                if not len(run.trials):   
                    message("\nThis run's parameter(s):")
                    for param, values in run._parameters.items():
                        message('%s: %g %s ' % (param, values['value'], values['unit']))
                    run.started = time.strftime('%d-%b-%Y__%H:%M:%S')
                    
                    exp.model_init_run(run)

            except StopIteration:
                message("Experiment finished")
                if exp.debug:
                    try:
                        # Try this to detect whether we are in Spyder ...
                        get_ipython().__class__.__name__ == 'SpyderShell'
                        # When in Spyder, do nothing
                    except:
                        # ... or in a regular shell
                        message("Close figures to exit pypercept.")
                        plt.show(block=True)
                return

            # In a simulation, the "signal" will be just empty
            trial, signals = exp.next_trial(run)

            if exp.model_calculates_correct_wrong:
                # EITHER THIS WAY, where the model_simulation() must directly
                # deliver a trial.result being either Result.CORRECT or Result.WRONG
                trial.result = exp.model_simulation(run, trial)
                trial.user_response = 'simu'

                run.trials.append(trial)

            elif exp.model_calculates_user_response:
                # OR THIS WAY, where the model_simulation() must deliver a
                # model_response being an interval number between 1 and n_afc ...
                model_response = exp.model_simulation(run, trial)

                # ... which can then be evaluated just like the normal user_response:
                exp.eval_response(run, trial, model_response)

            else:
                print('model type not yet implemented')
                return

            # Feedback for AFC-experiments
            if exp.feedback and hasattr(exp, 'num_afc'):
                if trial.result == Result.UNDECIDED:
                    message('undecided') 
                elif trial.result == Result.CORRECT:
                    message('correct')
                elif trial.result == Result.WRONG:
                    message('not correct')

            try:
                exp.adapt(run)
                
            except expt.RunFinishedException:
                if exp.debug:
                    plot_current_run(run)
                message("\nRun completed.")
                save_run(run)
                
            except expt.RunStartMeasurement:
                if exp.feedback:
                    message('Start of measurement phase')

            # plot the current run's track
            if exp.allow_debug and self.debug :
                plot_current_run(run)



def save_run(run):
    path = json_export(run)
    message("Results saved in %s" % path)
    return path

def warning(msg):
    print("\nWARNING:", msg)

def message(msg):
    print(msg)


