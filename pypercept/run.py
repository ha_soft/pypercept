"""This file is part of pypercept:
a package for developing psychoacoustical listening experiments.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
"""

"""
This module contains all classes and methods related to a run.  The class
:class:`Run` implements the basic functionality of a pypercept run.
"""

import numpy as np
import pandas as pd

class Run():
    """This class contains all parameters, a list of trials and settings for a single run.
    
    Attributes
    ----------
    variable : dict
        needs to contain name (string), value, unit (string)
    adapt_settings : dict
    parameters : dict
        Containing name, value, unit and description.
    sample_rate : int
        Samplinge rate, in Hz
    calib : float
        Calibration constant
    finished : boolean
        Flag whether run is finished
    trials : list
        list of trials :class:`Trial`
    test_signal : numpy array
        Test signal
    reference_signal : numpy array, or list of numpy arrays 
        Reference signal, or list of different reference signals
    isi_signal : numpy array
        Signal between test and reference interval(s)
    post_signal : numpy array
        Signal after the last interval
    pre_signal : numpy array
        Signal before the first interval
    step : float
        Current step size (later changed by adapt methods)
    _save_names : dict
    skipped : boolean
    start_measurement_idx : int
    """
    def __init__(self, experiment, parameters, variable, adapt_settings,
                 reference_signal, pre_signal, isi_signal, post_signal,
                 background_signal, sample_rate, calib, task):
        """initialization.

        Parameters
        ----------    
        experiment : experiment
        parameters : dict
            containing name, value, unit and description.
        variable : dict
            needs to contain name (string), value, unit (string)
        adapt_settings : dict    
        reference_signal : mumpy array 
            reference signal
        pre_signal : numpy array
            pre signal
        isi_signal : numpy array
            signal between reference and test or test and test signal
        post_signal : numpy array
            post signal
        test_signal : numpy array
        sample_rate : int
        calib : float
        """
        self.exp = experiment
        self.variable = variable
        self.__dict__.update(adapt_settings)
        self._parameters = parameters
        # set parameters and values as class attributes for easy access:
        self.__dict__.update({p:v['value'] for p, v in parameters.items()})
        self.sample_rate = sample_rate
        self.calib = calib
        self.task = task
        self.reference_signal = reference_signal
        self.isi_signal = isi_signal
        self.post_signal = post_signal
        self.pre_signal = pre_signal
        self.background_signal = background_signal
        self.step = self.start_step
        self.trials = []
        self.test_signal = []
        self.finished = False
        self.skipped = False
        self.start_measurement_idx = None
        self.result = {}
        

    def get_param_string(self):
        """Returns string containing all parameter information
               
        Parameters
        ----------
        (None)
        
        Returns
        -------
        string : string
        """
        string = 'Parameter(s): '
        for param, values in self._parameters.items():
            string += f'{param}: {values["value"]} {values["unit"]}, '
        return string[:-2]
            

    def finalize_run(self):
        """Calculate the result of the run after it is finished.

        For a constant stimuli experiment, the estimated points on the
        psychometric function (variable, prob_correct) are calculated
        from all trials, including the standard error of the
        prob_correct as a measure of the associated uncertainty.

        For an adaptive experiment, the threshold (median and mean)
        and measures for its uncertainty (std, min, max) are
        calculated from all trials during the measurement phase.

        """

        trials_var = []
        trials_ans = []
        for trial in self.trials:
            trials_var.append(trial.variable)
            trials_ans.append(trial.result.name.lower())

        if self.type == 'ConstStim':
            trials_ans_num = [1 if answer == 'correct' else 0 for answer in trials_ans]
            # use pandas to calculate estimated probability correct
            est_prob = pd.DataFrame({'variable': trials_var, 'correct': trials_ans_num}).groupby('variable').mean()
            # estimated resulting "probablity correct" [0:1]
            est_prob_corr = est_prob['correct'].tolist()
            # stdandard error of "probability correct" assuming a binomial distribution
            std_err = np.sqrt(est_prob * (1-est_prob) / self.exp.num_presentations)
            # variable, sorted
            self.result['variable'] = est_prob.index.values.tolist()
            self.result['est_prob_corr'] = est_prob['correct'].tolist()
            self.result['std_err_prob_corr'] = std_err['correct'].tolist()

        else:
            measure = trials_var[self.start_measurement_idx:]
            self.result['measurementphase'] = measure
            self.result['med']  = np.median(measure)
            self.result['mean'] = np.mean(measure)
            self.result['std']  = np.std(measure, ddof=1)
            self.result['maxi'] = max(measure)
            self.result['mini'] = min(measure)

