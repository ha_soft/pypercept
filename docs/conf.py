# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os  # MH
# import sys
# sys.path.insert(0, os.path.abspath('.'))

# -- Project information -----------------------------------------------------

project = 'pypercept'
copyright = '2023, Martin Hansen, Sven Kissner'
author = 'Martin Hansen, Sven Kissner'
with open('../VERSION') as f: release = f.read().strip()


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for LaTeX output -------------------------------------------------

latex_elements = {
  'extraclassoptions': 'openany,oneside', 
  'papersize': 'a4paper',
  'fncychap': '',
  'preamble': '\setcounter{tocdepth}{2}' ,
  'preamble': '\setcounter{tocdepth}{2}'
}

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
#html_theme = 'alabaster'
#html_theme = 'classic'
#html_theme = 'sphinxdoc'
html_theme = 'nature'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

numfig = True

# to avoid a bug in figure numbering in included files, this is a workaround:
# see https://github.com/sphinx-doc/sphinx/issues/9779
exclude_patterns = ['philosophy.rst', 'own-experiment.rst', 'save-analyse-data.rst']

def setup(app):
    app.add_css_file('pypercept_theme.css')

