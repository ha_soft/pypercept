.. license.rst

License
=======


pypercept is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

pypercept is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A local copy of the license can be found in the file `GNU-GPL`
in the distribution of pypercept,
or at `<https://www.gnu.org/licenses/gpl-2.0.html>`_


.. End of file
