.. philosophy.rst

Philosophy and structure of pypercept
=====================================


Terminology and definitions
---------------------------

This section explains some terms and definitions used for pypercept.   
In addition, the principal course of psychoacoustical experiments realised
with pypercept is explained.  Some of the terms are illustrated in the
figure :numref:`example-run` below.

  
:interval, signal, stimulus:

  During an experiment, the human test subject is presented with one
  or more **stimuli**.  The individual stimuli are also called
  **intervals** or signals.  In an :math:`n`-AFC-experiment, :math:`n`
  intervals are presented in succession.  Most often, these signal
  intervals will be separated by short pauses (of silence).

  When using a GUI, it is possible to visually mark the individual
  intervals while they are being played back.

   
:trial:

  A **trial** is one presentation of the :math:`n` stimulus
  intervals that pertain to an :math:`n`-AFC experiment.  After each trial
  the subject is required to answer respectively respond to a
  question, for example one of the kind 'which interval contained the
  test signal?'.

   
:(stimulus) variable:

  The **variable** inside pypercept takes the
  role of that stimulus dimension/magnitude which is varied from trial
  to trial (for example adaptively, or using a method of constant
  stimuli) in order to find the subject's threshold.  The variable
  carries the reserved name ``trial.variable`` inside pypercept.

   
:(stimulus) step size:

  The **step size** is the magnitude by which the stimulus variable
  gets changed from trial to trial during an adaptive experiment.  It
  is measured in the same unit as the variable.  The step size is
  typically larger in the beginning of the experiment and is then
  progressively reduced until some minimal step size has been reached.
  The step size carries the reserved name ``step`` inside pypercept.
  The initial step size has the name ``start_step`` and the minimal
  step size has the name ``min_step``.

  The step size doesn't have a meaning and isn't used in the 'method
  of constant stimuli'. 

   
:run:

  A **run** is the succession of all trials that are necessary
  to reach and determine one threshold of the varying stimulus
  variable.

   
:(stimulus) parameter:

  The **parameter** inside pypercept has the
  meaning of the arbitrary (but then fixed) stimulus
  dimension/magnitude which is usually kept constant during one run
  until threshold has been established.  In the next run the stimulus
  parameter would then usually be set to another value and the
  corresponding threshold would again be established, and so on.  The
  parameter is defined by the experiment's method
  ``add_parameter(name,values,unit,description)``.
  The ``name`` of the parameter will later be used to refer to the
  value of the parameter.  The ``values`` specify the different values
  that the parameter will have in the different runs of the
  experiment.  The ``unit`` and the ``description`` complete the
  information about the parameter.   

   
:2nd, 3rd, ... (stimulus) parameter:

  An experiment will often require more 
  than only one stimulus parameter.  pypercept offers the possibility to
  use and organise any number of independent parameters.  In that
  case, just call the method ``add_parameter()`` as many times as your
  experiment has parameters.  The experiment will then consist of as
  many runs as there are combinations of the different parameters'
  ``values``. 

      
:reference signal:

  In a trial of a :math:`n`-AFC experiment, :math:`n` intervals are
  presented in succession, and in random order.  There are :math:`n-1`
  intervals among them which contain the **reference signal**, i.e.
  they do *not* contain 'the signal' or the 'cue' that the subject is
  required to detect.  The reference signal must carry the reserved
  name ``run.reference_signal`` or ``trial.reference_signal``,
  depending on whether the reference signal stays the same during all
  trials or needs to be re-generated in every trial.  More details are
  explained in the section `Signal inheritance`_.  In the case that the
  :math:`n-1` need to be all different, e.g. when using running noise
  maskers, then  ``trial.reference_signal`` must be a ``list`` of
  signals.  

	
:test signal:

  The **test signal** is the signal which
  *does* contains that special 'cue' that the subject is
  required to detect, contrary to the reference signal which does not.
  The test signal will be presented at random in one of the :math:`n`
  intervals of a :math:`n`-AFC trial. The test signal must carry the
  reserved name ``run.test_signal`` or ``trial.test_signal``

   
:familiarisation phase:

  At the beginning of a new run with an adaptive control of the
  stimulus variable, the test signal will typically be presented
  clearly above threshold in order to orient the subject about the
  kind of the signal respectively the 'cue' that is to be detected,
  and to familiarise the subject with the measurement procedure.

  The initial step size, as specified by ``start_step``, will be larger
  at the beginning of this **familiarisation phase** so that correct
  responses of the subject will carry the stimulus variable towards
  the region of threshold more or less rapidly.  At the same time the
  step size will be reduced progressively until it has reached the
  minimal step size which the user has to specify.  The trials up to
  this point are only used for familiarisation of the subject, and to
  approach a value of the stimulus variable in the vicinity of the
  threshold.  The trials of the familiarisation phase will however
  *not* be included in the determination of the threshold.

  When using the 'method of constant stimuli', a familiarisation phase
  is currently not implemented. 

:measurement phase:

  In an adaptive run, the **measurement phase** starts at the end of
  the familiarisation phase.  The subject should have approached a
  point relatively close to the threshold and the stimulus variable
  changes now only with the minimal step size.  The measurement phase
  lasts until a certain number of reversals (see below) has been
  reached.  This maximum number of reversals serves as a stop
  criterion and needs to be specified by the user.  The threshold is
  then estimated based on all values that the variable had taken on
  during the measurement phase.

:reversal / turning point:

  In a transformed-up-down experiment [Levitt-1971]_, the stimulus
  variable and its direction of change depend on the subject's
  responses.  The direction alternates back and forth between 'going down'
  and 'going up', or possibly 'stay'.    

  pypercept counts both an upper point (from-up-to-down) and a lower
  point (from-down-to-up) on the track of the variable as 1 **reversal**.

  In other words, both the occurrence of a local
  maximum, and likewise of a local minimum, of the stimulus variable
  ``trial.variable`` as a function of the trial number is defined as a
  reversal.  This definition inside pypercept is in line with the
  definition of 'reversals' respective 'turning points' in the
  majority of the literature.
 
  The number of reversals serves as a stop criterion for an adaptive
  run.  When the number of reversals within the measurement phase has
  reached the **maximum number of reversals**, as specified by
  ``max_reversals``, that run is terminated.

  The function ``add_adapt_setting(adapt_method, max_reversals,
  start_step, min_step)`` is used to specify the parameters
  controlling the adaptive method.

:(user) response:

  The **response** is what the test subject answered after the
  stimulus presentation of a trial.  In a 3-AFC task, for example, the
  response might be '2' if the subject decided that she heard the test
  signal in interval number 2.  The response is stored in
  ``trial.user_response``

:result:

  The **result** needs to be defined separately from the user response,
  at least for some kind of experimental paradigms.  Take the example
  of a 3-AFC task, where the subject's response was '2'.  This response
  can then be evaluated into a **result** being either 'correct' (the
  test signal *was* in interval 2) or 'wrong'.  The result is stored in ``trial.result``.
  The result(s) of the recent trial(s) will control the value of the
  variable of the next trial in adaptive experiments.  
  Note that the result and the response can be (almost) the same in case of a
  matching experiment, where the user response might be 'd' to
  indicate the result of her decision that the reference 
  signal must go more 'down' in frequency in order to match the
  test signal.  


.. _example-run:
.. figure:: example_run.png

   Example of the course of one experimental run with pypercept, where
   the detection threshold of the variable modulation degree was
   measured as a function of the two parameters modulation frequency
   and carrier frequency.  The plus-signs mark a correct response of
   the subject, the minus-signs mark a wrong one.  The familiarisation
   phase (depicted with a dashed line) lasted from trial 1 up to
   trial 23.  It started with an initial step size of 8 dB.  At each
   upper reversal, the step size was halved (resulting in step sizes
   4, 2, 1) until the minimum step size of 1 dB had been reached. At
   that point, the measurement phase (depicted with a solid line)
   started at trial 24 and it lasted for 8 reversals (at trials 25,
   27, 30, 34, 36, 38, 41, and 43) because 8 had been set as the
   maximum number of reversals.  The last reversal of those 8 was
   reached in trial 43 at a value of ``trial.variable`` = -29 dB, and
   the run terminated there.  The next data point *would* have been
   presented in trial 44 at a modulation degree of -30 dB, hadn't the
   run stopped.



   

Types of experiments in pypercept
---------------------------------

|pyper| offers to perform various kinds of experiments.  One class of
experiments are detection and discrimination experiments using a
:math:`n`-AFC paradigm where the variable can be controlled using either
`adaptive transformed-up-down methods`_, or using the
`method of constant stimuli`_.
These experiments can then be performed as a sequence of single runs,
sequentially with one
run at a time, or as a group of interleaved runs.  These are described
in sections `n-AFC experiments`_ resp. `Interleaved tracks
experiments`_

Slightly different are matching experiments, like for example employed
in pitch matching or loudness matching.  The difference relative to
:math:`n`-AFC detection experiments is that the answer of the subject can
not be judged to be correct or wrong.  Matching experiments are
described in section `Matching experiments`_.  Matching experiments
may also be carried out in an interleaved-runs fashion.


n-AFC experiments
+++++++++++++++++

The class ``AdaptAFCExperiment`` in ``pypercept.experiments``
is a central part of |pyper| for controlling a
**n-AFC experiment**.  As a default it uses an adaptive control of the
stimulus variable, as described in ``Adaptive transformed-up-down
methods``_. It can be used in combination with the class
``Sequential`` oder ``Interleaved`` in ``pypercept.order`` which
manages an experiment with sequential or interleaved runs.

Use 'sequential' runs if you intend to measure one experimental run at
a time, where the next run starts only after the previous run has been
completed, i.e. threshold has been reached.  Otherwise use
'interleaved' runs, where several runs run concurrently in a randomly
interleaved fashion.

The experiment ``sam_sincarrier_detect.py`` in the subdirectory
``examples`` shows an example for an adaptive 1-up-2-down 3-AFC
experiment with sequential runs which measure the modulation detection
threshold for sinusoidal amplitude modulation of sinusoidal carriers.
Details are explained in section `How to write your own experiment`_.


Matching experiments
++++++++++++++++++++

The class ``AdaptMatchingExperiment`` in ``pypercept.experiments`` is a
central part of |pyper| for controlling **matching experiments** where two
signal intervals are presented per trial and the subject is asked to
adjust some stimulus dimension of one of them, with the aim to `match`
the two stimuli regarding a given perceptual aspect, for example match
the stimuli to have the same loudness, or to have half pitch, or twice
the sharpness, etc. etc.  

Also matching experiments can be performed with sequential or
interleaved runs.

The experiment ``match_freq_binaural.py`` in the subdirectory
``examples`` shows an example for a matching experiment with
interleaved runs where an interaural pitch match is measured for
sinusoids presented monaurally left respectively right.



Adaptive transformed-up-down methods
++++++++++++++++++++++++++++++++++++

|pyper| offers a number of adaptive methods to choose from.  The
purpose of these methods is to adaptively control the value of the
stimulus variable, depending on the subject's previous answer(s).  


The most well-known of these methods are probably the **transformed
up-down methods** [Levitt-1971]_, and the 1up-1down, 1up-2down,
2up-1down, and 1up-3down methods are readily available in |pyper|.  The method
is specified as in   ``exp.add_adapt_setting('1up2down')`` which
leads to the 1up-2down method.

The transformed up-down methods are typically accompanied by a
varying step size.  It is larger in the beginning and gets
progressively smaller during the experiment.  In |pyper| this is
implemented in the following way: The start step size and the minimal
step size are both specified by the user, using the same unit as the
experiment variable.  Then, every time the adaptive algorithm detects
an upper reversal in the current run, the step size is halved until
the minimal step size is reached.  The corresponding internal code to
achieve this is just:

.. code::

  self.step = max(self.min_step, self.step/2)


When the minimal step size has been reached, this is defined as the
beginning of the measurement phase.  The run will then continue
with an adaptive change of the variable using the minimal step size.
The user also specifies a maximum number of reversals.  When that
number of reversals has been reached during the measurement phase, this is
used as a stop criterion for the experiment.

The start step size, the minimal step size, and the maximum number of
reversals can be specified as arguments in the function call like in
this example:

.. code::

  exp.add_adapt_setting("1up2down", max_reversals=8, start_step=8, min_step=1)


Two other possible adaptive methods are ``'WUD'`` for the
*weighted-up-down* method [Kaernbach-1991]_, and ``'UWUD'`` for the
*unforced weighted up-down* method [Kaernbach-2001]_.  These two
methods offer the possibility to (more or less) freely select the
probability for correct responses towards which the adaptive method
shall converge.  This probability, e.g. 0.75, must be specified in
the variable ``pc_converge`` as in this example:

.. code::

  exp.add_adapt_setting('UWUD', max_reversals=6, start_step=5, min_step=1, pc_convergence=0.75)

Note, that a probability between 0 and 1 is expected, not a percentage
value between 0 and 100!  This variable is only used for the 'WUD' and
'UWUD' methods.  Also note in this example that the step size will
have the consecutive values of 5, 2.5, 1.25, 1

The two weighted-up-down methods employ the same algorithms for
changing the step size and for determining the beginning and the end
of the measurement phase as explained above.



When using any of the adaptive methods mentioned above, the common
behaviour is that wrong answers of the subject lead to an
*up*-ward change of the stimulus variable (thereby making the
task easier), while correct answers lead to a *down*-ward change
(making the task more difficult).  This behaviour can be changed by setting
the variable ``reversed_up_down=True``.
In section `Need to go up after correct responses?`_ this is
explained in more detail. 



.. [Levitt-1971] H. Levitt (1971), "Transformed up-down procedures in
		 psychoacoustics", JASA 49, p. 467-477.

.. [Kaernbach-1991] C. Kaernbach (1991), "Simple adaptive testing with the
		    weighted up-down method", Perception & Psychophysics, 
		    49, p. 227-229.

.. [Kaernbach-2001] C. Kaernbach (2001), "Adaptive threshold estimation 
		    with unforced-choice tasks", Perception & Psychophysics, 
		    63(8), p. 1377-1388. 



   
Method of constant stimuli
++++++++++++++++++++++++++

As an alternative to the adaptive control of the stimulus variable, as
explained in section `Adaptive transformed-up-down methods`_, pypercept also
offers the **method of constant stimuli**.  With this method, the stimuli
(respectively the values of the stimulus variable) that are going to
be presented are known/fixed prior to the test subject's arrival at the
test booth, hence the name "constant stimuli".  The class
``ConstAFCExperiment`` in ``pypercept.experiments``
implements the AFC paradigm for the stimulus presentation.  


A number of different relevant values of the stimulus variable needs
to be chosen by the experimenter, and must then be specified as in

.. code::

  exp.set_variable("freq_incr", [1, 2, 4, 8, 16], "Cent", "relative frequency increment")

The number of (repeated) presentations must be specified in the
variable ``num_presentations`` as in

.. code::

  exp.num_presentations = 10   # how often each variable value is presented

   
A total number of ``len(exp.variable['start_val']) *
exp.num_presentations``, in this case :math:`5 \cdot 10` stimuli will
then presented in a randomized/permuted order, with a new permutation
for each new experimental run and for each subject.

The file ``jnd_frequency.py`` in the subdirectory
``examples`` shows an example for such a constant stimuli
experiment where the just noticeable difference in frequency for two
sinusoidal stimuli is measured.  

Note that an experiment with an adaptive method (like for example
1-up-2-down) has an in-built familiarisation phase that takes place
prior to the collection of those data points from which the threshold
will be derived.  Such a familiarisation phase is not normally part of
a constant stimuli experiment, resp. this is not yet implemented in
|pyper| .  However, it is easy for you to implement some training
phase, for which the results shall be discarded: You could easily
execute the same constant stimuli experiment twice: first with a
temporary different subject name and with a smaller number of stimulus
repetitions ``exp.num_presentations``, and then in the second run with
the original subject name and number of repetitions.



Interleaved tracks experiments
++++++++++++++++++++++++++++++

|pyper| offers the feature to run an :math:`n`-AFC experiment or a
matching experiment with **interleaved tracks**, which means that several
experimental runs (usually with different sets of parameter values)
are running at the same time in a randomly interleaved fashion.  Each run is
controlled as if it was a single run (see section `n-AFC experiments`_) 
but the current trial is chosen randomly from all incomplete runs,
i.e. those runs which have not yet reached threshold.

The class ``Interleaved`` (as opposed to ``Sequential`` for
non-interleaved runs) is used for controlling such interleaved-tracks
adaptive transformed-up-down :math:`n`-AFC experiments.  It should be
unnecessary for the user to change this class.  The same requirements
and conventions apply as for sequential single-run experiments (see
section `n-AFC experiments`_).

The file ``tone_in_running_noise_intrlv.py`` in the subdirectory
``examples`` shows an example for a 1-up-2-down 3-AFC experiment with
interleaved tracks, which measures the detectability of a sinusoidal
tone in broad band running noise.  In this example, the threshold for
tones of different frequencies and for different overall noise levels
is measured in an interleaved-tracks fashion.

The file ``match_freq_binaural_intrlv.py`` in the same subdirectory
shows another example for an interleaved experiment, where a pitch
matching for sinusoids is measured binaurally between left resp. right
ear.

Note that interleaved-tracks experiments may require some extra amount
of testing and debugging, especially for the inexperienced.
For details see subsection `Setting up interleaved tracks experiment`_.  
Use the force -- read the source!

Experiments with interleaved tracks are not yet available for the
method of constant stimuli.  
