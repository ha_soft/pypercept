#!/usr/bin/env python3

"""Measurement of detection threshold level of a sinusoid in noise
   as a function of sinusoid frequency and noise level.

   This file must be called using the commandline argument '-u model'

 This file uses pypercept, a collection of functions for designing and
 controlling interactive psychoacoustical listening experiments.

 This file is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 2 of the License, 
 or (at your option) any later version.
 See the GNU General Public License for more details:
 http://www.gnu.org/licenses/gpl

"""

from pypercept.experiments import AFCSimulation
from pypercept.result import Result
from pypercept.analyse_data import AnalyseData
from pypercept.utils import rms, gammatone_crit_filter as gcf
import pypercept
import numpy as np

from tone_in_running_noise import ToneInRunningNoise

class ToneInRunningNoiseSim(AFCSimulation, ToneInRunningNoise):
    
    """This is a simple example to illustrate how to
    a) implement an easy auditory model with pypercept,
    b) re-use an already existing experiment for human subjects
       and to run the same experiment with an auditory model
       instead of a human listener.

    Have a look at the files experiments.py (for class AFCSimulation)
    and ui.py (for class Model) if you want to implemented more
    sophisticated models.

    Author: MH Jan 2024
    """

    def init_experiment(self, exp):
        exp.subject_name = 'sim_swamp_psm'
        super().init_experiment(exp)

        # Limit parameter values compared to original experiment, if needed
        exp.parameters['noise_level']['values'] =   [-40]
        exp.parameters['sine_freq']['values'] =   [250, 500, 1000, 5000]

        exp.debug = True


    def model_simulation(self, run, trial):

        # Apply a very easy "swamping" model for masking, as described in
        # Moore's "Introduction to the Psychology of Hearing", 6th edition,
        # chapter 6C "The Mechanism of Masking - Swamping or Suppression?"

        # Calculate the signal power at the output of the auditory filter
        # that is tuned to the test frequency.  The auditory filter is
        # modeled by a gammatone filter with a bandwidth equal to the
        # critical/auditory bandwidth, implemented in gcf(), which is tuned
        # to the test signal frequency:

        crit_filt_testsig,_ = gcf(trial.test_signal, run.sample_rate, run.sine_freq)
        crit_filt_refsig1,_ = gcf(trial.reference_signal[0], run.sample_rate, run.sine_freq)
        crit_filt_refsig2,_ = gcf(trial.reference_signal[1], run.sample_rate, run.sine_freq)

        level_cf_testsig = 20*np.log10(rms(np.real(crit_filt_testsig)))
        level_cf_refsig1 = 20*np.log10(rms(np.real(crit_filt_refsig1)))
        level_cf_refsig2 = 20*np.log10(rms(np.real(crit_filt_refsig2)))

        if self.debug:
            print(f'{level_cf_testsig=:5.1f}, {level_cf_refsig1=:5.1f}, {level_cf_refsig2=:5.1f}', end=' ')

        # The model decides to detect the test signal, when the signal+noise
        # interval is higher in level by 1.4 dB relative to the noise alone
        # interval(s), when the levels are measured at the output of the 
        # auditory filter.  The criterion of 1.4 dB results from the idea of the
        # Power Spectrum Model, saying:
        #       Level_signal_thres = Level_noise_filter - 4 dB
        # From this it follows, that at threshold 
        #    10*log10( RMS(noise + signal@threshold) / RMS(noise alone))
        #  = 10*log10( 1+10**(-4/10) ) = 1.4 dB

        delta_level_criterion = 1.4  # level difference at threshold, in dB

        if level_cf_testsig > max(level_cf_refsig1, level_cf_refsig2) + delta_level_criterion:
            return Result.CORRECT
        else: 
            return Result.WRONG



if __name__ == '__main__':
    pypercept.start(ToneInRunningNoiseSim)
    ad = AnalyseData()
    ad.load_data('sim_swamp_psm', 'ToneInRunningNoiseSim')
    ad.display_data(raw=True, perm_params=[2,1], fig_num=102)
