.. save-analyse-data.rst

Saving and analysing your experimental data
===========================================


How your data are saved
-----------------------

Each time when one run has been completed, i.e. a threshold has been
reached in an adaptive experiment or the block of trials in a constant
stimulus experiment has been finished, the result of that run is
automatically saved in a result file in a json format. The name of
that file is always formed by the common prefix ``pypercept_``
followed by the value of ``exp.subject_name``, as for example in
``pypercept_mh.json``.  Note that any whitespace will be removed from 
the subject name. Nevertheless, it is advised to avoid whitespace
characters in the subject name in any case. 

As the pypercept result-file is written in json format, it looks
rather human-readable.  However, note that the format depends
critically on correct placement of parentheses, string delimiters,
etc. Upon adding new result data to the file, it is re-read and its
content undergoes some slight string re-formatting for nicer visual
accessibility.  Should you ever want to manually edit/change the json file,
please be sure to make a backup of the file in advance to avoid a fatal
loss of data.  



Adaptive experiments
++++++++++++++++++++

The data from an adaptive experiment which are saved into the
pypercept result file can best be explained via an example entry. It
is taken from one of the example experiments in 
``examples/sam_sincarrier_detect.py``  which measures the
threshold of the modulation degree in dB for sinusoidal amplitude
modulation of a sinusoidal carrier:

.. code::

  {
    "Experiment": "SamSincarrierDetect",
    "Subject": "mh",
    "Timestamp (started)": "23-Aug-2023__12:34:58",
    "Timestamp (finished)": "23-Aug-2023__12:38:14",
    "NumChoices (AFC/AUC)": 3,
    "Adaptation": {
      "type": "1up2down",
      "max_reversals": 8,
      "start_step": 8,
      "min_step": 1,
      "reversed_up_down": false
    },
    "NumParameters": 2,
    "Parameter 1": {
      "Name": "mod_freq",
      "Value": 4,
      "Unit": "Hz"
    },
    "Parameter 2": {
      "Name": "car_freq",
      "Value": 800,
      "Unit": "Hz"
    },
    "Variable": {
      "Name": "m",
      "Unit": "dB"
    },
    "Threshold": {
      "Method": "all",
      "Median": -27.0,
      "Mean": -26.85185185185185,
      "Std. Deviation": 1.0267088458424103,
      "Maximum": -25.0,
      "Minimum": -29.0
    },
    "Trials": {
      "Variable": [-8, -8, -16, -16, -24, -24, -32, -24, -24, -28.0, -24.0, -24.0, -26.0, -26.0, -28.0, -28.0, -30.0, -28.0, -26.0, -26.0, -27.0, -27.0, -26.0, -26.0, -27.0, -27.0, -26.0, -26.0, -27.0, -27.0, -28.0, -28.0, -29.0, -29.0, -28.0, -27.0, -26.0, -26.0, -25.0, -25.0, -26.0, -26.0, -27.0, -27.0, -28.0, -27.0, -27.0      ],
      "Response": ["2", "3", "3", "2", "2", "3", "2", "1", "1", "3", "2", "3", "1", "3", "3", "2", "1", "3", "2", "1", "1", "1", "1", "1", "2", "1", "3", "2", "3", "2", "1", "3", "1", "2", "1", "1", "2", "3", "3", "1", "3", "3", "1", "1", "1", "3", "2"      ],
      "Result": ["correct", "correct", "correct", "correct", "correct", "correct", "wrong", "correct", "correct", "wrong", "correct", "correct", "correct", "correct", "correct", "correct", "wrong", "wrong", "correct", "correct", "correct", "wrong", "correct", "correct", "correct", "wrong", "correct", "correct", "correct", "correct", "correct", "correct", "correct", "wrong", "wrong", "wrong", "correct", "wrong", "correct", "correct", "correct", "correct", "correct", "correct", "wrong", "correct", "correct"      ]
    }
  },



As you can see, that measurement used the 3-AFC paradigm in
combination with the 1up-2down adaptive procedure with a start step
size, minimal step and maximum number of reversals as they were
specified in that experiment's method ``init_experiment()``.  The
experiment had two parameters which were set to a modulation frequency
of 4 Hz and a carrier frequency of 800 Hz in this run.  The run yielded a
threshold estimate of -27 dB resp. -26.85 dB for the median resp. the
mean across the measurement phase.  The standard deviation during the
measurement phase was around 1.03 dB and the minimum resp. maximum
value of the variable were -29 dB resp. -25 dB.

When calculating the mean, the median, and the standard deviation of
the "Threshold", the default behaviour of pypercept is to calculate
across the values of the variable of *all* trials during the
measurement phase.  It is possible to change this behaviour in your
experiment by setting the variable ``exp.threshold_reversal = True``.
In that case, the "Threshold" values will only be calculated across the
trials at the reversal points during the measurement phase.  The
method that was chosen is also output into the json file using the
name "Method" 



The end of the entry lists the values that the variable took during
all of the trials of this run, the "responses" (i.e. interval numbers)
that the subject gave in the those trials, and the corresponding
"results" (correct or wrong) that the responses were evaluated to.
The information in the field "Trials" might later be used for a
another analysis of the run.




Constant stimuli experiments
++++++++++++++++++++++++++++

In case of a constant stimuli experiment, one experimental run will
produce many similar entries in the result file, but will exhibit some
differences compared to an adaptive run.  It is also best explained
by an example:

.. code::

  {
    "Experiment": "JNDFrequency",
    "Subject": "mh",
    "Timestamp (started)": "18-Sep-2023__15:57:02",
    "Timestamp (finished)": "18-Sep-2023__16:00:07",
    "NumChoices (AFC/AUC)": 3,
    "Adaptation": "ConstStim",
    "NumParameters": 2,
    "Parameter 1": {
      "Name": "ref_freq",
      "Value": 1000,
      "Unit": "Hz"
    },
    "Parameter 2": {
      "Name": "tone_level",
      "Value": -55,
      "Unit": "dB FS"
    },
    "Variable": {
      "Name": "freq_incr",
      "Unit": "Cent"
    },
    "Results": {
      "Number of presentations": 10,
      "Variable": [4, 8, 16, 32      ],
      "Estim. prob_correct": [0.5, 0.7, 0.9, 1.0      ],
      "Std.Err. prob_correct": [0.15811388300841897, 0.14491376746189438, 0.09486832980505137, 0.0      ]
    },
    "Trials": {
      "Variable": [8, 4, 16, 16, 4, 16, 8, 32, 8, 16, 32, 4, 16, 32, 8, 32, 8, 16, 8, 32, 32, 32, 4, 8, 4, 4, 16, 16, 4, 4, 8, 32, 32, 4, 16, 4, 32, 8, 8, 16      ],
      "Response": ["3", "2", "2", "3", "2", "1", "1", "3", "2", "3", "1", "3", "3", "2", "1", "3", "2", "1", "1", "1", "1", "1", "2", "1", "3", "2", "3", "2", "1", "3", "1", "2", "1", "1", "2", "3", "3", "1", "3", "3"      ],
      "Result": ["correct", "correct", "correct", "correct", "correct", "correct", "correct", "correct", "wrong", "correct", "correct", "wrong", "correct", "correct", "correct", "correct", "correct", "correct", "wrong", "correct", "correct", "correct", "wrong", "correct", "correct", "correct", "correct", "wrong", "wrong", "wrong", "wrong", "correct", "correct", "wrong", "correct", "correct", "correct", "correct", "correct", "correct"      ]
    }
  }

The main difference compared to an adaptive run is the outcome, being
several points on the psychometric function for which the probability
of correct response (``prob_correct``) as a function of the stimulus'
(``Variable``) has been estimated from the answers of the subject.
The result file reports also the standard errors associated with these
probabilities. The rationale for it is explained in the section
`Data averaging for constant stimuli experiments`_ 


Processing and displaying your results
--------------------------------------

Your experimental data can be displayed and analysed using the class
``AnalyseData`` in ``anyalyse_data.py``. 

Calling the file directly on the command line requires the two
arguments 'subject name' and 'experiment name'. For an adaptive
experiment, it will then plot all threshold data points as a function
of the 1st parameter in a single plot.  If more than one parameter is
present in the experiment, each further parameter value (or
combination of further parameters) will result in a separate curve
with a corresponding label, as illustrated by the following figure for
the case of two parameters:

.. figure:: example_analyse_average.png

In case of a constant stimuli experiment, the probability for correct
answers will be plotted as a function of the variable values, and each
parameter resp. each combination of parameters will result in a
separate curve with a corresponding label.

For more control, the class ``AnalyseData`` can be used programatically:
First, get an instance of the class and load the desired data by passing
the subject name and experiment name to the method ``load_data()``,
like in

.. code::

  ad = AnalyseData()
  ad.load_data('cs', 'ModMaskPTC')   # load data for subject 'cs' in experiment 'ModMaskPTC'

At his point the attribute ``ad.data`` contains the requested experiment's
data as a pandas dataframe, a 2-dimensional data structure which can be thought
of as a spreadsheet. Each row of the dataframe holds the complete data of one
run as stored in the experiment's json-file, with columns that are labeled
accordingly. Jupyter Notebooks or Editors like VSCode come with an inline
browser for pandas dataframes and can be used to visualise and explore the
data structure.  

For data visualisation in a figure similar to the above one,
``display_data()`` can be employed as in

.. code::

  ad.display_data()


The function ``display_data()`` takes three parameters:

:``plot_style``:
  ``1``:  (default) Plot all data into a single figure.  A separate
  line is drawn for each unique (combination of)  parameter values
  when more than two parameters are present. 

  ``2``: Create a separate figure for each unique (combination of)
  parameter values when more than two parameters are present.

:``raw``:  
  ``False``:  (default) Average across all repetitions belonging to the same
  unique combination of parameter values before plotting.

  ``True``: Do not average data, i.e. all individual 'raw' data points
  from the individual runs are plotted.  

:``perm_params``:
  List contatining a permutation of the parameter numbers.  This
  allows to change the 'look' of your result plot when your experiment has
  more than one parameter: The result data (threshold resp. percent correct)
  will be plotted as a function of the first parameter along the
  x-axis.
  The default order of parameters is the order in which the parameters
  were defined in the experiment's setup.

Data averaging, happening as the default or when ``raw = False``, is
explained in section `Result data averaging`_.


Parameter permutation
+++++++++++++++++++++

The effect of a parameter permutation by specifying the argument
``perm_params`` is illustrated in the following two figures.  Consider an
experiment that measures the BMLD by measuring the level of the test
tone at threshold, expressed as the SNR of the tone relative to the
overall noise level.  The threshold is measured as a function of
masker center frequency, masker bandwidth, and interaural condition.
In the experiment definition you could write

.. code ::

  exp.add_parameter('masker_cf', [500, 4000], 'Hz',
                    'center frequency of the masker == test tone frequency')
  exp.add_parameter('masker_bw', [25, 100], 'Hz',
                    'masker bandwidth')
  exp.add_parameter('ia_cond', ['N0S0', 'N0Spi', 'NpiS0'], '',
                    'interaural condition.')
  exp.set_variable('SNR', 0, 'dB',
                   'signal-to-overall-noise ratio')

After gathering the experimental data with your subject, the following
code

.. code ::

  from pypercept.analyse_data import AnalyseData

  ad = AnalyseData()
  ad.load_data('ts', 'BMLD_vdPK_1999')  # load data for subject 'ts' in experiment 'BMLD_vdPK_1999'

  ad.display_data()                     # produce left figure with original parameter order
  ad.display_data(perm_params=[3,1,2])  # produce right figure with parameter order permuted

can produce these two figures:

.. figure:: example_param_permute.png

As you can see, the same six data points are displayed in both
figures.  The difference between them is the ordering of the three
parameters.  As described above, the first parameter will make up the
x-axis of the plot, and any further parameter(s) will produce separate
curves with corresponding labels.  In the left figure, "masker_cf"
appears as the x-axis, because it was defined as the first parameter of the
experiment.  In the right figure, using ``perm_params=[3,1,2]``, the
third parameter "ia_cond" has been turned into the first parameter to
appear on the x-axis.  Also, the original parameters 1 and 2 were
turned to be the 2nd and 3rd parameter now.  




Result data averaging
---------------------
  
When calling ``ad.display_data()`` with ``raw=False``, i.e. with
averaging across repeated measurements before plotting, the averaged
data is stored in a second dataframe called ``ad.data_processed``,
i.e. as an attribute of the class ``AnalyseData``.



Data averaging for adaptive experiments
+++++++++++++++++++++++++++++++++++++++

For adaptive experiments, the thresholds of all repeated measurements with the
same set of parameter values are averaged and their standard deviation
calculated. The respective columns of ``data_processed`` are Prefixed with
'Threshold.Avg.'. Note that ``data_processed`` still contains a row for each
run. Runs with the same set of parameter values hold identical values
for averaged threshold and standard deviation. 


Data averaging for constant stimuli experiments
+++++++++++++++++++++++++++++++++++++++++++++++

For constant stimuli experiments, averaging takes place across repeatedly measured
data (i.e. percentage correct values), individually per each (combination
of) different parameter value(s), and then plots the data.  Individual data
sets contain the "score" :math:`p`, i.e. the probability for a correct
answer, and the number :math:`N` of presentations from which the
probability was obtained.  When a score :math:`p` was measured repeatedly,
e.g. in repeated sessions on different days, the individual scores are taken
as independent measurements, and averaged using a weighting according to
the number of presentations: Let the score :math:`p_i` be based on
:math:`N_i` stimulus presentations in measurement number :math:`i`, with
:math:`i = 1,...,M`. The averaged score :math:`\bar{p}` is then calculated
as :math:`\bar{p} = \frac{\sum_i N_i \cdot p_i}{\sum_i N_i}`. That means
:math:`\bar{p}` is interpreted as being based on the total number of
:math:`N_\text{tot} = \sum_i N_i` stimulus presentations. A higher number
of presentations will lead to a better acuity of the estimate
:math:`\bar{p}`, e.g. as expressed by a smaller standard error. For a
constant stimuli experiment, ``display_data()`` will calculate both
:math:`\bar{p}` and also the standard error associated with
:math:`\bar{p}`, according to the following reasoning: Consider :math:`N`
stimulus presentations where the stimulus variable was held constant. For
each presentation, the subject's answer can either be correct or wrong and
it is assumed that the subject's performance is constant over time, which
means that the probability :math:`p` for a correct answer stays
constant. Let :math:`X` be the (random) number of correct responses, then
:math:`X` is distributed according to a Binomial or "Bernoulli" distribution
with parameters :math:`N` and :math:`p`: :math:`X \sim \text{Bi}(N , p)`,
with expectation value :math:`\text{E}(X) = Np` and variance
:math:`\text{Var}(X) = Np(1 - p)`. Value :math:`\bar{p}` is fixed but
unknown, and shall therefore be estimated. The estimator of :math:`\bar{p}`
is then :math:`\hat{p} = X / N`, which is the relative occurrence of
correct answers amoung all :math:`N` presentations. It can be shown that
:math:`\hat{p}` is an unbiased and consistent estimator for :math:`p`. Also
for repeated estimations of :math:`p`, the averaged score :math:`\bar{p}`
is identical to the optimal estimator :math:`\hat{p}` for the real (but
unknown) value of :math:`p`. The random variable :math:`\hat{p} = \bar{p}`
can be interpreted as the average of a total number of :math:`N_\text{tot}`
ones and zeros, with :math:`X` their sum. The expectation value of
:math:`\bar{p}` is then :math:`\text{E}(\hat{p}) = \text{E}(\bar{p}) = p`
and the associated standard error, i.e. the standard deviation of
:math:`\bar{p}`, is :math:`\sigma_{\bar{p}} = \sqrt{\text{Var}(X)/N^2} =
\sqrt{p(1 - p)/N}`.

The estimated probability (the "score") and its standard error will be stored in
``data_processed`` with the prefix 'Avg.' and separately for each parameter
combination and variable value. Note that this results in ``data_processed``
containing a row for each set, whereas in ``data``, each row contains a
complete run and holds all variable values and scores of that run.


Help with inspection of result data
-----------------------------------

After you have gathered result data from a number of subjects, and
possibly from several experiments, the content of your data files may
become overwhelming.  |pyper| has a small graphical tool called
"data_helper" that can be helpful in this case.  The tool is started on
the commandline by

.. code ::

  $ python data_helper.py

It will then find all files called "pypercept_*.json" in the *current
directory*.  The result gets displayed in the leftmost 'Listbox' of a
small GUI, with one line per json file.  After you select one file
resp. one test subject from the Listbox, that json file gets scanned
and all separate pypercept experiments contained in the file get
displayed in the middle Listbox of the GUI.  A click on one experiment
will display all available measurements that have been saved for the
chosen subject and experiment in the rightmost Listbox.  You can then
selected one or more of these measurements for further inspection.
The GUI might look like this:

.. figure:: pypercept_data_helper.png

The use of the buttons in the bottom part of the GUI should be
self-explaining: You can either plot *all* data points from the
experiment chosen in the middle Listbox, or you can look at a subset
of the data points which you *selected* in the rightmost Listbox.  You
can also display the trials that pertained to one run resp. one data
point.

The edit field in the bottom row can be used to enter a parameter
permutation that should be applied to the data before plotting, as
explained in section `Parameter permutation`_.  The info text next to
the edit field tells you about the expected format for this entry: use
only the digits of the indices, without any whitespaces, commas, etc.
As an example, use ``312`` to specify the index list ``[3, 1, 2]`` for
your permutation.  Leaving this field empty, which is the default,
will not permute the parameters.  

Plotting the trials of one run subsequently can be informative, for
example to identify a poor convergence of a run which can be a
reason for a large standard deviation connected with a threshold
value.  An example of such a plot is shown in the following figure.

.. figure:: example_run_trials.png


.. End of file
