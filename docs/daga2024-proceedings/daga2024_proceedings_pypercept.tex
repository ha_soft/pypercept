\documentclass[a4paper, 10pt, twocolumn]{article}
% Sowohl LaTeX als auch pdfLaTeX können benutzt werden, um das Manuskript zu erstellen.

% Bitte öffnen sie diese Datei mit utf8 Zeichenkodierung!!!
\usepackage[utf8]{inputenc}         % Schriftkodierung dieser Datei
%\usepackage[german]{babel}          % für deutsche Dokumente

\usepackage{graphicx}               % optional für Grafiken
\usepackage{tabularx}               % optional für Tabellen
\usepackage{multirow}               % optional für Tabellen
\usepackage{url}                % optional für Internet Links

\usepackage[small,bf]{caption2}     % bitte für Bildunterschriften verwenden
\usepackage{parskip}
\usepackage{titlesec}
%\usepackage{amsmath}                % optional für Formeln


\titleformat{\section}{\normalfont\large\bfseries}{\thesection}{}{}
\titleformat{\subsection}{\normalfont\large\bfseries}{\thesection}{}{}
\titleformat{\paragraph}{\normalfont\bfseries}{\theparagraph}{}{}
\titlespacing{\section}{0pt}{6pt}{-1pt}
\titlespacing{\subsection}{0pt}{3pt}{-1pt}
\titlespacing{\paragraph}{0pt}{3pt}{-1pt}

\newcolumntype{Y}{>{\centering\arraybackslash}X}    %für Tabellen mit tabularx

% Definition der Seitenränder
\addtolength{\textwidth}{2.1cm}
\addtolength{\topmargin}{-2.4cm}
\addtolength{\oddsidemargin}{-1.1 cm}
\addtolength{\textheight}{4.5cm}
\setlength{\columnsep}{0.7cm}

\pagestyle{empty}                   % weder Kopf- noch Fußzeile auf 1. Seite

\begin{document}

\date{}                                         % kein Datum auf 1. Seite

\title{\vspace{-8mm}\textbf{\large
    Pypercept: free Python software for psychoacoustical experiments
  %  in education and research
  }}

% Hier die Namen und Daten der beteiligten Autoren eintragen
\author{
Martin Hansen, Sven Kissner\\
\emph{\small Institut für Hörtechnik und Audiologie, Jade Hochschule, 26121 Oldenburg, Deutschland
}\\
\emph{\small Emails:  martin.hansen@jade-hs.de, sven.kissner@jade-hs.de } } 
\maketitle
\thispagestyle{empty}           % weder Kopf- noch Fußzeile auf Folgeseiten
% Beginn des eigentlichen Manuskripts
\section*{Introduction}
\label{sec:Einleitung}
Pypercept is a new Python package for developing and running psychoacoustical
experiments.
%
Pypercept has been developed for education and research at the Institut für
Hörtechnik und Audiologie (IHA) and is distributed as free open source software
\cite{pypercept_gitlab}.  One aim of pypercept has been to provide the students in
their psychoacoustics courses with a uniform and simple starting point for
designing their own listening experiments.
%
Pypercept offers a common framework for all generic parts of a psychoacoustical
experiment, like presentation of stimuli, analysis of the test subject's
response, control of the stimulus variable, saving the experimental data, etc.
The user of pypercept only needs to design the individual acoustical stimuli of
the experiment.  This helps students and researchers in that they need to care
less about computer programming and can focus more on psychoacoustics.
%
Due to the use of Python, pypercept runs on Linux, Mac, and Windows platforms,
and can easily be modified for one's own needs.  Pypercept offers
the possibility to run the same experiment with either human test subjects, or
with an auditory model hooked in which then replaces the test subject.

\paragraph*{History of pypercept}
\label{sec:history-pypercept}

Pypercept expanded on the results of a student programming project in 2015
named ``earyx'' \cite{earyx_github}.  Earyx was in turn inspired by the software ``psylab''
\cite{psylab_2006, psylab_github}, written in
Matlab, which has been developed at IHA since 2003.  Psylab has been used by more
than 450 students at IHA for designing individual listening experiments as part
of the assignments in their psychoacoustics courses.  Also their experience and
feedback has lead to the ongoing improvement of pypercept.

\paragraph*{License and availability}
\label{sec:license-availability}

Pypercept is distributed under the GNU General Public License.  It is
available as a git repository or zip~file %through a public server
at \url{https://gitlab.gwdg.de/ha_soft/pypercept/}
\cite{pypercept_gitlab}.


\section*{Features of pypercept}
\label{sec:features-pypercept}

Pypercept offers to perform various kinds of experiments. One class are 
detection and discrimination experiments using a $n$-AFC
paradigm where the variable can be controlled using either adaptive
methods, or the constant stimuli method.

Slightly different are matching experiments, like for example employed
in pitch matching or loudness matching.  The difference relative to
$n$-AFC detection experiments is that the answer of the subject can
not necessarily  be judged to be correct or wrong.  

Both kinds of experiments, detection and matching, can then be
performed for several runs, either sequentially with one run at a
time, or as a block of interleaved runs.

Pypercept offers a number of adaptive methods which control the value
of the stimulus variable depending on the subject's previous
answer(s). Examples are the ``transformed up-down methods''
\cite{levitt_1971} like the 1up-1down, 1up-2down, 2up-1down, and
1up-3down methods.  Also the ``weighted-up-down'' and the ``unforced
weighted-up-down'' \cite{kaernbach_2001} adaptive methods are
available in pypercept.

There are several options for running your experiments: Pypercept offers a
commandline/text based user interface (UI) or a browser based graphical UI
running on your local computer.  Alternatively pypercept can act as a server
that one or more remote clients within your own network can connect to through
a browser with a graphical UI.


\section*{Example experiments}
\label{sec:example-experiments}

One good way to learn how to use pypercept is to study the example experiments
that come with pypercept.  The following examples are part of the pypercept
distribution:
\begin{itemize}
\item Absolute detection threshold of sinusoids in quiet: 3-AFC with
  weighted-up-down method for sequential runs as a function of
  frequency
  %
\item Spectral masking, detection of sinusoid in a simultaneous broad band
  noise: 3-AFC with 1-up-2-down method for sequential runs as a function of
  sinusoid frequency and masker level
  %
\item Amplitude modulation detection for sinusoidal carrier: 3-AFC with
  1-up-2-down method for sequential runs as a function of modulation frequency
  and carrier frequency
  % 
\item Just noticeable difference for frequency of a sinusoid:  3-AFC  with
  constant-stimuli method for sequential runs as a function of reference
  frequency and level
  % 
\item Just noticeable difference for intensity of a sinusoid: 3-AFC with
  1-up-2-down method for sequential runs as a function of reference level
  and frequency
  %
\item Binaural pitch matching: 2-AFC frequency matching with 1-up-1-down method
  for an interleaved run as a function of reference frequency and ear side
\end{itemize}

\section*{Programming own experiments}
\label{sec:progr-own-exper}

Every pypercept experiment can be written as one single Python file.
After importing a few Python packages, the file needs to define a new
class and three functions that describe the individual experiment.  In
order to understand their role, it is necessary to give a short
introduction to the nomenclature/philosophy of pypercept.
%
\begin{description}
\item [Trial]
A ``trial'' is one presentation of the $n$ stimulus intervals
that pertain to an $n$-AFC experiment.  After each trial the subject
is required to respond to a question, for example
``which interval contained the test signal?''.
%
\item [Reference signal]
  In a trial of a $n$-AFC experiment, $n$ intervals are
  presented in succession, and in random order.  There are $n-1$
  intervals among them which contain the ``reference signal'', i.e.\
  they do \emph{not} contain 'the signal' or the 'cue' that the subject is
  required to detect.
%
\item [Test signal]   
  The ``test signal'' is the signal which
  \emph{does} contains that special 'cue' that the subject is
  required to detect, contrary to the reference signal which does not.
  The test signal will be presented at random in one of the $n$
  intervals of a $n$-AFC trial. 
%
\item [Variable]
The ``variable'' inside pypercept takes the role of that
stimulus dimension/magnitude of the test signal which is varied from trial to trial
(for example adaptively, or using a method of constant stimuli) in
order to find the subject's threshold.
%
\item [Run]
A ``run'' is the succession of all trials that are necessary to
reach and determine one threshold of the varying stimulus variable.
%
\item [Parameter]
  A ``parameter'' has the meaning of the arbitrary (but then fixed)
  stimulus dimension/magnitude which is usually kept constant during
  one run until threshold has been established.  In the next run the
  stimulus parameter would then usually be set to another value and
  the corresponding threshold would again be established, and so on.
  Pypercept offers the possibility to use and organise any number of
  independent parameters.
%
\item [Experiment]
  The ``experiment'' consist of as many runs as there
  are combinations of the different parameters' values.
\end{description}

To specify an experiment with pypercept, the user needs to define
three functions
% \verb+init_experiment()+, \verb+init_run()+, and
% \verb+init_trial()+,
which are methods of a class describing the
experiment.  The first method, \verb+init_experiment()+, will be
called once at the beginning of the experiment.  The purpose is to
define properties which are global to the whole experiment.  The
second, \verb+init_run()+, will be called once at the beginning of
every new run.  The purpose is to define properties which are
individual to a certain run.  The third method, \verb+init_trial()+, will be
called at the beginning of each new trial.  The purpose is to define
properties for the individual trial.  At the end of the function
\verb+init_trial()+ the test signal and the reference signals must
have been defined as (numpy) arrays, depending on the values of the
parameter(s) and the variable.

The procedure can best be explained with a short example experiment
which is also part of the distribution.  The following Python code in
the File \verb+tone_in_running_noise.py+  
defines the complete experiment:

\scriptsize
%\footnotesize
\begin{verbatim}
from pypercept.experiments import AFCExperiment
from pypercept.order import Sequential
from pypercept.utils import gensin, rms, hanwin
import pypercept
import numpy as np

class ToneInNoise(AFCExperiment, Sequential):

  def init_experiment(self, exp):

    exp.description = """
    This experiment measures the detectability of a sinuoid test
    signal in a broadband noise masker as a function of sinuoid
    frequency and noise level. The noise is always a running 
    noise, i.e. an independent noise realisation in all intervals 
    and in all trials. 
    """

    exp.add_parameter('noise_level', [-40, -60], 'dB FS',
                      'level of broad band noise masker')
    exp.add_parameter('sine_freq', [500, 1500, 3000], 'Hz',
                      'frequency of sinusoid test signal')
    exp.set_variable('sine_level', -30, 'dB FS',
                     'level of sinusoid test signal')
    exp.add_adapt_setting('1up2down', max_reversals = 8, 
                          start_step = 8, min_step = 1)

    exp.num_afc = 3        # The number of AFC alternatives

    exp.sample_rate = 48000
    exp.pre_signal  = 0.3  # Pre-Signal, duration of silence in s
    exp.post_signal = 0.2  # Post-Signal, duration of silence in s
    exp.isi_signal  = 0.3  # Inter-Stimulus-Interval, silence in s

  def init_run(self, run):

    # Set the start value of the variable (sine_level) at some
    # supra threshold value, depending on the level of the noise.  
    run.variable = run.noise_level + 10

    # The durations of sine and noise and window ramp, in seconds:
    run.sine_dur = 0.25
    run.noise_dur = 0.35
    run.ramp_dur = 0.05

    # The same durations in number of samples:
    run.sine_nsamp = round(run.sine_dur * run.sample_rate)
    run.noise_nsamp = round(run.noise_dur * run.sample_rate)
    run.ramp_nsamp = round(run.ramp_dur * run.sample_rate)

    # Pre-generate the sinusoid with RMS = 1
    run.sine1 = gensin(run.sine_freq, np.sqrt(2), 
                       run.sine_dur, run.sample_rate)
    # and apply Hanning window ramps
    run.sine1 = hanwin(run.sine1, run.ramp_nsamp)

    # Define indices of start and end of the sine within the noise 
    run.idx1 = round((run.noise_nsamp - run.sine_nsamp) / 2)
    run.idx2 = run.idx1 + run.sine_nsamp

  def init_trial(self, trial, run):

    # Generate num_afc new, independent Gaussian noises 
    noise = np.random.randn(run.noise_nsamp, self.num_afc)

    # Adjust the RMS level, in all num_afc instances of the noise
    noise = noise/rms(noise,axis=0)*10**(run.noise_level/20)

    # Apply Hanning window to all noises
    for k in range(self.num_afc):
        noise[:,k] = hanwin(noise[:,k], run.ramp_nsamp)

    # Now use the first num_afc-1 columns of noise as reference
    # signals and the last column of noise for the test signal
    trial.reference_signal = []
    trial.reference_signal.append(noise[:,0])
    trial.reference_signal.append(noise[:,1])
    trial.test_signal = noise[:,2]

    # Scale the pre-generated sinusoid signal in amplitude 
    scaled_sine = run.sine1 * 10**(trial.variable/20)
    # and add it to the noise
    trial.test_signal[run.idx1:run.idx2] += scaled_sine
      
if __name__ == '__main__':
  pypercept.start(ToneInNoise)
\end{verbatim}
\normalsize

Details of the inheritance concept from the experiment to a run and
from a run to a trial can be found in the documentation of pypercept.
%
The experiment is then started by executing the Python file, like in:
~~ 
{\footnotesize
\verb+$ python3 tone_in_running_noise.py+
}

An example of the course of one run of this 3-AFC experiment with
the adaptive 1-up-2-down method controlling the level of the sinusoid
is shown in figure~\ref{fig:example_run_1u2d_3000hz}.
% 
\begin{figure}[hbt]
    \begin{center}
        \includegraphics[width=1.1\columnwidth]{example_run_1u2d_3000hz}
    \end{center}
    \caption{Example of one run of the experiment described by the
      example code above.  Correct answers are marked with a
      '\texttt{+}'~sign, incorrect answers with a '\texttt{-}'~sign.
      The variable step size starts at the initial value of 8 dB.  It
      is automatically halved at each upper reversal point until the
      minimal step size of 1 dB is reached.  This point is defined as
      the transition from the familiarisation phase (dashed line) to
      the measurement phase (solid line).  The resulting threshold is
      expressed as the mean and the median across the reversal points
      during the measurement phase, which was defined to last for a
      number of 8 reversals.}
    \label{fig:example_run_1u2d_3000hz}
\end{figure}


Pypercept sets up an internal list of all runs that are to be measured
during the experiment.  In case of an \emph{interleaved} experiment,
pypercept will \emph{randomly} select one run from the list of all
unfinished runs, and will then present the next trial from that chosen
run.  This is the main difference compared to a \emph{sequential} experiment,
where pypercept will always choose the \emph{same} (current) run from
the list of all runs, until that run is finished.


\section*{Analysis of measurement data}
\label{sec:meas-data-handl}

Each time when one run has been completed, i.e.\ a threshold has been
reached in an adaptive experiment or the block of trials in a constant
stimulus experiment has been finished, the result of that run and all
its properties is automatically saved in a result file in json
format. It is therefore rather human-readable and can easily be
processed for later analysis.

Pypercept offers functions to load and analyse the measured data for
any combination of subject and experiment name. For an adaptive
experiment, all threshold data points will be plotted as a function of
the first parameter.  If more than one parameter is present in the
experiment, each further parameter value (or combination of further
parameters) will result in a separate curve with a corresponding
label, as illustrated in figure~\ref{fig:analyse_average} for the case
of two parameters.
\\
In case of a constant stimuli experiment, the probability for correct
answers will be plotted as a function of the variable values, and each
parameter resp. each combination of parameters will result in a
separate curve with a corresponding label.
\\
%
\begin{figure}[tb]
    \begin{center}
        \includegraphics[width=1.1\columnwidth]{example_data_average}
    \end{center}
    \caption{Example of threshold data for the experiment described in
      the above code.  The thresholds are shown as the mean and
      standard deviation across repeated measurements of each run.
      Note that the order of the two parameters, sinusoid frequency
      and noise level, has been swapped relative to the order in which
      they are specified in the experiment's code.}
    \label{fig:analyse_average}
%\end{figure}
%
%\begin{figure}[hbt]
    \begin{center}
        \includegraphics[width=1.1\columnwidth]{example_data_raw}
    \end{center}
    \caption{All single threshold data points are shown individually, i.e.\
      without averaging across repetitions of a run.
      Averaging resulted in the data points shown in
      Fig.~\ref{fig:analyse_average}.
      The two parameters, noise
      level and sinusoid frequency, appear in the order in which they
      are specified in the experiment's code.} 
    \label{fig:analyse_raw}
\end{figure}
%
The figures~\ref{fig:analyse_average} and~\ref{fig:analyse_raw} were
produced by the following code which loads the data gathered
from a model simulation for the subject/model called
\verb+sim_swamp_psm+ in the experiment called \verb+ToneInNoiseSim+:

\scriptsize
\begin{verbatim}
from pypercept.analyse_data import AnalyseData

ad = AnalyseData()
ad.load_data('sim_swamp_psm', 'ToneInNoiseSim')

# Average across repetitions (default), 
# permute the two parameters. 
ad.display_data(perm_params=[2,1])    # Result shown in Fig. 2

# No averaging across repetitions, 
# no parameter permutation (default).
ad.display_data(raw=True)             # Result shown in Fig. 3
\end{verbatim}
%
\normalsize

The default behaviour is to average the threshold data, or
the probability for correct answers respectively, across all repeated measurements
of any suitable run.  This is shown in figure~\ref{fig:analyse_average} which
also illustrates the possibility to change the order of parameters
post-hoc.  Instead of averaging, it is possible to access and plot all
single data points individually.  An example of such a plot is shown
in figure~\ref{fig:analyse_raw}.


\section*{Model simulations}
\label{sec:model-simulations}

Pypercept can also be a useful tool for developing psychoacoustical models.
Pypercept offers an easy way to run the same experiment with either
human test subjects or with an auditory model that replaces the human
listener's answers.

Using pypercept this way will prevent that the stimuli are presented
acoustically and that a listener is asked for a response.  Instead,
the user can provide own functions which calculate the response of the
auditory model.
%
The method \verb+model_init_run()+ will get called once at the
beginning of a new run.  This method would do any general calculations
that pertain to the entire run.  The method \verb+model_simulation()+
will get called once per trial, at the time when the human listener
would have been asked for a response.  This method should therefore
analyse the stimuli of the current trial and calculate the model
response, respectively its decision.  More details can be found in
pypercept's documentation and in an example auditory model which is
part of the distribution.  This model was used to generate the data
shown in figures~\ref{fig:analyse_average} and \ref{fig:analyse_raw}.  

Pypercept's distribution contains a number of utility functions used
in a family of established psychoacoustical models: linear Gammatone
filter \cite{hohmann_2002} with critical auditory bandwidth, Gammatone
filterbank, simple haircell model employing halfwave-rectification and
lowpass filtering, adaption loops \cite{pueschel_1988,amtoolbox},
optimal detector \cite{amtoolbox,dau_96b}, modulation lowpass filter
\cite{dau_96b}, and modulation filterbank \cite{amtoolbox,dau_97a}.

\section*{Outlook}
\label{sec:outlook}

The software has been tested and used succesfully by the first cohort
of users.  Due to the open source format, the user can easily adapt
and expand pypercept to his/her own needs.

Ideas for future improvements are to apply a linear filter to
equalise a headphone transfer function.  A database with equalisation
filters for a number of commonly used headphone models could be
provided.  Pypercept can be augmented by further experimental
paradigms like ABX or Yes/No procedures.  Alternative methods to
control the adpative stepsize and further criteria definining the end
of the familiarisation phase and/or measurement phase could easily be
implemented.



%
\begin{thebibliography}{11}
\bibitem{pypercept_gitlab}
  Pypercept software, URL: \\
  \url{https://gitlab.gwdg.de/ha_soft/pypercept}
\bibitem{earyx_github}
  Earyx software, URL: \\
  \url{https://github.com/TGM-Oldenburg/earyx}
\bibitem{psylab_2006}
  Hansen, M. ``Lehre und Ausbildung in Psychoakustik mit psylab: freie
  Software für psychoakustische Experimente'', in Fortschritte der
  Akustik -- DAGA~'06, 591-592, 2006.
\bibitem{psylab_github}
  Psylab software, URL: \\
  \url{https://github.com/TGM-Oldenburg/Psylab}
\bibitem{levitt_1971}
  Levitt, H. ``Transformed Up‐Down Methods in Psychoacoustics'', JASA
  49, 467-477, 1971.
\bibitem{kaernbach_2001}
  Kaernbach, C. ``Adaptive threshold estimation with unforced-choice
  tasks'', Perception \& Psychophysics 63(8), 1377-1388, 2001. 
\bibitem{hohmann_2002}
  Hohmann, V. ``Frequency analysis and synthesis using a gammatone
  filterbank'', Acta Acustia, 88, 433-442, 2002.
\bibitem{pueschel_1988}
  Püschel, D. ``Prinzipien der zeitlichen Analyse beim Hören'', PhD
  thesis, Universität Göttingen, 1988.
\bibitem{amtoolbox}
  Auditory Modeling Toolbox: \\
  \url{https://www.amtoolbox.org/}
\bibitem{dau_96b}
  Dau et al. ``A quantitative model of the effective signal processing
  in the auditory system: II Simulations and measurements'', JASA 99,
  3623-3631, 1996.
\bibitem{dau_97a}
  Dau et al. ``Modeling auditory processing of amplitude modulation:
  I. Detection and masking with narrow-band carriers'', JASA 102,
  2892-2905, 1997
\end{thebibliography}
\end{document}



