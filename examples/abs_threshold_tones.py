#!/usr/bin/env python3


"""Measurement of the absolute threshold level of sinusoid in quiet as a function of its frequency
    
   This file uses pypercept, a collection of functions for designing and
   controlling interactive psychoacoustical listening experiments.

   Copyright (C) 2023 by Martin Hansen, Jade Hochschule, Oldenburg
   Author :  Martin Hansen <martin.hansen AT jade-hs.de>
   Date   :  22 Aug 2023

   This file is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published
   by the Free Software Foundation; either version 2 of the License, 
   or (at your option) any later version.
   See the GNU General Public License for more details:
   http://www.gnu.org/licenses/gpl
"""

from pypercept.experiments import AdaptAFCExperiment
from pypercept.order import Sequential
from pypercept.utils import gensin, hanwin, rms
import numpy as np
import pypercept

class AbsThresholdTones(AdaptAFCExperiment, Sequential):
    
    def init_experiment(self, exp):

        exp.description = """
        The experiment measures the detectability of a sinuoid test
        signal in quiet as a function of its frequency.
        
        This experiment needs the use of visual interval indication
        using the GUI. 
        """
        
        exp.add_parameter('sine_freq', [125, 250, 500, 1000, 1500, 2000, 3000, 4000, 6000, 8000], 'Hz',
                          'frequency of sinusoid test signal')
        exp.set_variable('sine_level', -30, 'dB',
                         'level of sinusoid test signal')
        exp.add_adapt_setting('WUD', pc_convergence = 0.75, 
                              max_reversals = 8, start_step = 8, min_step = 1)

        exp.num_afc = 2        # The number of AFC alternatives

        exp.isi_signal  = 0.3  # Inter-Stimulus-Interval, duration of silence in s
        exp.pre_signal  = 0.2
        exp.post_signal = 0.2

        exp.debug = True
        exp.debug_plot_signal = True


    def init_run(self, run):
        # The durations of sine and window ramp, in seconds:
        run.sine_dur = 0.5
        run.ramp_dur = 0.05

        # The same durations in number of samples:
        run.sine_nsamp = round(run.sine_dur * run.sample_rate)
        run.ramp_nsamp = round(run.ramp_dur * run.sample_rate)

        # Pre-generate the sinusoid with RMS = 1
        run.sine1 = gensin(run.sine_freq, np.sqrt(2), run.sine_dur, run.sample_rate)
        run.sine1 = hanwin(run.sine1, run.ramp_nsamp)

        # The reference signal is always the same: zeros with same duration as the test signal
        run.reference_signal = run.sine1 * 0


    def init_trial(self, trial, run):

        if trial.variable > -3:
            print('WARNING:    tone level limited to -3dB RMS to prevent clipping.')
            trial.variable = -3 

        # Scale the sinusoid signal in amplitude
        trial.test_signal = run.sine1 * 10**(trial.variable/20)

        

if __name__ == '__main__':
    pypercept.start(AbsThresholdTones) 


