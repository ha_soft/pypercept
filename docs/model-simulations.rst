.. model-simulations.rst


Hooking in an auditory model for simulations
============================================

|pyper| offers an easy way to run the same experiment with either
human test subjects or with an auditory model that replaces the human
listener's answers.

In short, your experiment class should inherit from class
``AFCSimulation`` and from your own experiment's class , and you
must provide the method ``model_simulation()``.  The class
``AFCSimulation`` will prevent that the stimuli are presented
acoustically and that a listener is asked for a response.   The class
also provides the two methods ``model_init_run()`` and ``model_simulation()``
which the user needs to define. 

The method ``model_init_run()`` will get called once at the beginning
of a new run.  This method would do any general calculations
that pertain to the entire run.  Depending on your modeling approach,
this method might be left undefined.
The method ``model_simulation()`` will get called once per trial, at the
time when the human listener would have been asked for a response.
This method should therefore analyse the stimuli of the current trial
and calculate the model's response, respectively its decision.  The
method must return the model's decision as for example
``Result.CORRECT`` or ``Result.WRONG``.

This mechanism is best explained using an example.  Consider the same
experiment ``ToneInRunningNoise`` in the file ``tone_in_running_noise.py`` as
described above and as found in the ``examples`` subdirectory.  As a
very easy auditory model we use the concept of *masking by swamping*
with the following rationale: The RMS level :math:`L(n(t)+s(t))` of
the interval containing signal+noise will be higher than the RMS level
:math:`L(n(t))` of the interval containing only noise.
Let :math:`L(n(t)+s(t))` and :math:`L(n(t))` be the levels at the *output*
of the auditory filter centered to the test tone frequency.  The
swamping model postulates that the level difference
:math:`\Delta L = L(n(t)+s(t)) - L(n(t))` equals a constant value
:math:`\Delta L` = 1.4 dB at the detection threshold.  This can be used as a
decision criterion.  The idea is implemented as follows in the file
``tone_in_running_noise_sim.py`` which can be found in the
``examples`` subdirectory: 

.. code::

  """Simulation of detection threshold level of a sinusoid in noise
     as a function of sinusoid frequency and noise level

   This file must be called using the commandline argument '-u model'
   """

  from pypercept.experiments import AFCSimulation
  from pypercept.result import Result
  from pypercept.analyse_data import AnalyseData
  from pypercept.utils import rms, gammatone_crit_filter as gcf
  import pypercept
  import numpy as np

  from tone_in_running_noise import ToneInRunningNoise

  class ToneInNoiseSim(AFCSimulation, ToneInRunningNoise):

    def init_experiment(self, exp):
      exp.subject_name = 'sim_swamp_psm'
      super().init_experiment(exp)

      # Limit parameter values compared to original experiment, if needed
      exp.parameters['noise_level']['values'] =   [-40]

      exp.debug = True

    def model_simulation(self, run, trial):

      # Apply a very easy "swamping" model for masking, as described in
      # Moore's "Introduction to the Psychology of Hearing", 6th edition,
      # chapter 6C "The Mechanism of Masking - Swamping or Suppression?"
        
      # Calculate the signal power at the output of the auditory filter
      # that is tuned to the test frequency.  The auditory filter is
      # modeled by a gammatone filter with a bandwidth equal to the
      # critical/auditory bandwidth, implemented in gcf(), which is tuned
      # to the test signal frequency:
        
      crit_filt_testsig,_ = gcf(trial.test_signal, run.sample_rate, run.sine_freq)
      crit_filt_refsig1,_ = gcf(trial.reference_signal[0], run.sample_rate, run.sine_freq)
      crit_filt_refsig2,_ = gcf(trial.reference_signal[1], run.sample_rate, run.sine_freq)
            
      level_cf_testsig = 20*np.log10(rms(np.real(crit_filt_testsig)))
      level_cf_refsig1 = 20*np.log10(rms(np.real(crit_filt_refsig1)))
      level_cf_refsig2 = 20*np.log10(rms(np.real(crit_filt_refsig2)))

      if self.debug:
        print(f'{level_cf_testsig=:5.1f}, {level_cf_refsig1=:5.1f}, {level_cf_refsig2=:5.1f}', end=' ')

      # The model decides to detect the test signal, when the signal+noise
      # interval is higher in level by 1.4 dB relative to the noise alone
      # interval(s), when the levels are measured at the output of the 
      # auditory filter.  The criterion of 1.4 dB results from the idea of the
      # Power Spectrum Model, saying:
      #       Level_signal_thres = Level_noise_filter - 4 dB
      # From this it follows, that at threshold 
      #    10*log10( RMS(noise + signal@threshold) / RMS(noise alone))
      #  = 10*log10( 1+10**(-4/10) ) = 1.4 dB

      delta_level_criterion = 1.4  # level difference at threshold, in dB
        
      if level_cf_testsig > max(level_cf_refsig1, level_cf_refsig2) + delta_level_criterion:
        return Result.CORRECT
      else: 
        return Result.WRONG

  if __name__ == '__main__':
    pypercept.start(ToneInNoiseSim)



As you can see, the simulation experiment ``ToneInNoiseSim`` inherits
from ``AFCSimulation`` and from the very same experiment
``ToneInRunningNoise`` which is used for the listening experiment.  If you
want, you can change the parameters of the experiment, as done in
``init_experiment()`` for illustration.  By assigning the
``subject_name`` before the parent's class'
``super().init_experiment(exp)`` is called, the user will not be
queried interactively for this name.

In this example, there is no need for the method ``model_init_run()``,
and it is therefore left unspecified.  All model calculations are found
within the method ``model_simulation()``:
The auditory filtering is modeled by a 4th order gammatone filter with
a bandwidth equal to the critical auditory bandwidth, as defined in
``gammatone_crit_filter()`` in ``utils.py``.  Note that this filter
implementation calculates the complex valued analytical signal of the
output signal.  Only its real part is used as the filter output here. 
Its absolute magnitude is the Hilbert envelope of the output signal,
and its imaginary part is the Hilbert transform of the real part signal.  

In the end, the model directly calculates/decides the result to be either
``Result.CORRECT`` or ``Result.WRONG``.  

Note that in order to inform |pyper| to run a model simulation
instead of a listening experiment, you must call your experiment's
python file with the commandline argument ``-u model``, like so:

.. code::

  $ python tone_in_running_noise_sim.py -u model


A number of functions useful for auditory modeling is readily
implemented in |pyper|.  They are briefly described in 
`Utility functions in pypercept`_
