"""This file is part of pypercept:
a package for developing psychoacoustical listening experiments.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
"""

"""This module contains methods for saving or plotting experimental results"""

import json
import os
import regex as re
from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
from pypercept.result import Result

def json_export(run, path: str = os.getcwd()) -> str:
    '''Save data of one completed run as JSON-file.
    One file holds all experiments for a single subject identifier.
        
    Filename is <path>/pypercept_<subject>.json

    Parameters
    ----------
    experiment : Experiment 
        pypercept experiment (required for metadata not contained in the individual run)
    run : Run
        the single run to save   
    path : str
        directory pathname where json-file will be saved. Defaults to the current working directory
    
    Returns
    -------
    filepath : str
        fully qualified path to this subject's json file.

    '''  

    experiment = run.exp
    
    if run.finished:

        run.finalize_run()

        # Extract variables and answers of all trials of current run
        trials_variables = []
        trials_responses = []
        trials_results = []
        for trial in run.trials:
            trials_variables.append(trial.variable)
            trials_responses.append(trial.user_response)
            trials_results.append(trial.result.name.lower())

        # experiment data
        new_run = {}
        new_run['Experiment'] = experiment.cls
        new_run['Subject'] = experiment.subject_name
        new_run['Timestamp (started)'] = run.started
        new_run['Timestamp (finished)'] = run.finished
        new_run['NumChoices (AFC/AUC)'] = experiment.num_afc
        if run.type == 'ConstStim':
            new_run['Adaptation'] = run.type
        else:
            new_run['Adaptation'] = experiment.adapt_settings[0]

        new_run['NumParameters'] = len(experiment.parameters)
        idx = 1
        param_values = []
        for par, items in experiment.parameters.items():  
            new_run['Parameter ' + str(idx)] = {
                'Name': par, 
                'Value': getattr(run, par), 
                'Unit': items['unit']
                }
            param_values.append(getattr(run, par))
            idx = idx+1
        new_run['Variable'] = {
            'Name': experiment.variable['name'],
            'Unit': experiment.variable['unit']
            }

        if run.type == 'ConstStim':
            new_run['Results'] = {
                'Number of presentations' : experiment.num_presentations,
                'Variable' : run.result['variable'],
                'Estim. prob_correct' : run.result['est_prob_corr'],
                'Std.Err. prob_correct' : run.result['std_err_prob_corr']
            }
            # build a structure to allow plotting immediate results
            if experiment.debug:
                # 1st run? add empty results to experiment 
                if not hasattr(experiment, 'results'):
                    experiment.results = [] # use list as it's easier to append to
                # add current run, each variable as separate list 
                for var, corr, std_corr in zip(run.result['variable'], 
                                               run.result['est_prob_corr'], 
                                               run.result['std_err_prob_corr']):
                    experiment.results.append([var, corr, std_corr, *param_values])
                # plot data
                fig = plt.figure(102)
                ax = fig.add_subplot(111)
                fig.clear()
                data = np.array(experiment.results) # convert to array for easier plotting
                # this is the same procedure as pypercept: group unique datasets and plot
                u_data, u_idx, u_map = np.unique(data[:,3:], return_index = True, 
                                                 return_inverse=True, axis = 0)
                for i in range(len(u_idx)):
                    r_idx = np.where(i == u_map)[0]
                    plt.errorbar(x = data[r_idx, 0], y = data[r_idx, 1]*100, yerr = data[r_idx, 2]*100, 
                                fmt = 'o:', markerfacecolor='none', capsize = 3)  
                #title = f'experiment {experiment.cls}, subject {experiment.subject_name}'
                title = f'{experiment.cls}, {experiment.subject_name}'
                for idx, par in enumerate(experiment.parameters.items(), 1):
                    if par[1]["unit"] == "":
                        title += f', Par.{idx}: {par[0]}'
                    else:
                        title += f', Par.{idx}: {par[0]} in {par[1]["unit"]}'
                plt.title(title)
                legend = []
                for lines in u_data:
                    entry = ''
                    for idx, value in enumerate(lines, 1): 
                        entry += f'Par{idx}={value} '
                    legend.append(entry)
                plt.xlabel(f'{experiment.variable["name"]} in {experiment.variable["unit"]}')
                plt.ylabel('Percent correct')
                plt.legend(legend)
                plt.pause(0.1)
        else: 
            new_run['Threshold'] = {
                'Method': 'reversals' if run.exp.threshold_reversals else 'all',
                'start_measurement_idx': run.start_measurement_idx,
                'Median': run.result['med'],
                'Mean': run.result['mean'],
                'Std. Deviation': run.result['std'],
                'Maximum': run.result['maxi'],
                'Minimum': run.result['mini']
            }
            if experiment.debug:
                # 1st run? add empty results to experiment 
                if not hasattr(experiment, 'results'):
                    experiment.results = [] # use list as it's easier to append to (?)
                # add current run
                experiment.results.append([param_values[0], run.result['mean'], run.result['std'], *param_values[1:]])
                # plot data
                plt.ion()
                fig = plt.figure(103)
                # ax = fig.subplots(1)
                ax = fig.add_subplot(111)
                fig.clear()
                data_tmp = np.array(experiment.results) # convert to array for easier plotting
                data = data_tmp[0:,:3].astype(float)    # the plot itself needs only the first 3 columns
                # this is the same procedure as pypercept: group unique datasets and plot
                u_data, u_idx, u_map = np.unique(data_tmp[:,3:], return_index = True, 
                                                 return_inverse=True, axis = 0)
                for i in range(len(u_idx)):
                    r_idx = np.where(i == u_map)[0]
                    plt.errorbar(x = data[r_idx, 0], y = data[r_idx, 1], yerr = data[r_idx, 2], 
                                fmt = 'o:', markerfacecolor='none', capsize = 3)  
                #title = f'experiment {experiment.cls}, subject {experiment.subject_name}'
                title = f'{experiment.cls}, {experiment.subject_name}'
                for idx, par in enumerate(experiment.parameters.items(), 1):
                    if idx != 1: # skip 1st param (which is on x-axis)
                        if par[1]["unit"] == "":
                            title += f', Par.{idx}: {par[0]}'
                        else:
                            title += f', Par.{idx}: {par[0]} in {par[1]["unit"]}'
                plt.title(title)
                legend = []
                for lines in u_data:
                    entry = ''
                    for i, value in enumerate(lines): 
                        entry += f'Par{i+2}={value}  '
                    legend.append(entry)
                plt.xlabel(f'{new_run["Parameter 1"]["Name"]} in {new_run["Parameter 1"]["Unit"]}')
                plt.ylabel(f'{experiment.variable["name"]} in {experiment.variable["unit"]}')
                plt.legend(legend)
                plt.pause(0.1)

        new_run['Trials'] = {
            'Variable': trials_variables,
            'Response' : trials_responses,
            'Result': trials_results
        }
        
        # path/filename
        name = experiment.subject_name.replace(" ", "")
        filepath = os.path.join(path, 'pypercept_' + name + '.json')

        # load existing data
        data = []
        if os.path.exists(filepath):
            with open(filepath) as f:
                try:
                    data = json.load(f)
                except:
                    # apparently a datafile exists but cannot be read.
                    # to prevent data-loss, dump the current results to another file
                    print(f'Apparently a {filepath} for the subject exists but is corrupt.')
                    filepath = os.path.join(path, 'pypercept_' + name + '_' + datetime.now().strftime('%Y%m%d_%H%M%S') + '.json')
                    print(f'The current results will be written to {filepath}')
                    
        # append new run
        data.append(new_run)

        # In order to keep all the values for each trial in one long line,
        # https://stackoverflow.com/a/65445192  mentions this solutions which
        # reformats the json.dumps output, being a string:
        json_dumps = json.dumps(data, indent=2)
        start = [m.start() for m in re.finditer('\: \[', json_dumps)]
        end = [m.start() + 1 for m in re.finditer(' *\]', json_dumps)]
        original = []
        altered = []
        for s, e in zip(start, end) :
            original.append(json_dumps[s:e])
            altered.append(json_dumps[s:e].replace('\n', '').replace('  ', '').replace(',', ', '))
        for o, a in zip(original, altered) :
            json_dumps = json_dumps.replace(o, a)

        # write data
        with open(filepath, 'w') as f:
            f.write(json_dumps)          # all variables of a trial on 1 line in json output

        return filepath

    else:
        print('Error, an unfinished run shall not be exported to the result file')
        return


def plot_current_run(run, fig_num=101):

    variables = [trial.variable for trial in run.trials]
    length = len(variables)

    variable_unit = run.exp.variable['unit']
    variable_name = run.exp.variable['name']

    plt.ion()
    fig = plt.figure(fig_num)
    ax = fig.add_subplot(111)
    fig.clear()

    idx_corr = np.where(np.array([trial.result == Result.CORRECT for trial in run.trials]) == True)
    idx_wrong = np.where(np.array([trial.result == Result.WRONG for trial in run.trials]) == True)
    idx_undecided = np.where(np.array([trial.result == Result.UNDECIDED for trial in run.trials]) == True)
    trial_nr = np.arange(1,length+1)
    plt.plot(trial_nr[idx_corr[0]], np.array(variables)[idx_corr], 'g+', markersize=8)
    plt.plot(trial_nr[idx_wrong[0]], np.array(variables)[idx_wrong], 'r_', markersize=8)
    plt.plot(trial_nr[idx_undecided[0]], np.array(variables)[idx_undecided], 'ko', markersize=8)

    if run.type == 'ConstStim':
       x_axes = np.arange(1,length+1)
       plt.plot(x_axes,variables,'--')
    else:
        if run.start_measurement_idx == None:
            x_axes = np.arange(1,length+1)
            plt.plot(x_axes,variables,'--')
        else:
            x1 = np.arange(1,run.start_measurement_idx+1)
            x2 = np.arange(run.start_measurement_idx,length+1)
            measurement_variables = variables[run.start_measurement_idx-1:]
            plt.plot(x1,variables[:run.start_measurement_idx],'--', color='0.6')
            plt.plot(x2,measurement_variables,'-', color='0.1')

        if run.finished:
            if  hasattr(run.exp, 'threshold_reversals') and run.exp.threshold_reversals:
                # variables at reversals only
                plt.text(0.7, 0.90, 'Threshold for reversals only', ha='left', va='center', transform=ax.transAxes)
                measurement_variables = find_reversals(measurement_variables)
                print(measurement_variables)

            median_threshold = np.median(measurement_variables)
            mean_threshold = np.mean(measurement_variables)
            std_threshold = np.std(measurement_variables, ddof=1)
            
            median_text = 'Med: %g %s' % (median_threshold, variable_unit)
            mean_text = 'Mean: %g %s' % (mean_threshold, variable_unit)
            std_text = 'Std.: %g %s' % (std_threshold, variable_unit)
            plt.text(0.7, 0.85, mean_text, ha='left', va='center', transform=ax.transAxes)
            plt.text(0.7, 0.80, median_text, ha='left', va='center', transform=ax.transAxes)
            plt.text(0.7, 0.75, std_text, ha='left', va='center', transform=ax.transAxes)

    #title_string = 'Exp.: %s, ' % type(run.exp).__name__
    title_string = '%s, ' % type(run.exp).__name__
    title_string += '%s, ' % run.exp.subject_name
    title_string += run.get_param_string()
    plt.title(title_string)
    plt.xlabel('trial number')
    variable_string = '%s [%s]' % (variable_name, variable_unit)
    plt.ylabel(variable_string)
    plt.xlim((0,length+1))
    plt.ylim((min(variables)-1,max(variables)+1))
    if run.exp.isSim:
        plt.pause(0.01)
    else:
        plt.draw()

    return fig


def find_reversals(measurements):
    # find reversals
    direction = None
    results = []
    for i in range(len(measurements)-1):
        if measurements[i+1] < measurements[i]:
            if direction == 1:
                results.append(measurements[i])
            direction = 0 # down
        elif measurements[i+1] > measurements[i]:
            if direction == 0:
                results.append(measurements[i])
            direction = 1 # up

    # last variable is always a reversal
    results.append(measurements[-1])

    return results
