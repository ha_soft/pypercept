
Welcome to pypercept's documentation
====================================

.. _pypercept:  https://gitlab.gwdg.de/ha_soft/pypercept
.. _earyx:  https://github.com/TGM-Oldenburg/earyx
.. _psylab:  https://github.com/TGM-Oldenburg/Psylab
.. _daga_paper: file://docs/daga2024-proceedings/daga2024_proceedings_pypercept.pdf

pypercept_ is a Python framework for developing and running psychoacoustical
experiments.

pypercept_ is used for education and research at Institut für Hörtechnik und
Audiologie (IHA), Jade Hochschule in Oldenburg, Germany.  One aim of
pypercept_ has been to provide the students in the psychoacoustics courses
with a uniform and simple starting point for designing their own listening
experiments.

Currently, :math:`n`-AFC detection and discrimination experiments
(:math:`n=2,3,4`) and 2-AFC matching experiments are readily
supported. The stimulus variable can either be controlled adaptively
according to transformed-up-down algorithms or weighted-up-down
algorithms. Alternatively, the method of constant stimuli may be used
for detection and discrimination experiments.

pypercept_ runs on Linux, Mac, and Windows platforms and there are several
options for running your experiments: It offers a commandline/text based user
interface, a browser based graphical UI, or a server based UI.  The
output stimuli can be filtered to allow for a headphone equalisation,
if you provide the filter coefficients.

pypercept_ offers the possibility to run the same experiment with
either human test subjects, or with an auditory model hooked in which
replaces the test subject.

The different features are explained in the documentation and can be
explored through several example experiments which are part of the
distribution.


History / Authors
-----------------
pypercept_ is being developed by Sven Kissner and Martin Hansen at
`Institut für Hörtechnik und Audiologie (IHA) <https://www.jade-hs.de/ha/>`_,
`Jade University of Applied Sciences <http://www.jade-hs.de/>`_ in Oldenburg.  

pypercept_ expanded on earyx_, which was developed at the IHA, Jade
Hochschule.  The first version of earyx was the result of a student
programming project in 2015.  earyx_ was developed by Sven Hermann,
Jonas Klug, Nils Schreiber, Matthias Stennes, and Stephanus Volke with
the supervision of Bastian Bechtold.

Earyx was in turn inspired by psylab_, written in Matlab, which has
been developed by Martin Hansen at IHA, Jade Hochschule since 2003.
psylab_ has been used regularly for education and research at Institut
für Hörtechnik und Audiologie (IHA), Jade Hochschule in Oldenburg,
Germany.

So far, some +400 students at IHA had used pypercept's predecessor
psylab and +30 students have used pypercept_ in their
psychoacoustics assignments for designing and carrying out individual
listening experiments. Also their experience and feedback has lead to
an ongoing improvement of psylab and pypercept_.


Licence
-------

pypercept_ is “free software” and is distributed in OpenSource
format under the terms of the GNU General Public Licence (GPL). This
means, amongst others, that copies of the pypercept_ software may
be distributed without asking for permission, given that the terms of
the GNU GPL are obeyed to. For more details, see in the
file `LICENSE <file:./LICENSE>`_ in the root directory of pypercept_.


No warranty
-----------
pypercept_ is distributed without any kind of warranty.
The use of pypercept_, for whatever purpose, is under the complete
and sole responsibility of the user.
Please have a look at the file `NO-WARRANTY <file:./NO-WARRANTY>`_ in the
root directory of pypercept_ before using it.   

.. Danger::
  
  .. _special attention: 
   
    **Special attention** is drawn to the fact that pypercept_ does
    not comprise an automatic mechanism to prevent the delivery of too
    loud sound pressure levels. It is the explicit and sole
    responsibility of the user of pypercept_ to make sure that the
    generated stimuli, in combination with any further equipment
    (sound card, amplifier, headphone or loudspeaker, etc.), will
    yield the desired sound level.



Installation
------------

.. _zip-file: https://gitlab.gwdg.de/ha_soft/pypercept/

To get pypercept_ installed on your system, do one of the following:
  * either download the zip-file of the distribution at
    https://gitlab.gwdg.de/ha_soft/pypercept/ and extract it, 
  * or clone the repository using git:
  
.. code::

  git clone https://gitlab.gwdg.de/ha_soft/pypercept.git


Then change into the *root* directory (the one that contains the
subdirectories ``docs``, ``examples``, and ``pypercept``) and create a
`virtual environment <https://docs.python.org/3/library/venv.html>`_
(recommended):

.. code::

  cd pypercept            # the top level directory produced by 'git clone' / 'unzip' 
  python -m venv .venv    # create virtual environment

and activate your new virtual environment using one of the commands
below, depending on your platform:  

.. code::

  .venv\Scripts\activate.bat    #  (Windows command line)
  .venv\Scripts\Activate.ps1    #  (Windows PowerShell)
  source .venv/bin/activate     #  (Bash, Linux/Mac)

Install pypercept_ and its dependencies via pip:

.. code::

  pip install --editable .

.. note::
  
  `--editable` or `-e` installs a dummy in PYTHONPATH that points back
  to the source folder.  This allows for development without
  un-/reinstalling or tempering with paths.  

..  :fixme:
..
..   Mention a manual installation, incl. use of soundcard, numpy, etc. ? 


.. **The original earyx documentation can be found on** `Read the docs <http://earyx.readthedocs.org>`_. 



Short introduction
------------------

A short overview of the main features of pypercept_ is given in a proceedings paper
`"Pypercept: free Python software for psychoacoustical experiments" <https://pub.dega-akustik.de/DAGA_2024/files/upload/paper/204.pdf>`_
of the `DAGA 2024 <https://pub.dega-akustik.de/DAGA_2024/>`_
conference.
A copy of this paper can be found in the separate subdirectory
``docs/daga2024-proceedings`` of the documention directory.  This
paper is also appropriate for a `Citation of pypercept`_

.. This paper is also appropriate for a :ref:`citation of pypercept<section_cite_pypercept>`. 




Quick start
-----------

For a quick start, try out one of the example experiments in the
directory `examples <file:./pypercept/experiments/>`_. Each experiment
is contained in a single Python file.  Start the experiment by running
it like in:

.. code::

  python sam_sincarrier_detect.py


.. _section_cite_pypercept:

Citation of pypercept
---------------------

If you want to cite pypercept_, please cite the conference proceedings
paper [Hansen-Kissner-2024]_ which was also mentioned in the section
`Short introduction`_



.. [Hansen-Kissner-2024] M. Hansen & S. Kissner (2024), "Pypercept:
		 free Python software for psychoacoustical experiments", 
		 in Proc. "Fortschritte der Akustik - DAGA'24", Hannover.
		 `(pdf) <https://pub.dega-akustik.de/DAGA_2024/files/upload/paper/204.pdf>`_    

