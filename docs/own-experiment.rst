.. own-experiment.rst


How to write your own experiment
================================

This section starts with the description how to use |pyper| for
non-interleaved experiments.
Subsection `Setting up interleaved tracks experiment`_ explains specific
differences that apply for interleaved-tracks experiments.

Designing your own psychoacoustical experiment with |pyper| is easy:
Just write a Python file `my_experiment.py` which defines one class
with three methods therein, called ``init_experiment()``,
``init_run()``, ``init_trial()``.

This section shows you how to write these methods.  You
might also want to take a look at the example experiments in the
subdirectory ``examples`` as a demonstration, or to use them as a
template for your own experiment.

Here, we are going to look into the example experiment
``tone_in_running_noise.py``, which is also found in the ``examples``
subdirectory of the distribution.


The experiment ``tone_in_running_noise.py`` starts with some Python
imports:

.. code::

  from pypercept.experiments import AdaptAFCExperiment
  from pypercept.order import Sequential
  from pypercept.utils import gensin, rms, hanwin
  import pypercept
  import numpy as np

The functions ``gensin``, ``rms``, and ``hanwin`` are not mandatory
but they come in handy for signal generation.   
   
You must then define a new class for your experiment.  Your class
should inherit from some of the existing pypercept classes.  This
example uses the classes ``AdaptAFCExperiment`` and ``Sequential``.  The
class ``AdaptAFCExperiment`` defines a detection or discrimination
experiment with an Alternative Forced Choice Paradigm with adaptive control of the variable.  The class
``Sequential`` defines a non-interleaved experiment, where the runs of
the experiment follow each other sequentially, with a new run
beginning after the previous run has been finished.

.. code::

  class ToneInRunningNoise(AdaptAFCExperiment, Sequential):

This class must have the above mentioned three methods, as
explained in the next three sections.
   


init_experiment()
-----------------

The method ``init_experiment()`` will be called once at the beginning of the
experiment, where a number of general properties of the experiment are
specified, like in:

.. code ::

  def init_experiment(self, exp):

    exp.description = """
    This experiment measures the detectability of a sinuoid test signal in a
    broadband noise masker as a function of sinuoid frequency and noise
    level. The noise is always a running noise, i.e. an independent noise
    realisation in all intervals and in all trials.
    """
    
    exp.add_parameter('noise_level', [-40, -60], 'dB FS',
                      'level of broad band noise masker')
    exp.add_parameter('sine_freq', [500, 1500, 3000], 'Hz',
                      'frequency of sinusoid test signal')
    exp.set_variable('sine_level', -30, 'dB FS',
                     'level of sinusoid test signal')
    exp.add_adapt_setting('1up2down',
                          max_reversals = 8, start_step = 8, min_step = 1)

    exp.num_afc = 3        # The number of AFC alternatives 

    exp.sample_rate = 48000

    exp.isi_signal  = 0.3  # Inter-Stimulus-Interval, duration of silence in s
    exp.pre_signal  = 0.3  
    exp.post_signal = 0.2



In this example, there are two stimulus parameters.  The parameter
``noise_level`` will automatically be set to the values -40 and -60, which
are specified in dB FS (dB re. fullscale).  The parameter ``sine_freq``
will automatically be set to the values 500, 1500, and 3000, specified in
Hz.  The whole experiment will then automatically consist of
:math:`2\cdot3=6` runs with all possible combinations of parameters,
-40|500; -40|1500; -40|3000; -60|500; -60|1500; -60|3000, for which the
threshold of the variable ``sine_level`` will be measured.

In each new run the ``sine_level`` will be initialized to a value of -30 dB
FS and it will be adaptively varied via the 1-up-2-down method.  The
initial step size of 8 dB will be repeatedly halved until the minimal step
size of 1 dB has been reached.  There, the measurement phase will start and
it will last until a number 8 reversals has been reached.

At the end, the three signals with the reserved names ``isi_signal``,
``pre_signal``, and ``post_signal`` are defined which will be placed
between, respectively prior or after the stimulus intervals.  If any of
these variables is defined as a scalar, then that value defines a duration
in seconds, and the variable will be automatically replaced by a
corresponding zeros-signal with that length.  If you need anything else then
silence here, you need to generate the signals yourself as
numpy-arrays. These signals may be left empty, which is the default.  


Parameter combinations
++++++++++++++++++++++

As described above, all parameter values added using
``add_parameter()`` will be combined with each other.  If you do not
want your experiment to go through all possible combinations of
all parameters, then this can be achieved by passing a list of lists
containing the corresponding parameters to ``add_parameters()`` like
in:

.. code ::

    exp.add_parameter('mod_freq',  [[4, 16, 64, 128], [64, 128, 256]],
                      'Hz', 'modulation frequency')
    exp.add_parameter('car_freq', [[800, 1600], [3200]],
                      'Hz', 'carrier frequency')


This will result in the combinations of all elements of the n-th list
of each parameter.  This also applies for any additional parameter
added.  For the above code example this will result in the following
4*2+3*1=11 combinations:

    4|800; 4|1600; 16|800; 16|1600; 64|800; 64|1600; 128|800; 128|1600; 64|3200; 128|3200; 256|3200. 


Note that also a single parameter value, like 3200 in this example,
must be a list, i.e. '[3200]' and not '3200'.   If parameter grouping is being used, then each
parameter added to this experiment must have the same number of
groups, i.e. the same number of inner lists.


Parameter order
+++++++++++++++

Note that the order in which you "add" a parameter to the experiment
makes a small difference: the parameters get numbered internally and
when you experimental data are saved (see `How your data are saved`_),
the parameters will appear in that order in the data file.  When you
plot the result data of an adaptive experiment, the variable at
threshold will be shown as a function of the *first* parameter on the
abscissa, and any further parameter(s) will result in individual data
lines.  The parameter order can however be changed later on (see
`Processing and displaying your results`_)


Parameter values
++++++++++++++++

Most often, a parameter will have a numerical value and an
accompanying unit.  The value of a parameter may however also be a
string.  In that case there might not be a corresponding unit for
such a parameter.  As an example, consider a BMLD experiment where one
parameter is the "interaural condition" which can have values like
'N0S0', 'N0Spi', and 'NpiS0'.  This would be specified as in

.. code ::

    exp.add_parameter('ia_cond',  ['N0S0', 'N0Spi', 'NpiS0'], '', 'interaural condition')

Note that the unit was specified as the empty string ``''`` here.


Parameter randomization
+++++++++++++++++++++++

By default, the different parameter values are used respectively
combined in the order as specified in the ``add_parameter()``
call(s), i.e. the first run uses the first value of parameter 1, in
combination with the first value of the further parameters if they
are present.
As an alternative, it is possible to randomize the order of the runs
by setting

.. code ::

    exp.randomize_runs = True



init_run()
----------

The method ``init_run()`` will be called automatically *once at the
beginning of each new run*.  This will happen before the first trial
with stimulus presentation.

The purpose is to define a number of properties which are specific for
the current run of the experiment, for example to set some proper
start values.  Also, any calculations that are of high computational
load but only need to be performed once, should as well be placed into
``init_run()``.

In our example, it looks like this:

.. code::

  def init_run(self, run):
    # The durations of sine and noise and window ramp, in seconds:
    sine_dur  = 0.25
    noise_dur = 0.35
    ramp_dur  = 0.05

    # The same durations in number of samples:
    run.sine_nsamp  = int(np.round(sine_dur * run.sample_rate))
    run.noise_nsamp = int(np.round(noise_dur * run.sample_rate))
    run.ramp_nsamp  = int(np.round(ramp_dur * run.sample_rate))
    
    # Pre-generate the sinusoid with RMS = 1
    run.sine1 = gensin(run.sine_freq, np.sqrt(2), sine_dur, run.sample_rate)
    # and apply Hanning window ramps
    run.sine1 = hanwin(run.sine1, run.ramp_nsamp)

    # Define the indices of start and end of the sine within the noise
    run.idx1 = int(np.round(0.5 * (run.noise_nsamp - run.sine_nsamp)))
    run.idx2 = run.idx1 + run.sine_nsamp

  
Here, the frequency of the sinusoid test signal will not change within
one run, while it will vary in amplitude from trial to trial.  The
sinusoid is therefore generated once with a fixed amplitude of
:math:`\sqrt{2}` which corresponds to an RMS of 1, respectively an
RMS-level of 0 dB FS.  It will then later only need an amplitude
scaling corresponding the desired level.  This implementation will
reduce the computational cost (slightly), because multiplication of a
number of samples with some factor is faster than the calculation of
the sine for all the samples.

In this example, the noise masker has a longer duration than the test
signal.  Therefore, the indices of the signal start and end samples
are calculated once.  They are to be used regularly in the following
method:




init_trial()
------------

The method ``init_trail()`` will be called automatically *once at the
beginning of each new trial*.  

The purpose is to generate the (new) current stimuli that are to be
presented in the current trial.  Typically, these stimuli depend on
the current value of the stimulus variable ``trial.variable``, and
most probably also on the value, respectively set of values of the
parameters.  While the stimulus variable is contained in the variable
with the fixed name ``trial.variable``, the parameters have names as
they were specified by the method ``exp.add_parameter()`` in
``init_experiment()``.  This is best demonstrated in the example code:

.. code::

  def init_trial(self, trial, run):
  
    # Generate num_afc new, independent realisations of Gaussian noise
    noise = np.random.randn(run.noise_nsamp, self.num_afc)

    # Adjust the RMS level, individually in all num_afc instances of the noise
    noise = noise / rms(noise,axis=0) * 10**(run.noise_level/20)

    # Apply Hanning window to all noises
    for k in range(self.num_afc):
      noise[:,k] = hanwin(noise[:,k], run.ramp_nsamp)

    # Now use the first num_afc-1 columns of noise as reference signals 
    # and the last column of noise for the test signal

    # ----- This works for num_afc = 3, hard-coded:
    trial.reference_signal = []
    trial.reference_signal.append(noise[:,0])
    trial.reference_signal.append(noise[:,1])
    trial.test_signal = noise[:,2]

    # Scale the pre-generated sinusoid signal in amplitude and add it to the noise
    trial.test_signal[run.idx1:run.idx2] += run.sine1 * 10**(trial.variable/20)




The generation of the noise can thus use some values that were
pre-calculated and explicitly defined as members of the current run in ``init_run()``, for example
``run.noise_nsamp`` and ``run.ramp_nsamp``.  On the other hand,
``run.noise_level`` exists because it is one of the parameters which were
defined in ``init_experiment()`` as members of ``exp``.  The current ``run``
has automatically inherited the necessary properties from the experiment
``exp``.  Remember that the stimulus variable will change from trial to
trial, i.e., it is specific to the trial, not to the run.  As mentioned
above, it is therefore referred to by ``trial.variable`` rather than by its
name 'sine_level' as it was specified by ``exp.set_variable('sine_level',
-20, 'dB', 'level of sinusoid test signal')`` in ``init_experiment()``.  In
the last line, the pre-calculated ``run.sine1`` is thus scaled in amplitude
according to the desired level in ``trial.variable``, and then added
to the noise at the pre-calculated indices.

The stimuli ``trial.test_signal`` and ``trial.reference_signal`` generated by
``init_run()`` will then be taken for further use, normally for sound output.


  
Note that ``init_trial()`` has two input arguments, namely ``trial`` and
``run``, while ``init_run()`` has only one argument ``run``.  The reason is
further explained in section `Signal inheritance`_
  


Signal generation
-----------------

In the end, the goal of the three methods ``init_experiment()``,
``init_run()``, and ``init_trial()``, is to provide/generate the following
signals/stimuli with reserved names:

:pre_signal:  

   The signal at the very beginning of the output signal.  This can for
   example be an empty signal, or a zeros signal, i.e. silence, or an
   acoustic marker.
   This signal defaults to an empty signal, if it is  not specified by the user.  

:post_signal:

   The signal at the very end of the output signal.
   This signal defaults to an empty signal, if it is not specified by the user.
   
:isi_signal:  

   The Inter-Stimulus-Interval signal which is placed *between* all successive
   test resp. reference signals.  This can for example be a silence
   signal.
   This signal defaults to an empty signal, if it is not specified by the user.  

:reference_signal:  

   The signal resp. list of signals that do *not* contain that certain
   'cue' which the subject is required to detect.  

:test_signal:  

   The signal which *does* contains that certain 'cue' which the subject is
   required to detect, contrary to the reference signal which does not.


These signals are then used internally to mount together the complete
output signal, depending on the selected type of experiment (AFC or
Matching). 

It is possible, but not mandatory, to specify a

:background_signal:

   This is a background signal which is to be played during the entire
   duration of the trial, i.e. from the start of the pre_signal until
   the end of the post_signal.  This can for example be a background
   noise.  See below in section  `Semi-continuous
   background signal`_ for more details. 




Signal inheritance
++++++++++++++++++

|pyper| provides a mechanism called **signal inheritance** which means, that
signals will be inherited from stage to stage. Let's have a look at the three
stages ``init_experiment()``, ``init_run()`` and ``init_experiment()``. In each of
them you have access to the above |pyper| signals as attributes. For example, in
``init_experiment()`` you can set the ``pre_signal`` via
``exp.pre_signal = ...``.
The other stages ``init_run()`` and ``init_trial()`` also have the attribute
``pre_signal`` and inherit the values from parent stages if already set.  That
means, ``run.pre_signal`` and ``trial.pre_signal`` will automatically be set to the
value of ``exp.pre_signal`` defined in ``init_experiment()``.  This concept allows
comfortable and memory efficient signal generation. Let's say, you want to design
an experiment in which pre_signal, isi_signal and post_signal should be the same in all
trials.  Then simply define them in ``init_experiment()``. If your reference signal
should be the same not for the whole experiment but in every trial of one run,
then you can define your ``reference_signal`` in ``init_run()``. For
more inspiration see the example experiments.


Mono, stereo, multichannel
++++++++++++++++++++++++++

In |pyper| the experimenter is responsible for generating valid
signals. That means, all signals must have the same shape in order to allow
for automatic concatenation.  For simplicity we suggest to build your
signals as numpy arrays with shape ``(NUM_SAMPLES, NUM_CHANNELS)``. In case
all your signals are diotic (left and right channels are identical), there
is one exception from this rule, because you can build your signals as
numpy arrays either with shape ``(NUM_SAMPLES,)`` or ``(NUM_SAMPLES,1)``.  Then
these single vectors will be copied to all channels that your playback
system has.



Zero signals
++++++++++++

In most cases `pre_signal`, `isi_signal` and `post_signal` will be only
zero signals of a given length to control the  duration of the silent
pause between the presented test or reference signals. Therefore
|pyper| provides a comfortable way to define them.
To generate a zero signal with length `0.3s` you can write:

``pre_signal = 0.3`` which is automatically transformed into
``np.zeros(0.3*sample_rate)``

But there is also an extended syntax to define multi channel zero signals.

``pre_signal = (0.3, NUM_CHANNELS)`` leads to
``np.zeros((0.3*sample_rate, NUM_CHANNELS))``

 

Semi-continuous background signal
+++++++++++++++++++++++++++++++++

If a continuous background signal is to be presented throughout a trial, it can
be assigned to ``background_signal`` in any of the three init stages.
If ``background_signal`` is defined, |pyper| applies it the to the signals/stimuli 
before presentation of each trial.   

The background signal must be a single numpy-array and its size must match 
the total size of the signals/stimuli for a given trial, i.e.

``(NUM_FRAMES_TRIAL, NUM_CHANNELS)``,

where ``NUM_FRAMES_TRIAL`` is the total number of samples for one channel. In case of 
an :math:`n`-AFC experiment, the following frame lengths need to be
added up to yield the length of ``background_signal``:   

- ``pre_signal``
- ``post_signal``
- ``isi_signal`` · (:math:`n`-1)
- ``reference_signal`` · (:math:`n`-1)
- ``test_signal``


Start your experiment
---------------------

At the end of your code, the experiment is then started by calling
pypercept's ``start()`` function with the class name as its argument
as in

.. code::

  if __name__ == '__main__':
    pypercept.start(ToneInRunningNoise)



Once you have set up your experiment file correctly, running your
experiment means running that Python file, like in 

.. code::

  $ python tone_in_running_noise.py


You should instruct your subjects to always identify themselves by
the identical user ID, for example, their initials, which should not
contain any white-spaces.  All data gathered from a subject will be saved
in a cumulative fashion, on a one-separate-file-per-subjectname basis, see
section `How your data are saved`_

The subjects can run the experiment at their own pace.  They may answer in
one of the ways described in section `Test subject interface`_,
as specified by the experimenter.

Analysis of your data can be performed at any later time,
independently from the subject performing the measurement.


Commandline flags
+++++++++++++++++

Invoking the experiment on the commandline may use the following flag:

    [-u]: Defines the user interface. 
      - ``-u terminal`` starts the experiment with a text based user
        interface (TUI) on the commandline.  This is the default.
      - ``-u gui``  starts the experiment with a graphical user
	interface (GUI).  The GUI is presented through your local browser. 
      - ``-u egui``  gives you an IP address with which the experiment can be performed
        on another device by calling that IP address in an internet browser. Your
        computer and the other device have to be in the same network for this!
      - ``-u model``  starts the experiment using a model to calculate
	the answer to the stimuli, instead of asking a human
	listener.  See `Hooking in an auditory model for simulations`_
      
      If this flag is not set, the default (TUI) is used. 

    [-a]: Defines the audio output. 
        - ``-a 1``: Sound from internet browser; default with ``-u egui``
        - ``-a 2``: Sound from internet browser and Python (host)
        - ``-a 3``: Sound from Python (host); default with ``-u gui``
      
      This flag is only available with ``-u gui`` and ``-u egui``.
      The text based interface (``-u terminal``) uses only Python for audio output. 


Persistent server mode
++++++++++++++++++++++

|pyper| offers a special `server` mode which lets you perform one or
several experiments at the same time on different devices.  The idea
is to have several test subjects perform an experiment simultaneously
(but asynchronously and independently), where each subject only needs
a web browser on their local device.

To start the server you have to be in the directory
``YOUR_PYPERCEPT_PATH/pypercept/`` or adjust the path of the call to
``server.py``, e.g. by calling

.. code ::
 
 python pypercept/server.py

The server will then display a URL which can be typed or pasted in a
web browser on devices on the same network.  The server will
list/serve all experiments that are found in the directory
``examples/`` of your local pypercept installation.  You can change
this default directory by adding a commandline argument which
specifies your desired directory, like in

.. code ::

  python pypercept/server.py  my_directory_name


Note that a server started like this (as opposed to starting a
specific experiment using the commandline flag `-u egui`, see in
section `Commandline flags`_) stays up after finished experiments and
has to be manually terminated.


Need to go *up* after *correct* responses?
------------------------------------------

In normal adaptive methods, like 1-up-2-down etc., the stimulus
variable goes down (is reduced) after one ore more correct answers,
thereby making the task more difficult for the test subject.
Likewise, the stimulus variable goes up (is increased) after one or
more wrong answers, thereby making the task easier for the test
subject.  A classical example for this is the detection of a test tone
in a noise masker, where the masker level is kept fixed and the tone
level is the stimulus variable.  When the tone level goes up after
wrong answers it should get easier to detect the tone.

Sometimes, however, the opposite up/down-behaviour is necessary: The
stimulus variable should go up after correct responses and it should go
down after wrong responses.  An example of this is the same test of
tone detection in a noise masker like above, but with a fixed tone
level and the stimulus variable being the masker level.  After correct
answers, the masker level should go up to make the task more
difficult.  

This reversed up/down behaviour can be specified in |pyper| by setting the
flag variable ``reversed_up_down`` to ``True``.  Additionally to this, your
step size ``start_step`` and your minimal step size ``min_step`` both have to
take *negative* values, like in the following code example:

.. code ::

   exp.add_parameter('sine_level', [-30, -40], 'dB')
   exp.set_variable('noise_level', -40, 'dB')
   exp.add_adapt_setting(adapt_method = '1up1down', reversed_up_down = True,
                         start_step = -8, min_step = -1)


Using this code, the *absolute size* of initial steps will be 8 dB (but in
opposite direction compared to the normal case), and the step size
will be halved successively after each second reversal until it
reaches the minimal *absolute size* of 1 dB.


Custom responses in matching experiments
-----------------------------------------

The default matching experiment defines `u` (up) and `d` (down) in
``user_answers`` as the two possible responses.  These responses would
be the natural choice in experiments to measure, for example, loudness
matching or frequency/pitch matching, where the task of the subject is
to adjust the one interval more up or down such that it matches
the other interval.

``eval_response()`` of ``class AdaptMatchingExperiment`` in
``experiments.py`` then evaluates `u` to ``Result.UP`` and `d` to
``Result.DOWN`` and assigns that to ``trial.result``.  Note that
``Result.UP`` is identical to ``Result.WRONG``, and ``Result.DOWN`` is
identical to ``Result.CORRECT``, as defined in ``class Result`` in the
file ``result.py``.  That's because *this* correspondence fits to the
behaviour of the adaptive algorithms (see ``adapt.py``) used to
calculate the run's variable for the next trial: The adaptive variable
goes down after one or more correct responses, and correspondingly up
after wrong responses.

If an experiment requires different responses (e.g. (l)eft & (r)ight) and/or 
a different mapping of responses to the result of a trial for the desired 
adaptation, this can be defined in the experiment.

Possible responses can be set by assigning a string containing valid 
characters to ``exp.user_answers`` in ``init_experiment()``, e.g. 


.. code::

    exp.user_answers = 'lr'


These answers then need to be mapped to one of the valid results of a
trial, i.e. one of ``Result.DOWN``, ``Result.UP``,
resp. ``Result.CORRECT``, ``Result.WRONG``, or ``Result.UNDECIDED``,
depending on the desired outcome of a given response.  This must be done
by implementing and overwriting the method ``eval_response()`` as part
of your own experiment, which can be as simple as


.. code::

    def eval_response(self, run, trial, user_response):
        
        trial.user_response = user_response

        if user_response == 'l':
            trial.result = Result.CORRECT
        elif user_response == 'r'
            trial.result = Result.WRONG

        run.trials.append(trial)


Note that ``user_response`` must be assigned to the appropriate attribute of 
the trial `(`trial.user_response``) and that the trial needs to be appended to 
the list of trials of the current run (``run.trials``).


Test subject interface
----------------------

|pyper| offers several interfaces for the test subject.  One simple
interface is to answer via a keyboard.  Another is to provide a
graphical user interface to be controlled via a mouse and/or key
presses.  The former has the advantage that an extra monitor for the
listening booth can be omitted.  The latter may be more informative
for the subject and can provide some extra functionality,

The text-based user interface using the keyboard on a
commandline/shell is the default behaviour of |pyper|.  

In an :math:`n`-AFC test, a subject answer of :math:`1, 2, \ldots, n`
indicates the subject's decision for interval :math:`1, 2, \ldots, n`.
These answers can be given by the keyboard (followed by ENTER).  

Additionally, the following subject answers are possible which carry special
meanings:

======  ==================================================
Answer  Meaning
======  ==================================================
'n'     Terminate the current run.
'q'     Terminate the whole experiment.
'g'     Toggle debug status.
        This is only possible if debugging is allowed,
        by setting ``exp.allow_debug = True``
'v'     Forced correct answer.
        This is only possible if ``exp.debug`` is ``True``
'x'     Forced wrong answer
        This is only possible if ``exp.debug`` is ``True``
======  ==================================================



The graphical user interface is invoked by an additional commandline
option like in ``python tone_in_running_noise.py -u gui``.  This
starts a local webserver and uses the web browser to display the GUI.  


The amount of information and feedback that is provided to the subject is
controlled by these attribute variables:

.. ========================  ============================================================
.. Variable                  Meaning
.. ========================  ============================================================
.. ``exp.feedback``          if ``True``, then feedback is given to the subject, e.g.
..                           whether or not the last answer was correct.
.. ``exp.info``              if ``True``, then additional information is provided to
..                           the subject, for example, about parameters of next run,
.. 	                     etc.
.. ``exp.debug``             if ``True``, then additional information for debugging
..                           is provided.  This should not be used during productive
..                           measurement with test subjects.
.. ``exp.visual_indicator``  if ``True``, then the different stimulus intervals will be
..                           visually marked by a flashing color.  This is only
.. 		                       possible when using the GUI.   
.. ========================  ============================================================


:``exp.feedback``:

  If ``True``, then feedback is given to the subject, e.g. whether or
  not the last answer was correct.
	
:``exp.info``:

  If ``True``, then additional information is provided to the subject,
  for example, about parameters of next run, etc.
       
:``exp.visual_indicator``: 

  If ``True``, then the different stimulus intervals will be visually
  marked by a flashing color.  This is only possible when using the GUI.
   
:``exp.debug``:

  If ``True``, then additional information for debugging is
  provided.  This should not be used during productive measurement with test subjects.

:``exp.debug_plot_signal``:

  If ``True``, then plot the current output signal of each trial.
  This must not be used during productive measurement with test subjects.
   



Setting up interleaved tracks experiment
----------------------------------------

Designing an interleaved-tracks experiment is rather easy in |pyper|.  Most
things are just same as for a single-run non-interleaved experiment.
However, you must keep in mind how attributes and signals are inherited
from the experiment to a run and from a run to a trial.

|pyper| sets up an internal list of all runs that are to be measured during
the experiment.  In case of an interleaved experiment, |pyper| will
*randomly* select one run from the list of all unfinished runs, and will then
call ``init_trial()`` which inherits from that chosen run.  This is the main
difference compared to a
sequential experiment, where |pyper| will always choose the *same* (current)
run from the list of all runs, until that run is finished.  


Equalisation filter for your transducer
---------------------------------------

If you need to equalise the transfer function of your headphone/acoustic
transducer, this can easily be achieved in |pyper|.  You only need to
specify the filter coefficients :math:`\vec{a}` and :math:`\vec{b}` which
will be used for a digital linear filter as implemented in the function
``scipy.signal.lfilter()``.  The filter coefficients are specified by
assigning the two variables ``eqfilter_a`` and ``eqfilter_b`` as in:

.. code::

   exp.eqfilter_a = [1, -0.9]  # The array of the a coefficients
   exp.eqfilter_b = [0.1, 0]   # The array of the b coefficients 


The filtering will only take place when both variables aren't empty.  Not
defining these variables explicitly in your experiment will leave them at
their default value ``[]``, meaning that no filtering is performed.

It is beyond the scope of |pyper| to perform the measurement of the
transfer function of your acoustic equipment and to design the
corresponding equalisation filter.  This task is left to the talented
experimenter.  The community is however encouraged to contribute to |pyper|
by sharing their equalisation filter coefficients for commonly used
headphones on commonly used headphone couplers.  These will then be
compiled into a convenient module with predefined filter sets to choose from.



Debugging your experiment
-------------------------

For the purpose of debugging, set ``exp.debug = True`` and/or
``exp.debug_plot_signal = True`` in order to inspect the current stimuli
and the subject's answers.  


When gathering real data with test subjects, however, you will want to
set these variables to ``False``.  


Troubleshooting
---------------

Plotting
++++++++

Display of subject data and stimuli is handled by `matplotlib`. Depending on the 
operating system and/or python configuration, a fitting backend needs to be 
set to properly display the data. If there's an issue like for example
an empty figure-window, try configuring a different backend using
environment variables or ``matplotlibrc``. More information can be found at 
`<https://matplotlib.org/stable/users/explain/figure/backends.html>`_.  



Audio and playback issues
+++++++++++++++++++++++++

|pyper| uses the Python library 'soundcard' as the default for audio
output.  There have been reports of crackling, choppy, or otherwise
faulty stimulus output on a few Windows systems when using
'soundcard'. There also has been an issue with an Apple MacBook
supporting only very specific (and odd) blocksizes. Should you
encounter any problems with audio output, try the following:

  - Change the blocksize for stimulus playback by setting
    ``exp.audio_blocksize`` in the ``init_experiment()`` function of
    your experiment to a value different from its default (8192).
    Experimenting with different values may solve your problem.
  - Alternatively, switch the audio library to `sounddevice` by
    specifying ``exp.audio_lib = 'sounddevice'`` in
    ``init_experiment()`` of your experiment.  This will change the
    Python library use, which is `soundcard` by  default.
  - You can change the playback device by setting ``exp.device`` to the
    desired device-ID or name. You can use the short function ``output_devices.py``
    supplied with |pyper| to display all devices available for
    ``soundcard`` on your local machine. When using ``sounddevice``,
    calling ``python -m sounddevice`` in a shell will achieve the same.
  - When using the GUI, try to use the browser for stimulus playback (see section on 
    `commandline flags`_ above). 

