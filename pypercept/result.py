"""This file is part of pypercept:
a package for developing psychoacoustical listening experiments.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
"""

from enum import Enum

class Result(Enum):
    """
    Defines possible results for experiments
    """
    CORRECT = 1
    DOWN = 1

    WRONG = 2
    UP = 2

    UNDECIDED = 3
    FORCED_CORRECT = 4
    FORCED_WRONG = 5
