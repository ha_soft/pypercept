# Makefile for pypercept
#
# Author:  Martin Hansen 
# Date:    22 Aug 2023
#


VERSION = $(shell cat VERSION)
TAGNAME = pypercept-$(VERSION)

# These files are the central part of the pypercept distribution
PYFILES = DISTRIBUTION NO-WARRANTY LICENSE GNU-GPL CHANGELOG.md \
     Makefile setup.py pypercept/*.py \
     pypercept/gui/*.html pypercept/gui/*.py pypercept/gui/script.js

# The file names for the compiled documentation files
DOCU_ZIP_SINGLEHTML = pypercept-$(VERSION)_singlehtml.zip
DOCU_ZIP_HTML = pypercept-$(VERSION)_html.zip
DOCU_PDF = pypercept-$(VERSION).pdf

# These files are part of the pypercept documentation
DOCFILES = README.rst docs/*.rst docs/*.py docs/Makefile docs/make.bat \
      docs/$(DOCU_PDF) docs/$(DOCU_ZIP_HTML) docs/daga2024-proceedings/daga2024_proceedings_pypercept.pdf

# These files contain example experiments
EXAMPLES = examples/abs_threshold_tones.py \
           examples/sam_sincarrier_detect.py \
           examples/jnd_frequency.py \
	   examples/jnd_intensity.py \
           examples/match_freq_binaural_intrlv.py \
           examples/tone_in_running_noise.py \
           examples/tone_in_running_noise_intrlv.py \
           examples/tone_in_running_noise_sim.py

# These files go public in the normal pypercept distribution
DISTFILES = $(PYFILES) $(EXAMPLES) $(DOCFILES)

#check_version:
#	#./check_version.sh

distribution: docu dist zip

docu:
	cd docs; $(MAKE) html;  $(MAKE) singlehtml;  $(MAKE) latex; $(MAKE) latexpdf;
	cd docs; cp _build/latex/pypercept.pdf .; cp pypercept.pdf $(TAGNAME).pdf;
	cd docs/_build; zip -r $(DOCU_ZIP_SINGLEHTML) singlehtml/* ; mv $(DOCU_ZIP_SINGLEHTML) .. ; cd ../..
	cd docs/_build; zip -r $(DOCU_ZIP_HTML) html/* ; mv $(DOCU_ZIP_HTML) .. ; cd ../..

dist: docu
	tar cvfz $(TAGNAME).tar.gz  $(DISTFILES)

zip: docu
	if [ -e $(TAGNAME).zip ]; \
	then \
	  rm $(TAGNAME).zip ; \
	fi ; \
	zip $(TAGNAME).zip   $(DISTFILES)


# End of Makefile

