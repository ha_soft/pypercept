"""This file is part of pypercept:
a package for developing psychoacoustical listening experiments.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
"""

"""This module implements pypercept's server mode.  It starts a
webserver on the local machine.  Several remote users can select an
experiment each through a small webpage which delivers both the audio and
acts as the GUI for the subject's answers.
"""

import pypercept.exception as excpt
from tornado.websocket import WebSocketHandler
from tornado.web import Application, RequestHandler
import tornado.web
from tornado.ioloop import IOLoop
import ast
import uuid
import re
import os
import json
import time
import sys
import struct
import socket
from pypercept.result import Result
from pypercept.save import json_export

def to_bytes(n):
    return struct.pack("@i", n)

class PyperceptServer(Application):

    def __init__(self, path_to_exps=None, is_persistent=False):
        if path_to_exps:
            self.eh = ExperimentHandler(path_to_exps = path_to_exps, is_persistent = is_persistent)
        else:
            self.eh = ExperimentHandler(is_persistent = is_persistent)
        
        dirname = os.path.dirname(__file__)
        settings = {
            'static_path': os.path.join(dirname, 'gui'),
            'template_path': os.path.join(dirname, 'gui'),
        }
        handlers = [
            (r"/", IndexHandler, dict(eh=self.eh)),
            (r"/select/", SelectHandler, dict(eh=self.eh)),
            (r"/pypercept/(.*)", EchoWebSocket, dict(eh=self.eh)),
            (r"/active/", ActiveHandler, dict(eh=self.eh))
        ]

        Application.__init__(self,handlers, **settings)

class SelectHandler(RequestHandler):

    def initialize(self, eh):
        """ makes experiment available for websocket server

        This method makes available the functions that belong to the
        :class:`Experiment` class.

        Parameters
        ----------
        exp : :class:`Experiment`
        """
        self.__eh = eh
    
    def get(self):
        try:
            cid = self.get_argument("cid")
        except tornado.web.MissingArgumentError:
            self.redirect('/')
        etype, var = self.__eh.get_experiment_type(cid)

        if isinstance(var, str) and var in 'vh':
            filename = f'{etype}_{var}_gui.html'
        else:
            filename = f'{var}_{etype}_gui.html'
        
        self.render(filename, clientid=cid)


class ActiveHandler(RequestHandler):
    
    def initialize(self, eh):
        """ makes experiment available for websocket server

        This method makes avaible the functions that belong to the
        :class:`Experiment` class.

        Parameters
        ----------
        exp : :class:`Experiment`
        """
        self.__eh = eh

    def get(self):
        self.render('active.html', active=self.__eh.exps)


class ExperimentHandler():

    def __init__(self, path_to_exps=os.path.abspath(os.path.join(
            os.path.dirname(__file__),"../examples")), is_persistent=False):
        self.exps = {}
        self.available_exps = {}
        self.exp_path = path_to_exps
        sys.path.insert(0,self.exp_path)
        self.is_persistent = is_persistent
        # when server is started w/o experiment specified, get all experiments in path_to_exps
        # otherwise, Gui.start() is loading teh one specified.
        if is_persistent:
            self.load_experiments()
        del sys.path[0]

    def load_experiments(self):
        for root, dirs, files in os.walk(self.exp_path, topdown=False):
            for name in files:
                if name[-3:] == ".py":
                    e_name = name[:-3]
                    self.available_exps[e_name] = __import__(e_name, locals(), globals())


    def get_experiment_names(self):
        return self.available_exps.keys()


    def add_existing_experiment(self, exp, debug, audio):
        cid = uuid.uuid4().hex
        if cid not in self.exps:
            self.exps[cid] = {}
            self.exps[cid]['exp'] = exp
            self.exps[cid]['debug'] = debug
            self.exps[cid]['audio'] = audio
            self.exps[cid]['started'] = False
        return cid

    def get_experiment_type(self,cid):

        if hasattr(self.exps[cid]['exp'], 'test_position'):
            if not self.exps[cid]['exp'].ui_layout in 'vh':
                print('Error: invalid layout for matching experiment, check ui_layout') 
            else:
                return 'matching', self.exps[cid]['exp'].ui_layout
        elif hasattr(self.exps[cid]['exp'], 'num_afc'):
            if self.exps[cid]['exp'].forced:
                return 'afc', self.exps[cid]['exp'].num_afc
            else:
                return 'auc', self.exps[cid]['exp'].num_afc
        else: 
            print('Error: cannot determine experiment type')

    def remove_client(self, cid):
        print('Remove client with id:',cid)
        del self.exps[cid]
        if not self.exps and not self.is_persistent:
            print('Stopping server...')
            IOLoop.instance().stop()
        else:
            print('No active experiments running...')

    
class IndexHandler(RequestHandler):
    
    def initialize(self, eh):
        self.__eh = eh

    def get(self):
        try:
            name = self.get_argument("name")
        except tornado.web.MissingArgumentError:
            exps = self.__eh.get_experiment_names()
            self.render('index.html', experiments=list(exps))
            return
                
    def post(self):
        name  = self.get_argument('experiment')
        try:
            debug = self.get_argument('debug')
            deb = True
        except:
            deb = False
        #print('debug:',deb)
        module = self.__eh.available_exps[name]
        #classname = self._to_camelcase(name)
        classname = self.get_class_name(module.__file__)
        exp = getattr(module, classname)()
        cid = self.__eh.add_existing_experiment(exp,deb,1)
        self.redirect('/select/?cid='+cid)

    def get_class_name(self, path):
        """Extracts the names of top-level classes in a Python file and returns the first one."""
    
        with open(path, 'r', encoding='utf-8') as f:
            data = f.read()

        # Parse the file contents to an Abstract Syntax Tree (AST)
        tree = ast.parse(data)

        # Extract class names
        class_name = [node.name for node in ast.walk(tree) if isinstance(node, ast.ClassDef)]

        return class_name[0]

    def _to_camelcase(self, s):
        e = re.sub(r'(?!^)_([a-zA-Z])', lambda m: m.group(1).upper(), s)
        return e[0].upper()+e[1:]
            
            
class EchoWebSocket(WebSocketHandler):
    """This class builds a websocket server and handles the incoming and outgoing
    events from and to the GUI.

    Parameters
    ----------
    WebSocketHandler : :class:`WebSocketHandler`

    """
    
    def initialize(self, eh):
        """ makes experiment available for websocket server

        This method makes avaible the functions that belong to the
        :class:`Experiment` class.

        Parameters
        ----------
        exp : :class:`Experiment`
        """
        self.__eh = eh
        self.exp = None
        self.terminated = False
        self.run = None
     
    def open(self, client_id):
        print("WebSocket opened")

        self.cid = client_id

        # get experiment
        self.exp = self.__eh.exps[self.cid]['exp']
        self.debug = self.__eh.exps[client_id]['debug']
        self.audio_flag = self.__eh.exps[client_id]['audio']

        # apply UI configuration
        self.config_ui()

        if self.__eh.exps[client_id]['started'] == False:
            if hasattr(self.exp, 'description'):
                self.write_message({'type':'desc', 'content':self.exp.description})
            if self.exp.allow_debug:
                self.write_message({'type': 'debug_state', 'content': self.exp.debug})
            self.write_message({'type':'name','content': self.exp.subject_name})
            self.write_message({'type':'feedback','content': 'To start, click "start experiment"'})
        self.__eh.exps[client_id]['started'] = True
        self.write_message({'type': 'allow_plot', 'content': self.exp.allow_debug})
  
    def config_ui(self):
        """  configure UI

        This allows to configure the GUI using experiment settings, e.g. user 
        responses and button text. 
        """
        self.write_message({'type':'config_answers', 'content':self.exp.user_answers})
        if hasattr(self.exp, 'test_position'):
            self.write_message({'type':'config_buttons', 'content':(self.exp.ui_blink_label + self.exp.ui_response_label)})

    def on_message(self, message):
        """ sends and receives signals to and from the websocket client

        This method is the main method of a GUI-aided experiment, as it analyses the
        incoming events. It receives button clicks and executes the respective
        code. In this way an answer is set, a new run is started or the experiment
        gets quit. 

        Parameters
        ----------
        message : JSON struct

        Returns
        -------
        no return arguments, but sends messages to the client
        """
        ans_type = json.loads(message)['type']
        user_reply = json.loads(message)['content']

        if ans_type == "start_signal": # i.e. start experiment
            self.write_message({'type':'feedback','content': ' '})
            self.write_message({'type':'params',  #'params' for correct printing pos
                                'content':'starting experiment ...'})
            print(f'  ----- start experiment {self.exp.cls} for subject {self.exp.subject_name}')
            time.sleep(2)
            #self.write_message({'type':'feedback', 'content':' '})
            self.run, self.trial = self.present_next_trial()
        
        elif ans_type == 'answer':
            if not self.run:
                return

            self.exp.eval_response(self.run, self.trial, user_reply)

            # TODO where is this caught?
            self.send_message('audio', 'clear')

            # feedback for matching experiments
            if self.exp.feedback and hasattr(self.exp, 'test_position'):
                    self.write_message({'type':'feedback', 'content':self.trial.user_response})

            # feedback for AFC-experiments
            elif self.exp.feedback and hasattr(self.exp, 'num_afc'):
                if self.trial.result == Result.UNDECIDED:
                    self.write_message({'type':'feedback', 'content':'undecided'})
                elif self.trial.result == Result.CORRECT:
                    self.write_message({'type':'feedback', 'content':'correct'})
                else:
                    self.write_message({'type':'feedback', 'content':'not correct'})

            time.sleep(1)                  # show feedback message for 1 sec
            self.write_message({'type':'feedback', 'content':' '}) # del msg

            try:
                self.exp.adapt(self.run)
            except excpt.RunFinishedException:
                path = json_export(self.run)
                self.write_message({'type': 'feedback', 'content':
                                    'Run saved as %s.' % path})
                self.write_message({'type':'feedback',
                                    'content':'Run finshed'})
                self.write_message({'type':'run_finished',
                                    'content':'run_finished'})
                return 
            except excpt.RunStartMeasurement:
                self.write_message({'type': 'feedback',
                                    'content': 'start measurement phase'})
                # FIX too short, maybe permanent display?
                time.sleep(1)                  # show feedback message for 1 sec
                self.write_message({'type':'feedback', 'content':' '}) # del msg
                    
            self.run, self.trial = self.present_next_trial()
                
        elif ans_type == "next_run":
            self.exp.skip_run(self.run)
            self.send_message('feedback', 'Next run started')
            time.sleep(3)                      # show feedback message for 3 sec
            self.send_message('feedback', ' ') # del. feedb. msg
            
            try:
                self.run, self.trial = self.present_next_trial()
            except StopIteration:
                self.on_message(json.dumps({"type": 'quit',
                                            "content": 'save'}))
                
        elif ans_type == "quit":
            print(f'  -----  quit experiment {self.exp.cls} for subject {self.exp.subject_name}')
            self.write_message({'type':'params', 'content': ''})
            self.write_message({'type':'task', 'content': 'Experiment aborted,'})
            self.send_message('feedback', 'this browser/tab can be closed.')
            self.__eh.remove_client(self.cid)
            self.write_message({'type': 'quit', 'content': 'all_done'})

            
        elif ans_type == 'name':
            self.exp.subject_name = user_reply
       
        elif ans_type == 'cancel':
            pass

        elif ans_type == 'debug':
            if self.exp.allow_debug:
               self.exp.debug = not self.exp.debug
               state = "on" if self.exp.debug else "off"
               self.write_message({'type':'feedback',
                                   'content':"Debugging is '%s' now" % state})
            else:
               self.write_message({'type': 'feedback',
                                   'content': 'Debugging not allowed'})

        elif ans_type == 'terminate':
            self.terminated = True

    def send_message(self, msg_type, content, data=None):
        """Send a message.
        Arguments:
        msg_type  the message type as string.
        content   the message content as json-serializable data.
        data      raw bytes that are appended to the message.
        """

        if data is None:
            try:
                self.write_message(json.dumps({"type": msg_type,
                                           "content": content}).encode())
            except:
                pass
        else:
            header = json.dumps({'type': msg_type,
                                 'content': content}).encode()
            # append enough spaces so that the payload starts at an 8-byte
            # aligned position. The first four bytes will be the length of
            # the header, encoded as a 32 bit signed integer:
            header += b' ' * (8 - ((len(header) + 4) % 8))
            # the length of the header as a binary 32 bit signed integer:
            prefix = to_bytes(len(header))
            try:
                self.write_message(prefix + header + data, binary=True)
            except:
                pass
        
    def on_close(self):
        print("WebSocket closed")

    def check_origin(self, origin):
        return True
        
    def present_next_trial(self):
        
        run = self.exp.next_run()
        # is this a new run (trials attribute of run is empty)?
        if not len(run.trials):
            # Further prompts? Task and parameters are updated below...  
            self.exp.next_run().started = time.strftime('%d-%b-%Y__%H:%M:%S')
        
        if self.exp.info:
            if self.exp.order_type == 'interleaved':
                string = ' '
            else:
                string = run.get_param_string()
            self.write_message({'type':'params', 'content': string})
        trial, signals = self.exp.next_trial(run)
        self.write_message({'type':'task', 'content':trial.task})

        # Prepare/preprocess signal
        signals = self.exp.audio.preprocess_signals(signals)

        # Present the signals acoustically
        self.exp.audio.present_signal(signals, handler = self)
        
        return run, trial

                    
if __name__ == '__main__':
    if len(sys.argv) <= 1:
        # Without additional commandline argument, use the default directory 
        app = PyperceptServer(is_persistent = True)
    else:
        # If present, take the commandline argument as the pathname where
        # to look for the experiments.  All Python files in that directory
        # must be well-functioning pypercept experiments.
        exp_path = os.path.abspath(os.path.join(os.path.dirname(__file__), sys.argv[1]))
        print(f'  serving all experiments in {exp_path} \n')
        app = PyperceptServer(path_to_exps=exp_path, is_persistent = True)
    port = 8889
    app.listen(port)
    your_ip =  socket.gethostbyname(socket.gethostname()+'.')
    print('Type http://'+your_ip+""":8889 in your browser to connect to the experiment.
    \n(Server and client must be on the same network.)""")
    IOLoop.instance().start()
