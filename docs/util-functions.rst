.. util-functions.rst


Utility functions in pypercept
==============================

|pyper| comes with a few utility functions placed in ``pypercept.utils``
which can be handy for signal generation or analysis:


:gensin:  Generate a sinusoidal tone.

:hanwin:  Window/gate a signal with Hanning window shaped ramps of variable length.

:fft_rect_filt:  Filtering in the spectral domain via FFT with a
		 rectangular shape transfer function.

:rms:   Calculate the root mean square.

:gammatone_filter:  Filtering using a gammatone filter.

:gammatone_crit_filter: Filtering using a gammatone filter with a
			bandwidth equal to the critical auditory bandwidth.

:gammatone_crit_filterbank: Filter bank of gammatone filters with
			    critical auditory bandwidth with
			    center frequencies spaced equidistantly on
			    the auditory band rate frequency scale.
   
:erbn_equidist_centerfreqs: Calculates center frequencies being equidistantly 
			    spaced on the auditory band rate frequency scale.

   
Functions useful for auditory modeling are placed separately in
``pypercept.model-utils``:

:adaptloop: Nonlinear adaptation loops by Püschel (1988) which are
	    used in several models since the one of Dau et al. (1996).

:modulation_lowpass: A standalone lowpass filter as was used in the
		     model by Dau et al. (1996).

:modfilterbank: A modulation filterbank as proposed for the model of
		modulation perception by Dau et al. (1997).

:ihc_envelope: Simple model for the transduction of the inner hair
	       cells consisting of half-wave rectification and lowpass
	       filter. 

:internal_representation: Calculation of the "internal representation"
			  of a stimulus.

:optdet: Calculation of the "optimal detector" as used in several models since the one of
         Dau et al. (1996)

.. End of file
