"""This file is part of pypercept:
a package for developing psychoacoustical listening experiments.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
"""

# Exception classes
class RunAbortException(Exception):
    def __init__(self, arg=""):
        # Set some exception infomation
        self.msg = arg

class RunFinishedException(Exception):
    def __init__(self, arg=""):
        self.msg = arg

class WrongAnswerFormat(Exception):
    def __init__(self, arg=""):
        self.msg = arg

class ExperimentAbortException(Exception):
    def __init__(self, arg=""):
        self.msg = arg

class ToggleDebugException(Exception):
    def __init__(self, arg=""):
        self.msg = arg

class RunStartMeasurement(Exception):
    def __init__(self, arg=""):
        self.msg = arg
