"""This file is part of pypercept:
a package for developing psychoacoustical listening experiments.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
"""

"""This module contains the class for audio output of the generated
stimuli.  The user can select to use the Python library 'soundcard' or
'sounddevice', using the variable 'experiment.audio_lib'.  If it is
unspecified, it defaults to 'soundcard'.
"""

from io import BytesIO
import numpy as np
import soundfile as sf

class AudioOut():

    def __init__(self, experiment):
        self.exp = experiment
        self.blocksize = experiment.audio_blocksize
        # Import modules for this experiment (self) 
        if experiment.audio_lib == 'soundcard':
            import soundcard as snd
            if experiment.device != -1 and experiment.device != 'default':
                # Use a specific device
                self.out = snd.get_speaker(experiment.device)
            else:
                # Otherwise, use default device
                self.out = snd.default_speaker()
        elif experiment.audio_lib == 'sounddevice':
            import sounddevice as snd
            self.out = snd
            if experiment.device != -1 and experiment.device != 'default':
                # Use a specific device
                self.out.default.device = experiment.device
        else:
            raise ValueError(f'Unknown or unsupported audio library: {experiment.audio_lib=}')


    def preprocess_signals(self, signals):
        """Playback is always in stereo : Transform all signal parts into
        two channel stereo, in case they are still in single channel mono 
                
        Parameters
        ----------
        signals : list of numpy array
            list contains pre-signal, post-signal, isi-signal, reference
            signal(s), test signal.  They appear in the order in which they are
            to be presented in the audio output.
                
        Returns
        -------
        signals : list of numpy array
            The same list of signals, transformed into stereo format. 
        """
        
        return [np.tile(sig, (2, 1)).T if (type(sig) == list or (len(sig.shape) == 1)) 
                    else sig for sig in signals]


    def present_signal(self, signals, handler = None):
        """Play back all signal parts of the output stimulus acoustically.  
        """

        samplerate = self.exp.sample_rate    

        if handler is None:
            # Play back signals directly, without feedback (TUI)

            if self.exp.audio_lib == 'soundcard':
                with self.out.player(samplerate=samplerate, blocksize=self.blocksize) as o:
                    for part in signals:
                        o.play(np.asarray(part, dtype='float32', order='C'))

            elif self.exp.audio_lib == 'sounddevice':
                with self.out.OutputStream(samplerate=samplerate, channels=2, blocksize=self.blocksize) as o:
                    for part in signals:
                        o.write(np.asarray(part, dtype='float32', order='C'))

        else:
            # Play back signals from sever (GUI), prepare and send signals to client 
            # as byte-stream in wav-format. Feedback is controlled by client. 
            if (handler.audio_flag == 1 or handler.audio_flag == 2):
                num_channels = 2 # we force stereo with preprocess anyway 
                sigs = np.concatenate(signals)
                times = [part.size/num_channels/samplerate for part in signals]
                temp_file = BytesIO()
                with sf.SoundFile(temp_file, mode='w', format='WAV',
                                    samplerate=samplerate,
                                    channels=num_channels) as f:
                    f.write(sigs)
                handler.send_message('play',times,temp_file.getvalue())

            # play signals on host and control feedback (blink) in gui
            if (handler.audio_flag == 2 or handler.audio_flag == 3):
                blink = False
                i = 1 # button / interval

                # playback signals
                if self.exp.audio_lib == 'soundcard':
                    with self.out.player(samplerate=samplerate, blocksize=self.blocksize) as o:
                        for part in signals:
                            if handler.audio_flag == 3:
                                if (self.exp.visual_indicator and hasattr(self.exp, 'num_afc')):
                                    if blink:
                                        handler.write_message({'type': str(i),
                                                            'content':'red'})
                                        i += 1
                                    else:
                                        for k in range(len(self.exp.user_answers)+1):
                                            handler.write_message({'type': str(k),
                                                                'content':'white'})
                                    blink = not blink
                            o.play(np.asarray(part, dtype='float32', order='C'))
                        i = 1

                if self.exp.audio_lib == 'sounddevice':
                    with self.out.OutputStream(samplerate=samplerate, channels=2, blocksize=self.blocksize) as o:
                        for part in signals:
                            if handler.audio_flag == 3:
                                if (self.exp.visual_indicator and hasattr(self.exp, 'num_afc')):
                                    if blink:
                                        handler.write_message({'type': str(i),
                                                            'content':'red'})
                                        i += 1
                                    else:
                                        for k in range(len(self.exp.user_answers)+1):
                                            handler.write_message({'type': str(k),
                                                                'content':'white'})
                                    blink = not blink
                            o.write(np.asarray(part, dtype='float32', order='C'))
                        i = 1

                # unlock reply buttons in UI (locked if answer is received in script.js)
                handler.write_message({'type': 'unlock_buttons'})
