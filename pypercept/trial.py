"""This file is part of pypercept:
a package for developing psychoacoustical listening experiments.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
"""

class Trial():
    """This class sets all parameters, generates the signals defined by the user in
       the actual experiment's script and plays it back.
       
    Attributes
    ----------
    reference_signal : numpy array
    pre_signal : numpy array
        pre_signal
    isi_signal : numpy array
        inter stimulus signal
    post_signal : numpy array
        post signal
    background_signal : numpy_array
        constant background during trial
    test_signal : list
    correct_answer : str or int
    sample_rate : int
    calib : float
    input : str
    result : Result
        the result of a trial after the user's response has been evaluated
    user_response : str
        the user's response (i.e. input or selection)
    variable : dict
        needs to contain name (string), value, unit (string)
    parameters : dict
        containing name, value, unit and description.
    signal : list
    _save_names : dict    
    """


    def __init__(self, run):
        """initialization.

        Parameters
        ---------- 
        run: instance of the Run class, 
            extracts parameters for the current trial:
            variable : dict
                needs to contain name (string), value, unit (string)
            parameters : dict
                containing name, value, unit and description.
            reference_signal : numpy array
            pre_signal : numpy array
            isi_signal : numpy array
            post signal : numpy array
            background_signal : numpy_array
            sample_rate : int
            calib : float
            correct_answer : str or int
        """

        self.run = run 
        self.reference_signal = run.reference_signal
        self.pre_signal = run.pre_signal
        self.isi_signal = run.isi_signal
        self.post_signal = run.post_signal
        self.background_signal = run.background_signal
        self.test_signal = []
        self.correct_answer = None
        self.sample_rate = run.sample_rate
        self.calib = run.calib
        self.result = False
        self.task = run.task
        self.user_response = None
        self.variable = run.variable
        self.__dict__.update(run._parameters)
