"""This file is part of pypercept:
a package for developing psychoacoustical listening experiments.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
"""

import random

class Order():
    pass

class Sequential(Order):

    order_type = 'sequential'

    def next_run(self):
        for run in self.runs:
            if not (run.finished or run.skipped):
                return run
        raise StopIteration


                
class Interleaved(Order):

    order_type = 'interleaved'

    def next_run(self):
        open_runs = list(filter(lambda run: (not (run.finished or run.skipped)),
                                self.runs))
        if open_runs:
            return random.choice(open_runs)
        raise StopIteration
