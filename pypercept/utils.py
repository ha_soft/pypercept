"""This file is part of pypercept:
a package for developing psychoacoustical listening experiments.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
"""

"""This module provides useful functions often used for signal generation
in psychoacoustical experiments. Implementation inspired by "Psylab",
a similar framework written in Matlab by Martin Hansen.
"""

import numpy as np
from scipy.signal import lfilter, butter

def hanwin(sig, ramp_len=None):
    """Window/gate the input signal with Hanning window, with specified ramp length
    
    Parameters
    ----------
    x : array_like
        Input signal. 
    ramp_len : int, optional
        Length of onset and offset ramp, in number of samples.  The ramp length
        is defined as the number of samples that are smaller than the maximal
        value 1, including the first resp. last 0.  When left unspecified, the
        windows spans over the whole signal, i.e. the duration of both onset
        and offset ramp are half of the signal duration.

    Returns
    ----------
    out : array_like
        The windowed signal.

    Author:  MH 2003 (psylab).  Updated for pypercept Mar 2023.
    """

    if ramp_len is None:
        ramp_len = int(np.floor(len(sig)/2))    
    if 2*ramp_len > len(sig):
        print("warning: 2*ramp_len > signal length. ramp_len changed to window whole signal")
        h_window = _hanning_window(len(sig))
    else:
        han_flanks = _hanning_window(2*ramp_len)
        ones = np.ones(int(len(sig) - 2*ramp_len))
        h_window = list(han_flanks[0:ramp_len]) + list(ones) + \
                   list(han_flanks[ramp_len:])
    return h_window * sig



def _hanning_window(n):
    """Return n point symmetric hanning window including starting and ending zero"""
    t = np.arange(0,n)
    return np.array(0.5 * (1 + np.cos(-np.pi+2*np.pi/(n-1)*t)))



def fft_rect_filt(signal, f1, f2, fsamp, notch_flag=None, high_idx_offset=None):
    """Rectangular shape bandpass/notch filter via fft

    Parameters
    ----------
    x : array_like
        Input signal
    f1 : float
        Lower edge frequency (min.: 0,  max.: f2)
    f2 : float
        Upper edge frequency (min.: f1, max.: fsamp/2)
    fsamp : float
        Samplingrate of x.
    notch_flag : boolean, optional
        Flag to indicate a notch-filter instead of band-pass. Default: None   
    high_idx_offset : int, optional
        Offset for upper edge frequency index.  Set to -1 for use of fft_rect_filt
        for a filterbank purpose, so that neighbour bands have zero overlap samples.
        Default: 0.
    
    Returns
    ----------
    out : array_like
        Filtered output signal.

    Filter signal x having samplingrate fsamp via FFT, so that
    frequencies between f1 and f2 (both inclusive) are passed through,
    unless notch_flag is True, which means to pass frequencies outside
    of f1 and f2.  Returns the filtered signal.  The filter has
    rectangular shape in frequency domain, i.e., it has very steep
    slopes of approx. 300 dB damping within the bandwidth of 1
    FFT-bin.
    
    Setting f1=0 results in a lowpass at f2.  Setting f2=fs/2 results
    in a highpass at f1.  It is possible that f1==f2, resulting in a
    filter passing through (or notching) only 1 single FFT-bin.

    Because of the FFT, this function works fastest with signal lengths
    that are powers of 2.

    For filterbank purposes, high_idx_offset should be set to -1 to
    assure that neighbouring bands have exactly zero FFT bins
    overlap at the common edge frequencies, see the following example: 
    a = fft_rect_filt(x,    0,  300, fs, 0, -1)
    b = fft_rect_filt(x,  300, 1000, fs, 0, -1)
    c = fft_rect_filt(x, 1000, 2000, fs, 0, -1)
    y = a + 2*b +c  # 6 dB increase for frequencies between 300 and 1000 Hz (rectangularly).

    Author:  MH 2003-2018 (psylab).  Updated for pypercept Mar 2023.
    """
        
    if f1 > f2 or max(f1,f2) > (fsamp/2) or min(f1,f2) < 0:
        print(" 0 <= f1 <= f2 <= (fsamp/2) is required ")
        return
        
    if notch_flag is None:
        notch_flag = False
        
    if high_idx_offset is None:
        high_idx_offset = 0
        
    spectrum = np.fft.fft(signal)
    N = len(signal)
    
    # general calculation of Python spectral index:   idx = N*f/fs
    #
    #      .___________________.___________________.  ----> frequency
    #      |   |     |         |         |     |   |
    #freq  0   f1    f2       fs/2                 fs
    #idx   0   idx1  idx2     N/2      idx3  idx4  (N)

    idx1 = int(np.floor(N * f1/fsamp))
    idx2 = int(np.floor(N * f2/fsamp)) + high_idx_offset
    idx3 = N - idx2 
    idx4 = N - idx1 
    
    # Take a copy of the spectrum signal
    if notch_flag == True:
        # in the notch filter case we need a deep copy, see below
        ff = np.copy(spectrum)
    else:
        # for a band pass, a shallow copy is sufficient
        ff = spectrum
    
    # Now, set to zero parts of the spectrum, so that a band pass
    # between f1 and f2 is reached:
    # The spectrum at idx1:idx2 (both inclusive) is kept UNTOUCHED, 
    # and LIKEWISE at indices idx3:idx4 at negative frequencies.  
    # Everything outside these index-ranges is set to ZERO.
    # 
    # The following three lines also work for a lowpass situation where
    #   f1=0 ==> idx1=0 ==> idx4 = N, 
    #   so both index ranges [:idx1], and [idx4+1:] are then EMPTY(!).
    # They also work for a highpass situation where
    #   f2=fs/2 ==> idx2 = N/2 ==> idx3 = N/2
    #   because the index range [idx2+1:idx3] is then EMPTY(!)

    ff[:idx1] = 0        # empty index range in case of LOW  pass
    ff[idx2+1:idx3] = 0  # empty index range in case of HIGH pass
    ff[idx4+1:] = 0      # empty index range in case of LOW  pass
    
    if notch_flag == True:
        # Make a notch filter instead of a band pass filter.
        # Here we need "ff" to be a deep copy of the "spectrum", where
        # "spectrum" stayed unmodified while "ff" was modified, see above.
        ff = spectrum - ff
        
    # Mathematically, the result of the inverse FFT should be purely
    # real valued, but there will always be a small calculation
    # error/noise due to double precision, which leads to an imaginary
    # part != 0.  It is, however, approx. a factor 1e-12 smaller
    # relative to the real part.  So, force it to be real:

    return np.fft.ifft(ff).real


def gensin(freq, ampl, length_sec, phase_deg=0, fsamp=48000):
    """Generate sinusoidal tone with given parameters

    Parameters
    ----------
    freq : float
        Frequency of the sinusoid, in Hz.
    ampl : float
        Amplitude of the sinusoid.
    length_sec : float
        Duration of the sinusoid, in s.
    phase_deg : float, optional
        Phase in degree(!), not in radian.  Default: 0.
    fsamp : float, optional
        Samplingrate, in Hz. Default: 48000 Hz.

    Returns
    ----------
    out : array_like
        The generated sinusoid.
    """
    t = np.arange(0, length_sec, (1/fsamp))
    return ampl * np.sin(2*np.pi * (freq*t + phase_deg/360))



def rms(signal, axis=None):
    """Calculate the root mean square

    Parameters
    ----------
    signal : array_like
        Input signal.
    axis : None or int or tuple of ints, optional
        Axis or axes along which the means of the RMS are computed.
        The default is to compute the mean of the flattened array.
        axis is used to pass it to np.mean()

    Returns
    ----------
    out : float
        The RMS of the signal
    """
    return np.sqrt(np.mean(np.square(signal), axis=axis))



def gammatone_filter(x, fs, centerfreq, bandwidth, order=4, states=0):
    """Gammtone filtering with given center frequency, bandwidth, and order.

    Parameters
    ----------
    x : array_like
        Input signal.
    fs : float
        Sampling frequency of the input signal, in Hz.
    centerfreq : float
        Center frequency of the filter, in Hz.
    bandwidth : float
        Bandwidth at -3 dB points of the filter, in Hz.
    order : int, optional
        Filter order of the filter.  Default: 4.
    states = array_like or tuple, optional
        The array or tuple of filter states (previous output samples)
        of each filter stage/order.  Default: all-zeroes.
        This input argument is needed for continuous block-wise
        filtering.  In that case, the output filter states from one
        call need to passed as input filter states to the next call.

    Returns
    ----------
    y : array_like
        The complex valued (analytical) output signal, having the same shape as
        the input signal.
    states : array_like or tuple
        The updated filter states (previous output samples) of each
        filter stage, with same shape as the input states.

    Reference
    ----------
    V. Hohmann "Frequency analysis and synthesis using a gammatone filterbank".
    Acta Acustica united with Acoustica, 88(3):433-442, 2002.

    Author:  MH, Oct 2023.

    """

    if centerfreq >= fs/2:
        print('center frequency must not exceed fs/2!')
        return

    # Initialize filter states to a complex valued array of zeros with length=order
    if np.all(states == 0):
        states = np.zeros(order) + 0j

    # -------------------- Calculate filter coefficient --------------------

    # Bandwidth normalization factor.
    #
    # "bw_norm" is the factor $a_\gamma$ in Eq. (14)
    bw_norm = np.pi * np.math.factorial(2 * (order-1)) * 2**(-2.0*(order-1)) / (np.math.factorial(order-1) * np.math.factorial(order-1))

    # 'lambda' coefficient.  Eq. (14)
    lambda_coeff = 2 * np.pi * bandwidth / bw_norm

    # The recursive filter coefficient, being complex valued.  The
    # same coefficient is used in all filter stages, their number
    # being the filter order.  In other words, the "a-coefficients"
    # array would be:  a = [1, fc, fc, fc, fc] for a 4th order filter.
    #
    # "fc" is the coefficient  $\tilde{a}$ in Eq. (1)
    fc = np.exp(-lambda_coeff/fs) * np.exp(1j * 2 * np.pi * centerfreq / fs)

    # The scaling factor for the input samples.  This factor results in
    # a filter with gain = 1 for stimulus frequency = center frequency.
    # This corresponds to the iterative filter "b-coefficients" array
    # being: b = [scaling]
    #
    # "scaling" is the factor in the last equation (unnumbered) on page 435
    scaling = 2 * (1.0-np.exp(-lambda_coeff/fs))**order + 0j


    # -------------------- Do the filtering: --------------------

    # Initialize output array y with complex valued zeros
    y = x*0 + 0j
    # Loop through all input samples
    for k in range(len(x)):

        # Take next input sample
        store = x[k]

        # Loop through all filter order stages
        for l in range(order):
            # Next two lines are Eq. (7) with in-place update
            store += fc * states[l]
            states[l] = store

        y[k] = store * scaling

    return (y, states)



def gammatone_crit_filter(x, fs, centerfreq, broaden=1, order=4, states=0):
    """Gammtone filtering with a bandwidth equal to the critical auditory
    bandwidth.  The auditory bandwidth is calculated according to
    Glasberg & Moore (1990).

    Parameters
    ----------
    x : array_like
        Input signal.
    fs : float
        Sampling frequency of the input signal, in Hz.
    centerfreq : float
        Center frequency of the filter, in Hz.
    broaden : float, optional
        Broadening factor applied to the auditory bandwidth of normal hearing subjects.
        Default: 1.
    order : int, optional
        Filter order of the filter.  Default: 4.
    states = array_like or tuple, optional
        The array or tuple of filter states (previous output samples)
        of each filter stage/order.  Default: all-zeroes.
        This input argument is needed for continuous block-wise
        filtering.  In that case, the output filter states from one
        call need to passed as inputs to the next call.

    Returns
    ----------
    y : array_like
        The complex valued (analytical) output signal, having the same shape as
        the input signal.
    states : array_like or tuple
        The updated filter states (previous output samples) of each
        filter stage, with same shape as the input states.

    References
    ----------
    B. Glasberg, B. Moore "Derivation of auditory filter shapes from
    notched noise data". Hearing Research, 47, pp. 101-138, 1990.

    V. Hohmann "Frequency analysis and synthesis using a gammatone filterbank".
    Acta Acustica united with Acoustica, 88(3):433-442, 2002.

    Author:  MH, Oct 2023.

    """

    # The following two variants for  erb_aud  result in a deviation
    # of the impulse responses (at 2000 Hz with fs = 48000) with a
    # relative RMS difference of approx. 2e-5
    #
    # Critical auditory bandwidth specified as ERB (Hohmann 2002, eq. (13))
    # erb_aud = 24.7 + centerfreq / 9.265
    # Critical auditory bandwidth specified as ERB (Glasberg & Moore 1990, eq. (3))
    erb_aud = 24.7 + centerfreq * 24.7 * 4.37 / 1000.0

    # Apply broadening factor:  broader filter than in normal hearing
    bandwidth = erb_aud * broaden

    return gammatone_filter(x, fs, centerfreq, bandwidth, order, states)



def gammatone_crit_filterbank(x, fs, min_centerfreq, max_centerfreq, filter_density=1, broaden=1, order=4, states=0):
    """Filterbank of gammatone filters with critical auditory bandwidths.
    The filters are spaced equidistantly on the critical band rate frequency
    scale according to Glasberg & Moore (1990).

    Parameters
    ----------
    x : array_like
        Input signal.
    fs : float
        Sampling frequency of the input signal, in Hz.
    min_centerfreq : float
        Lowest filter center frequency of the filter bank, in Hz.
    max_centerfreq : float
        Highest filter center frequency of the filter bank, in Hz.
        This frequency is the upper limit which might not be reached,
        depending on the values of min_centerfreq and filter_density
    filter_density : float, optional
        Density of the filter center frequencies in the filter bank,
        specified as the number of filter center frequencies per ERB
        critical bandwidth.  A density of 0.5 means 1 filter per 2
        ERB critical bandwidths.  Default: 1.
    broaden : float, optional
        Broadening factor applied to the auditory bandwidth of normal
        hearing subjects.  Default: 1.
    order : int, optional
        Filter order of the filters.  Default: 4.
    states = 2-dim. array_like , optional
        The array or tuple of filter states (previous output samples)
        of each filter stage resp order, with filter stage resp. order
        running along axis 0.  Filter center frequencies run along
        axis 1.  Default: all-zeroes.
        This input argument is needed for continuous block-wise
        filtering.  In that case, the output filter states from one
        call need to passed as inputs to the next call.

    Returns
    ----------
    y : 2-dim. array_like
        The complex valued (analytical) output signals, with time running along
        axis 0, with same length as the input signal.  Filter center
        frequencies run along axis 1.
    states : 2-dim. array_like
        The updated filter states (previous output samples) of each
        filter stage, with same shape as for the input.
    centerfreqs : array_like
        The filter center frequencies, in Hz. 

    References
    ----------
    B. Glasberg, B. Moore "Derivation of auditory filter shapes from
    notched noise data". Hearing Research, 47, pp. 101-138, 1990.

    V. Hohmann "Frequency analysis and synthesis using a gammatone filterbank".
    Acta Acustica united with Acoustica, 88(3):433-442, 2002.

    Author:  MH, Oct 2023.

    """

    centerfreqs = erbn_equidist_centerfreqs(min_centerfreq, max_centerfreq, filter_density)

    num_centerfreqs = len(centerfreqs)

    # Initialize the output signal matrix with complex valued zeroes
    y = np.zeros( (len(x), num_centerfreqs) ) + 0j
    # Initialize filter states to a complex valued array of zeros with correct shape
    if np.all(states == 0):
        states = np.zeros((order, num_centerfreqs)) + 0j

    for c in range(num_centerfreqs):
        y[:,c], states[:,c] = gammatone_crit_filter(x, fs, centerfreqs[c], broaden, order, states[:,c])

    return y, states, centerfreqs



def erbn_equidist_centerfreqs(min_centerfreq, max_centerfreq, filter_density=1):
    """Calculate center frequencies equidistantly spaced on the auditory band rate frequency scale ERB_N.
    
    Parameters
    ----------
    min_centerfreq : float
        Lowest filter center frequency of the filter bank, in Hz.
    max_centerfreq : float
        Highest filter center frequency of the filter bank, in Hz.
        This frequency is the upper limit which might not be reached,
        depending on the values of min_centerfreq and filter_density
    filter_density : float, optional
        Density of the filter center frequencies in the filter bank,
        specified as the number of filter center frequencies per ERB
        critical bandwidth.  A density of 0.5 means 1 filter per 2
        ERB critical bandwidths.  Default: 1.

    Reference
    ----------
    B. Glasberg, B. Moore "Derivation of auditory filter shapes from
    notched noise data". Hearing Research, 47, pp. 101-138, 1990.

    Author:  MH, Oct 2023.
    """

    # Use Eq. (4) from Glasberg & Moore to calculate the ERB_N
    # ("Number of ERBs") frequency scale
    c1 = 21.4
    c2 = 4.37 / 1000
    min_erb_num = c1 * np.log10(c2 * min_centerfreq + 1)
    max_erb_num = c1 * np.log10(c2 * max_centerfreq + 1)

    # Vector of center frequencies on ERB_N frequency scale
    centerfreqs_erb_num = np.arange(min_erb_num, max_erb_num, 1/filter_density)

    # Vector of center frequencies in Hz
    centerfreqs = (10**(centerfreqs_erb_num / c1) - 1) / c2

    return centerfreqs



