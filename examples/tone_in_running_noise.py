#!/usr/bin/env python3

"""Measurement of detection threshold level of a sinusoid in noise as
   a function of sinusoid frequency and noise level

 This file uses pypercept, a collection of functions for designing and
 controlling interactive psychoacoustical listening experiments.

 This file is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 2 of the License, 
 or (at your option) any later version.
 See the GNU General Public License for more details:
 http://www.gnu.org/licenses/gpl

"""

from pypercept.experiments import AdaptAFCExperiment
from pypercept.order import Sequential
from pypercept.utils import gensin, rms, hanwin
import pypercept
import numpy as np

class ToneInRunningNoise(AdaptAFCExperiment, Sequential):

    def init_experiment(self, exp):

        exp.description = """
        This experiment measures the detectability of a sinuoid test
        signal in a broadband noise masker as a function of sinuoid
        frequency and noise level. The noise is always a running
        noise, i.e. an independent noise realisation in all intervals
        and in all trials.
        """

        exp.add_parameter('noise_level', [-40, -60], 'dB FS',
                          'level of broad band noise masker')
        exp.add_parameter('sine_freq', [500, 1500, 3000], 'Hz',
                          'frequency of sinusoid test signal')
        exp.set_variable('sine_level', -30, 'dB FS',
                         'level of sinusoid test signal')
        exp.add_adapt_setting('1up2down',
                              max_reversals = 8, start_step = 8, min_step = 1)

        exp.num_afc = 3        # The number of AFC alternatives

        exp.sample_rate = 48000

        exp.isi_signal  = 0.3  # Inter-Stimulus-Interval, duration of silence in s
        exp.pre_signal  = 0.3  # Pre-Signal, duration of silence in s
        exp.post_signal = 0.2  # Post-Signal, duration of silence in s

        exp.debug = True
        exp.debug_plot_signal = True


    def init_run(self, run):

        # Overwrite the start value of the variable (sine_level) to some
        # supra threshold value, but depending on the level of the noise.
        run.variable = run.noise_level + 0

        # The durations of sine and noise and window ramp, in seconds:
        sine_dur  = 0.25
        noise_dur = 0.35
        ramp_dur  = 0.05

        # The same durations in number of samples:
        run.sine_nsamp  = round(sine_dur * run.sample_rate)
        run.noise_nsamp = round(noise_dur * run.sample_rate)
        run.ramp_nsamp  = round(ramp_dur * run.sample_rate)

        # Pre-generate the sinusoid with RMS = 1
        run.sine1 = gensin(run.sine_freq, np.sqrt(2), sine_dur, run.sample_rate)
        # and apply Hanning window ramps
        run.sine1 = hanwin(run.sine1, run.ramp_nsamp)

        # Define the indices of start and end of the sine within the noise
        run.idx1 = round((run.noise_nsamp - run.sine_nsamp) / 2)
        run.idx2 = run.idx1 + run.sine_nsamp


    def init_trial(self, trial, run):

        # Generate num_afc new, independent realisations of Gaussian noise
        noise = np.random.randn(run.noise_nsamp, self.num_afc)

        # Adjust the RMS level, in all num_afc instances of the noise
        noise = noise/rms(noise,axis=0)*10**(run.noise_level/20)

        # Apply Hanning window to all noises
        for k in range(self.num_afc):
            noise[:,k] = hanwin(noise[:,k], run.ramp_nsamp)

        # Now use the first num_afc-1 columns of noise as reference signals
        # and the last column of noise for the test signal

        ## ----- This works for num_afc = 3, hard-coded:
        #trial.reference_signal = []
        #trial.reference_signal.append(noise[:,0])
        #trial.reference_signal.append(noise[:,1])
        #trial.test_signal = noise[:,2]

        ## ----- This works for any num_afc:
        trial.reference_signal = []
        for k in range(self.num_afc-1):
            trial.reference_signal.append(noise[:,k])

        trial.test_signal = noise[:,self.num_afc-1]

        # Scale the pre-generated sinusoid signal in amplitude and add it to the noise
        trial.test_signal[run.idx1:run.idx2] += run.sine1 * 10**(trial.variable/20)

        
if __name__ == '__main__':
    pypercept.start(ToneInRunningNoise)
