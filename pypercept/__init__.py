"""This file is part of pypercept:
a package for developing psychoacoustical listening experiments.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
"""

import argparse
import socket
from . import ui

def start(class_name):
    parser = argparse.ArgumentParser(description='Description of your program')
    parser.add_argument('-u','--ui',
                        help = 'select UI (terminal/ model/ gui/ egui) default: terminal',
                        choices=['terminal', 'model', 'gui', 'egui'],
                        default = 'terminal',
                        dest = 'ui')
    parser.add_argument('-a','--audio',
                        help = """audio states\n
                        1: audio from browser\n
                        2: audio from browser and host (python)\n
                        3: audio from host (python)""",
                        choices=[1, 2, 3],
                        type = int,
                        dest = 'audio_flag')
    args = parser.parse_args()

    experiment = class_name()

    if str.lower(args.ui) == 'terminal':
        ui.Tui(experiment)
    if str.lower(args.ui) == 'model':
        ui.Model(experiment)
    if str.lower(args.ui) == 'gui':
        if args.audio_flag == None: 
            args.audio_flag = 3
        ui.Gui(experiment, audio_flag = args.audio_flag)
    if str.lower(args.ui) == 'egui':
        if args.audio_flag == None: 
            args.audio_flag = 1
        your_ip =  socket.gethostbyname(socket.gethostname()+'.')
        print('Type http://'+your_ip+':8889/active/ in your browser to connect to the experiment.\n (Server and client must be on the same network.)')
        ui.Gui(experiment, extern = True, audio_flag = args.audio_flag)


