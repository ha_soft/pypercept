# pypercept ChangeLog

## v1.2.1
* New edit field for parameter permutation in data_helper.py.  Updated
  documentation. 

## v1.2
* New GUI data_helper.py for inspection of result data files

## v1.1.1
* Allow to choose directory for server experiments
* Allow for non-numeric value of a parameter

## v1.1
* Improvements in GUI and server

## v1.0.4
* Renaming of experiment classes for consistency:  
	"AFCExperiment"  >  "AdaptAFCExperiment"  
	"ConstantStimulusExperiment"  >  "ConstAFCExperiment"  
	"MatchingExperiment"  >  "AdaptMatchingExperiment"  

## v1.0.3
* Allow to choose a different audio library for sound output:  
	soundcard (default)  
	sounddevice  

## v1.0.2
* Implement headphone equalisation filtering

## v1.0.1
* Updated documentation

## v1.0
* Public release for presentation at DAGA 2024 conference

