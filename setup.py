from setuptools import setup, find_packages

with open('VERSION') as f: release = f.read().strip()

setup(
    name = "pypercept",
    version = release,
    packages=find_packages(exclude=['docs']),
    package_data={'pypercept.gui': ['*.html', '*.js']},
    include_package_data=True,

    # Project uses reStructuredText, so ensure that the docutils get
    # installed or upgraded on the target machine
    install_requires = ['cffi',
                        'matplotlib',
                        'numpy',
                        'pandas', 
                        'pysoundfile>=0.8',  
                        'regex',
                        'scipy',
                        'soundcard',
                        'sounddevice', 
                        'tornado']


    # metadata for upload to PyPI
    # author = "",
    # author_email = "",
    # description = "Next generation psylab",
    # license = "GNU GPL",
    # keywords = "audio psycoacustics",
    # url = "",   # project home page, if any

    # could also include long_description, download_url, classifiers, etc.
)
