"""This file is part of pypercept:
a package for developing psychoacoustical listening experiments.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
"""

"""This module contains a small GUI for exploring the json data files
that pypercept writes at the end of any experimental run peformed by a
test subject.
"""

import json
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import pathlib
from pypercept.analyse_data import AnalyseData
import sys
from tkinter import *
#from tkinter import ttk

all_files = None
data = None



def get_datafiles():
    global all_files
    all_files = list(pathlib.Path(os.getcwd()).glob('*.json'))
    all_files.sort()
    lb_subjects.delete(0, END)
    for path in all_files:
        lb_subjects.insert(END, path.name)

def get_experiments(filename):
    if os.path.exists(filename):
        try: 
            with open(filename) as f:
                global data
                data = pd.json_normalize(json.load(f))
            return data['Experiment'].unique()
        except:
            print(f'Apparently {filename} exists but cannot be read.')
            sys.exit()
    else: 
        print(f'File {filename} does not exist.')
        sys.exit()

def get_runs(exp):
    df = data.drop(data.loc[data['Experiment'] != exp].index)
    return df['Timestamp (started)'].unique()

def plot_trials():
    # We already have data, assume that there are no duplicate timestamps
    run_data = [lb_run.get(idx) for idx in lb_run.curselection()]
    fig_num = 100
    for d in run_data:
        plt.ion()
        fig = plt.figure(fig_num)
        ax = fig.add_subplot(111)
        fig.clear()

        # Get data frame
        tmp = data.loc[data['Timestamp (started)'] == d].reset_index(drop = True)

        # Drop columns with NaN entries.  This can happen, when data from
        # adaptive experiments and const. stim. experiment are mixed within
        # the same json data file
        tmp.dropna(axis=1, how='all', inplace=True)

        variables =  tmp['Trials.Variable'][0]
        results = tmp['Trials.Result'][0]    

        idx_corr = np.where(np.array([result == 'correct' for result in results]) == True)
        idx_wrong = np.where(np.array([result == 'wrong' for result in results]) == True)
        idx_undecided = np.where(np.array([result == 'undecided' for result in results]) == True)
        trial_nr = np.arange(1,len(variables)+1)
        plt.plot(trial_nr[idx_corr[0]], np.array(variables)[idx_corr], 'g+', markersize=8)
        plt.plot(trial_nr[idx_wrong[0]], np.array(variables)[idx_wrong], 'r_', markersize=8)
        plt.plot(trial_nr[idx_undecided[0]], np.array(variables)[idx_undecided], 'ko', markersize=8)

        # Adaptive experiment? 
        if 'Adaptation.type' in tmp:
            
            # Older json files did not save the start index of the measurement phase,
            # so check for existence of this info:
            if 'Threshold.start_measurement_idx' in tmp:
                idx = int(tmp['Threshold.start_measurement_idx'][0])
                x1 = np.arange(1,idx+1)
                x2 = np.arange(idx,len(variables)+1)
                plt.plot(x1,variables[:idx],'--', color='0.6')
                plt.plot(x2,variables[idx-1:],'-', color='0.1')
            else:
                x_axes = np.arange(1,len(variables)+1)
                plt.plot(x_axes,variables,'--')

            if 'Threshold.Method' in tmp:
                tmp_text = 'Threshold Method: "%s"' % tmp['Threshold.Method'][0]
                plt.text(0.9, 0.90, tmp_text, ha='right', va='center', transform=ax.transAxes)

            median_text = 'Med: %g %s' % (tmp['Threshold.Median'][0], tmp['Variable.Unit'][0])
            mean_text = 'Mean: %g %s' % (tmp['Threshold.Mean'][0], tmp['Variable.Unit'][0])
            std_text = 'Std.: %g %s' % (tmp['Threshold.Std. Deviation'][0], tmp['Variable.Unit'][0])
            plt.text(0.7, 0.85, mean_text, ha='left', va='center', transform=ax.transAxes)
            plt.text(0.7, 0.80, median_text, ha='left', va='center', transform=ax.transAxes)
            plt.text(0.7, 0.75, std_text, ha='left', va='center', transform=ax.transAxes)

        else: 
            x_axes = np.arange(1,len(variables)+1)
            plt.plot(x_axes,variables,'--')

        title_string = '%s, ' % tmp['Experiment'][0]
        title_string += '%s' % tmp['Subject'][0]
        for i in range(tmp['NumParameters'][0]):
            title_string += ', ' + tmp[f'Parameter {i+1}.Name'][0] + ': ' + \
                            str(tmp[f'Parameter {i+1}.Value'][0]) + ' ' + \
                            tmp[f'Parameter {i+1}.Unit'][0]
        plt.title(title_string)
        plt.xlabel('trial number')
        variable_string = '%s [%s]' % (tmp['Variable.Name'][0], tmp['Variable.Unit'][0])
        plt.ylabel(variable_string)
        plt.xlim((0,len(variables)+1))
        plt.ylim((min(variables)-1,max(variables)+1))

        plt.draw()

        fig_num += 1

def get_perm():
    perm_str = perm_params.get()
    if perm_str == '':
        perm = 0
    else:
        perm = []
        for s in perm_str:
            perm.append(int(s))
    return perm

def plot_all_data():
    perm = get_perm()
    plot_data(selected=False, perm_params=perm)

def plot_selected_data():
    perm = get_perm()
    plot_data(selected=True, perm_params=perm)

def plot_data(selected=False, perm_params=0):
    adtmp = AnalyseData()
    # The selected filename
    stmp = lb_subjects.get(lb_subjects.curselection())
    # subject name = the part of the string between 'pypercept_' at the beginning and '.json' at the end
    subjectname = stmp[10:-5]
    expname = lb_exp.get(lb_exp.curselection())
    adtmp.load_data(subjectname, expname)
    if selected == True:
        print(f'Selecting subset of {len(lb_run.curselection())} run(s).')
        adtmp.data = adtmp.data.loc[list(lb_run.curselection())]
        adtmp.subject = adtmp.subject + '(subset)'
    adtmp.display_data(perm_params=perm_params, fig_num=10*(1+lb_subjects.curselection()[0])+(1+lb_exp.curselection()[0]))
    #adtmp.display_data(fig_num=10*(1+lb_subjects.curselection()[0])+(1+lb_exp.curselection()[0]))


# Set up the GUI
root = Tk()
root.geometry("700x400")
root.title('pypercept data helper')
root.rowconfigure(0, weight=1)
root.columnconfigure(0, weight=1)
frame = Frame(root)
frame.grid(row = 0, column = 0, padx=5, pady=2, sticky='news')

btn_list_subjects = Button(frame, text='find all subjects', 
                           command=get_datafiles)
btn_list_subjects.grid(row = 0, column = 0, padx=5, pady=2, sticky='ew')
btn_plot_all = Button(frame, text='plot all data',
                      command=plot_all_data)
btn_plot_all.grid(row = 2, column = 1, padx=5, pady=2, sticky='ew')
btn_plot_runs = Button(frame, text='plot selected',
                       command=plot_selected_data)
btn_plot_runs.grid(row = 2, column = 2, padx=5, pady=2, sticky='ew')
btn_plot_trials = Button(frame, text='plot trials', 
                         command=plot_trials)
btn_plot_trials.grid(row = 3, column = 2, padx=5, pady=2, sticky='ew')

label_perm = Label(frame, text="perm_params (indices only !):").grid(column=0, row=3)
perm_params = StringVar()
entry_perm_params = Entry(frame, textvariable = perm_params)
entry_perm_params.grid(row = 3, column = 1, padx=5, pady=2, sticky='ew')

lb_subjects = Listbox(frame)
lb_subjects.config()
lb_subjects.grid(row=1, column=0, padx=5, pady=2, sticky='news')
lb_exp = Listbox(frame, exportselection=False)
lb_exp.grid(row=1, column=1, padx=5, pady=2, sticky='news')
lb_exp.config()
lb_run = Listbox(frame, selectmode=MULTIPLE, exportselection=False)
lb_run.grid(row=1, column=2, padx=5, pady=2, sticky='news')

frame.columnconfigure(tuple(range(3)), weight=1) # allow to resize horizontally
frame.rowconfigure(1, weight=1) # only resize row 1 vertically , i.e. listboxes

def on_select_subject(event):
    w = event.widget
    idx = w.curselection()[0] 
    lb_exp.delete(0, END)
    for exp in get_experiments(all_files[idx].name):
        lb_exp.insert(END, exp)

def on_select_experiment(event):
    w = event.widget
    lb_run.delete(0, END)
    for run in get_runs(lb_exp.get(w.curselection()[0])):
        lb_run.insert(END, run)

subject_select = lb_subjects.bind('<<ListboxSelect>>', lambda event: on_select_subject(event))
exp_select = lb_exp.bind('<<ListboxSelect>>', lambda event: on_select_experiment(event))

get_datafiles()

root.mainloop()
