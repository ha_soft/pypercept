#!/usr/bin/env python3

"""Measurement of modulation detection threshold for SAM as a function of modulation frequency

 Copyright (C) 2023  Martin Hansen
 Author :  Martin Hansen <martin.hansen AT jade-hs.de>
 Date   :  24 Mar 2023

 This file uses pypercept, a collection of functions for designing and
 controlling interactive psychoacoustical listening experiments.

 This file is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 2 of the License, 
 or (at your option) any later version.
 See the GNU General Public License for more details:
 http://www.gnu.org/licenses/gpl

"""

from pypercept.experiments import AdaptAFCExperiment
from pypercept.order import Sequential
from pypercept.utils import gensin, hanwin, rms
import numpy as np
import pypercept

class SamSincarrierDetect(AdaptAFCExperiment, Sequential):

    def init_experiment(self, exp):
        exp.description = """
        This experiment reproduces the experiment by Kohlrausch,
        Fassel, Dau (2000), JASA 108(2), p. 723-734, who investigated
        the detection threshold of the modulation degree for
        sinusoidal amplitude modulation of a sinusoidal carrier.  The
        stimuli are generated as described in the setup in section I.B.
        of their paper.
        """

        exp.set_variable("m", -8, "dB", "modulation degree")
        exp.add_parameter("mod_freq",  [[ 4, 16, 64, 128], [ 64, 128, 256]], "Hz", "modulation frequency")
        exp.add_parameter("car_freq", [ [800, 1600], [3200]], "Hz", "carrier frequency")
        exp.add_adapt_setting("1up2down", max_reversals=8, start_step=8, min_step=1)

        #exp.randomize_runs = True
        exp.num_afc = 3
        exp.task = "In which interval did you hear the test tone (1,2,3)?"

        exp.debug = True
        exp.debug_plot_signal = False

        exp.sample_rate = 48000

        isi_len = 0.25   # Inter stimulus interval length
        # Generate the signal explicitly ...
        exp.isi_signal = np.zeros(round(isi_len * self.sample_rate))
        # or generate the signal implicitly by just specifying its duration
        exp.post_signal = 0.1
        exp.pre_signal = 0.1


    def init_run(self, run):
        """Generate some signals/templates and reference signal."""

        # Temporal structure of the modulator, i.e. the envelope, illustrated
        # here for shorter carrier onset ramps and modulation onset ramp
        #
        #  test signal:
        #
        #    1+m             -~~~~~~~~~~~~~~~~-
        #                   /     modulation   \
        #     1       -----.                    .-----
        #            / ones                      ones \
        #     0   __/                                  \__
        #                                                  --> time
        #
        #  reference signal:
        #     1       --------------------------------
        #            /               ones             \
        #     0   __/                                  \__
        #                                                  --> time
        #  
        #
        # In the unmodulated reference signal, m is 0,
        # and in the modulated test signal, m is cur_trial.variable

        tone_dur = 0.8  # duration of the carrier tone [s]
        a0       = 0.5  # its amplitude        
        mod_dur  = 0.5  # duration of the modulated part [s]
        ramp_dur = (tone_dur-mod_dur)*0.5  # duration of onset/offset ramp [s]
        ramp_nsamp = round(ramp_dur*run.sample_rate)  #  in samples

        # the unmodulated carrier
        run.carrier = gensin(run.car_freq, a0, tone_dur, run.sample_rate)

        # the "ones-offset" of the modulator with Hanning window ramps
        run.mod1 = np.ones_like(run.carrier)
        run.mod1 = hanwin(run.mod1, ramp_nsamp)

        # The sinusoidal modulator with amplitude 1, for later use in init_trial 
        run.sam1 = gensin(run.mod_freq, 1, mod_dur, 0, run.sample_rate)
        # Apply full length Hanning window
        run.sam1 = hanwin(run.sam1)
        # And add prepended/appended zeros of duration ramp_dur
        ramp_zeros = np.zeros(ramp_nsamp)
        run.sam1 = np.concatenate((ramp_zeros, run.sam1, ramp_zeros))

        # The reference signal has no modulation, but only onset and offset ramps
        run.reference_signal = run.carrier * run.mod1


    def init_trial(self, trial, run):
        """Generate test signal with variable modulation degree."""

        if trial.variable > 0:
            print('WARNING:\nModulation degree > 0 dB causes overmodulation. Limited to 0 dB.')
            trial.variable = 0 

        # Scale the pre-generated sinusoid of amplitude 1 and then add it to the
        # pre-generated ones, i.e. calculate the term  (1 + m*cos(omega_mod * t))
        modulator = run.mod1 + 10**(trial.variable/20)*run.sam1

        # Perform the amplitude modulation on the carrier
        trial.test_signal = run.carrier * modulator

        # The next line is useful and necessary IN THIS example where
        # modulation detection is investigated.  However, it will probably not
        # be required in your experiment.  It may even be contra-productive
        # or wrong in your own experiment!
        # Equalize test signal and reference signal in RMS level:

        trial.test_signal = trial.test_signal / rms(trial.test_signal) * rms(trial.reference_signal)


if __name__ == '__main__':
    pypercept.start(SamSincarrierDetect) 
