.. pypercept documentation master file, created by
   sphinx-quickstart on Tue Aug 22 10:07:47 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

pypercept
=========

.. include:: ../README.rst

.. include:: philosophy.rst

.. include:: own-experiment.rst

.. include:: save-analyse-data.rst   

.. include:: model-simulations.rst

.. include:: util-functions.rst   

.. include:: variables-list.rst

.. include:: license-gnugpl.rst
   

Indices and tables
==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`


.. |pyper| replace:: pypercept_
