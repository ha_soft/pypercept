.. variables-list.rst

Variables controlling pypercept
===============================

This section lists variables/attributes with a fixed name and meaning
and which control the behaviour of pypercept.

Most of the following variables are attributes of the
:class:Experiment.  

:subject_name:
   String containing the subject name.  This string should not contain
   any whitespaces.

:feedback:
   Boolean, whether to give feedback ('correct', 'not correct') to the subject after his/her response.

:info:
   Boolean, whether to inform the subject about the next trial's parameter values.

:debug:
   Boolean, whether to print additional information.  This should be
   set to `False` during real measurement with subjects

:debug_plot_signal:
   Boolean, whether to plot the output stimulus waveform after each presentation.
   This should be set to `False` during real measurement with subjects

:allow_debug:
   Boolean, whether to allow the subject to toggle the debug status. 

:randomize_runs:
   Boolean, whether to randomize all runs with the different
   parameter(s) combinations. 

:threshold_reversals:
   Boolean, whether to calculate the threshold only across the trials
   at the reversal points during the measurement phase of an adaptive experiment.
   Default:  `False`, meaning that threshold is calculated from *all*
   trials during the measurement phase. 

:user_answers:
   String with allowed characters for user answers as a response to the stimulus,
   e.g. '1', '2', '3'; or 'u' (up), 'd' (down).

:meta_answers:
   String with allowed characters for user answers to control the experiment,
   e.g. 'n' for next run, 'q' for quit, etc. 

:audio_lib:
   String with Python library used for sound output.  Can be
   'soundcard' (default) or 'sounddevice'.  

:audio_blocksize:
   The blocksize used by the audio library.  Default: 8192.

:eqfilter_b:
   Array of filter 'b'-coefficients for linear equalisation filter.
   Default: empty.

:eqfilter_a:
   Array of filter 'a'-coefficients for linear equalisation filter.
   Default: empty.
   Filtering will only take place, if both the 'a' and 'b'
   coefficients are specified.  



.. End of file
