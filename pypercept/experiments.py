"""This file is part of pypercept:
a package for developing psychoacoustical listening experiments.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
"""

"""This module contains experiment classes for pypercept experiments.
The class :class:`Experiment` defines methods and attributes which
are the same for all types of experiment.
Classes like :class:`AdaptAFCExperiment` and :class:`AdaptMatchingExperiment` inherit
from :class:`Experiment` and implement the interface like methods defined
in this class.
"""

import random
from itertools import chain, product
from pypercept.result import Result
from pypercept.audio_out import AudioOut
import pypercept.adapt
import pypercept.exception as excpt
from pypercept.run import Run
from pypercept.trial import Trial
from pypercept.save import json_export
import datetime
import time
import numpy as np
from scipy.signal import lfilter


class Experiment():
    """
    Base class of all pypercept experiments

    This class provides the base methods and data structures for all
    experiments built with pypercept but it is never directly instantiated
    because some methods are just "Interfaces"
    
    Attributes
    ----------
    subject_name : str
        name or abbreviation of test subject
    runs : list
        becomes list of :class:`Run`. This list is auto generated while
        experiment creation
    parameters : dict
        becomes dict of dict containing name, values, unit and description. For
        simple parameter handling there is the method :func:`add_parameter`.
        There must be at least one parameter in each experiment.
    variable : dict
        needs to contain name (string), value, unit (string). For simple variable
        handling there is the method :func:`set_variable`. Each experiment
        has exactly one variable.
    adapt_settings : list
        list of adapt settings used in the experiment. For simple adapt settings
        handling there is the method :func:`add_adapt_setting`. There must be
        at least one adapt setting in each experiment.
    pre_signal : numpy array, float or tuple (optional)
        Signal part played before each trial. There are some options for simple
        signal generation. For more information see documentation section:
        `Zero signals`
    reference_signal : numpy array, list of numpy arrays
        Signal part(s) not containing the test sequence. In case of N-AFC experiment
        this can also an list of N-1 signals, which are randomly spread of the
        N-1 reference intervals *not* containing the test sequence.
    isi_signal : numpy array, float or tuple (optional)
        Inter stimulus interval, i.e. signal played between each signal part. Most
        of the time this variable is used for silence between the signal parts.
        For that reason it can be initialized as float or tuple. For more information,
        see documentation section: `Zero signals`. If not specified, signal parts
        are simply joined together
    test_signal : numpy array
        Signal part containing the test sequence. 
    post_signal : numpy array, float or tuple (optional)
        if specified, this signal part is played at the end of trial.
    background_signal: numpy array
        is specified, this is used as constant background signal during a single trial. 
    allow_debug : boolean (optional)
        With this variable the experiment creator can specify whether the
        test subject is  allowed to enable plotting or not. default: True
    randomize_runs : boolean (optional)
        Randomize order of runs. Default: False 
    device : int or str (optional)
        Select the audio device. `output_devices.py` displays a list of available
        devices. Default: 'default'.
    sample_rate : int (optional)
        sampling frequency for all signals of the whole experiment. Default: 48000 
    calib : int or float (optional)
        This variable is a place holder for a calibration value. It can be used
        as a constant to fit the signal level to the test system settings.
        Without explicit usage in your signal generation this value has no impact
        on the signals. Default: 0.
    task : str
        Description of experiment and description of what the test subjects have
        to do. This message is displayed before the experiment starts.
    isSim : boolean
        Flag whether a model simulation is used.  Default: False
    """


    def __init__(self):
        """initialization of the experiment

        In this method, experiment initialization takes place. Start date is set,
        default values are set, user defined init_experiment method is called
        and the list of runs is generated from given parameters and adapt settings.
        """
        self.cls = type(self).__name__
        self._create_time = datetime.datetime.now().strftime('%Y-%m-%dT%H.%M.%S')
        self.subject_name = ''
        self.runs = []
        self.parameters = {}
        self.variable = {}
        self.adapt_settings = []
        self.signals = {}
        self.pre_signal = []
        self.reference_signal = []
        self.isi_signal = []
        self.test_signal = []
        self.post_signal = []
        self.background_signal = []
        self.allow_debug = True
        self.randomize_runs = False
        self.threshold_reversals = False
        self.sample_rate = 48000
        self.calib = 0
        self.feedback = True # give feedback about correctness of last answer
        self.info = True     # give information about parameters of next run
        self.debug = False   # give debug information / allow forced answers
        self.debug_plot_signal = False # show signal for each trial 
        self.visual_indicator = True   # mark the stimulus intervals visually
        self.device = 'default' # name/ID of audio device, -1 / 'default' uses system default.
        self.audio_lib = 'soundcard' # library for audio playback, soundcard and sounddevice are supported 
        self.audio_blocksize = 8192 
        self.isSim = False
        self.meta_answers = ''
        self.user_answers = ''
        self.eqfilter_b = None
        self.eqfilter_a = None
        self.startup_message = '''

                        This is pypercept.

                        Make sure to check your
                        sound output levels
                        before you begin !

        '''

        self.init_experiment(self)

        self.duration_to_zero_signal(self)
        self._show_startup_message()
        self._generate_runs()

        # initialize audio output
        self.audio = AudioOut(self)


        # Set valid meta keys, evaluated by the appropriate flags (e.g. debug)
        #   n: next run, i.e. stop the current run
        #   q: quit experiment
        #   g: toggle debug (allow_debug required)
        #   v: forced correct (debug required)
        #   x: forced wrong (debug required)
        self.meta_answers += 'nq'
        if self.allow_debug:
            self.meta_answers += 'g'
        if self.debug:
            self.meta_answers += 'vx'

    def init_experiment(self, experiment):
        """Method to init experiment wide settings.

        This method is called only once at the beginning of an experiment.  It
        needs to be implemented individually in the user's experiment, and all
        settings with experiment wide validity should be defined here.
        """
        raise NotImplementedError

    def init_run(self, run):
        """Method to init a run.

        This method is called only once at the beginning of a run.  It needs to
        be implemented individually in the user's experiment, and all settings
        which are valid in the current run should be defined here.
        """
        raise NotImplementedError

    def init_trial(self, trial, run):
        """Method to init a trial.

        This method is called once at the beginning of each new trial.  It
        needs to be implemented individually in the user's experiment, and all
        settings which are valid in the current trial should be defined here.
        """
        raise NotImplementedError

    def add_parameter(self, name, values, unit, description=""):
        """Adds a new parameter to the experiment.

        This method offers a simple way to add a new parameter to the experiment.
        Usually this method is used in :func:`init_experiment`. To add more
        than one parameter this method can be called multiple times.

        Parameters
        ----------
        name : str
            name of the parameter. It **must not** contain any white space!!
        values : list
            list of all values of an parameter. If the parameter only contains
            one value it must be a list with one element
        unit : str
            unit of the parameter
        description : str (optional)
            short description

        Examples
        --------
        this.add_parameter(name='noise_level',values=[-60, -40, -20], unit='dB')

         .. note::

            The name must not contain any white space because in runs
            and trials the run specific value of this parameter can be
            referenced by its name.
        """
        values = list(values)
        new_param = {"values": values, "unit": unit, "desc": description}
        self.parameters[name] = new_param

    def set_variable(self, name, start_val, unit, description=""):
        """Sets the 'variable' of the experiment.

        This method is the normal way to set the 'variable' of the experiment.
        Usually this method is called in :func:`init_experiment`. Because every
        experiment has only one variable it can be called only once.

        Parameters
        ----------
        name : str
            name of the variable.
        start_val : int
            start value of the variable
        unit : str
            unit of the parameter
        description : str (optional)
            short description

        Examples
        --------
        this.set_variable(name='frequency', start_val=1000, unit="Hz")
        """
        var = {"name": name, "unit": unit, "start_val": start_val, "desc": description}
        self.variable = var


    def _show_startup_message(self):
        """Show a startup message"""
        print(self.startup_message)

    def _generate_runs(self):
        """Creates all runs, one for each combination of parameter(s)"""

        combinations = self._combine_parameters()

        # Randomize order of runs
        if self.randomize_runs:
            random.shuffle(combinations)
           
        for combi in combinations:
            params = {} 
            for param in combi[1:]:
                params.update(param)

            adapt_settings = combi[0]
            AdaptClass = getattr(pypercept.adapt, "Adapt%s" % adapt_settings["type"])
            RunClass = type('Run'+adapt_settings["type"],(Run,AdaptClass,),{})
            run = RunClass(self, params, self.variable["start_val"],
                           adapt_settings, self.reference_signal,
                           self.pre_signal, self.isi_signal,
                           self.post_signal, self.background_signal,
                           self.sample_rate, self.calib, self.task)
            
            self.init_run(run)
            self.duration_to_zero_signal(run)
            self.runs.append(run)

    def _combine_parameters(self):
        """Return a list with all parameter combinations"""
        
        # Check if we have grouped parameters, i.e. _multiple_ corresponding lists
        if type(self.parameters[next(iter(self.parameters))]['values'][0]) == list:
            num_groups = len(self.parameters[next(iter(self.parameters))]['values'])
            combinations = []
            for i_group in range(num_groups):
                # Combine groups, i.e. n-th list of each parameter
                subgroup = []
                for name, parameter in self.parameters.items():
                    subgroup.append([{name: {'value': value, 'unit': parameter['unit']}} for value in parameter['values'][i_group]])
                # If required, randomization by group should happen by a
                # shuffle() of the combined subgroup list before appending.
                if len(self.adapt_settings) > 0:
                    # For an adaptive experiment, include the adapt_settings as the first entry
                    combinations.append(list(product(self.adapt_settings, *subgroup)))
                else:
                    # For a const.stim. experiment, without adapt_settings, use an empty first entry
                    combinations.append(list(product({''}, *subgroup)))
            # Flatten the list
            combinations = list(chain.from_iterable(combinations))
        else:
            # calculate all combinations with each parameter type given in _one_ list
            outer_param_list = []
            for name, parameter in self.parameters.items():
                inner_param_list = [{name:{'value':value, 'unit':parameter['unit']}} for value in parameter['values'] ]
                outer_param_list.append(inner_param_list)

            if len(self.adapt_settings) > 0:
                # For an adaptive experiment, include the adapt_settings as the first entry
                combinations = list(product(self.adapt_settings, *outer_param_list))
            else:
                # For a const.stim. experiment, without adapt_settings, use an empty first entry
                combinations = list(product({''}, *outer_param_list))

        return combinations    

    def _add_background_signal(self, signal, background_signal):
        # Add background_signal piecewise to match signal (i.e. list of np arrays)
        idx = 0
        for part in signal:
            part += (background_signal[idx:idx+len(part)])
            idx += len(part)

        return signal

    def _equalisation_filter(self, signal):
        # Apply (headphone) equalisation filtering with the specified filter coefficients
        # Initialize filter states necessary for block-wise filtering
        num_states = max(len(self.eqfilter_a), len(self.eqfilter_b)) - 1
        if (len(signal[1].shape)<2):
            zstates = np.zeros(num_states)
        else:
            num_channels = signal[1].shape[1]
            zstates = np.zeros((num_states, num_channels))
        for k in range(len(signal)):
            # Only filter the non-empty parts of signal:
            if len(signal[k])>0:
                signal[k], zstates = lfilter(self.eqfilter_b, self.eqfilter_a, signal[k], axis=0, zi=zstates)

        return signal

    def add_adapt_setting(self):
        """ Specific implementation is made by  method of the individual experiment class.
        """
        raise NotImplementedError

    def adapt(self):
        """ Specific implementation is made by  method of the individual experiment class.
        """
        raise NotImplementedError

    def build_output_stimulus(self):
        """ Specific implementation is made by  method of the individual experiment class.
        """
        raise NotImplementedError

    def eval_response(self):
        """ Specific implementation is made by  method of the individual experiment class.
        """
        raise NotImplementedError


    def generate_trial(self, run):
        """ Generates trial

        This method generates a trial using the parameter set with its specific
        values given by the current run.        
        
        Parameters
        ----------
        run : :class:`Run`
            Reference on the Run, the next Trial is needed for.
        
        Returns
        -------
        trial : :class:`Trial`
            new Trial object with all parameters and variable values are set
            correctly
        """
        trial = Trial(run)
        return trial

    def duration_to_zero_signal(self, obj):
        """Convert given duration to a zero signal of that length

        This function checks if for a given object a signal is specified
        as float or tuple and converts it into a zero signal of given length.

        Parameters
        ----------
        obj : :class:`Run``, :class:`Trial` or subtype of :class:`Experiment`
            The Experiment, Run or Trial to check for signals not yet generated
        """
        signals = ['reference_signal', 'test_signal', 'pre_signal', 'post_signal',
                   'isi_signal']
        for signal in signals:
            sig = getattr(obj,signal)
            if isinstance(sig,tuple):
                if len(sig) == 1:
                    setattr(obj, signal, np.zeros(int(np.round(sig[0]*obj.sample_rate))))
                else:
                    setattr(obj, signal, np.zeros((int(np.round(sig[0]*obj.sample_rate)), sig[1])))
            elif isinstance(sig, int) or isinstance(sig, float):
                setattr(obj, signal, np.zeros(int(np.round(sig*obj.sample_rate))))

    def next_trial(self, run):
        """Build next trial
               
        Parameters
        ----------
        run : :class:`Run`
       
        Returns
        -------
        trial : :class:`Trial`
        
        signals : list of numpy arrays
        
        """
        trial = self.generate_trial(run)
        trial.correct_answer = self.correct_answer()
        self.init_trial(trial, run)
        self.duration_to_zero_signal(trial)
        signals = self.build_output_stimulus(trial)
        return trial, signals

    def skip_run(self, run):
        """ sets skipped to True
        """
        run.skipped = True



class AFCExperiment(Experiment):
    """This class provides functions specific for an n-AFC
    (alternative forced choice) experiment.
    
    Attributes
    ----------
    num_afc : int
        number of intervals per trial
        :func:`build_output_stimulus` for further information
    forced : boolean
        distinguishes between forced and unforced choice. This info is used e.g. for html selection

    """    
    
    task = "In which interval did you hear the test tone? "
    
    num_afc = 3 # default
    forced = True

    def __init__(self):
        super().__init__()
        # Do this AFTER the user/experiment specified the value exp.num_afc, and possibly exp.user_answers:
        # If the user/experiment did not specify its own user_answers ...
        if self.user_answers == '':
            # ... then set possible user_answers to the default: (1 ... num_afc).
            for d in range(self.num_afc):
                self.user_answers += str(d+1)

    def correct_answer(self):
        """set correct answer for given trial

        depending on the current experiment type this method defines the right
        answer for each trial

        Returns
        -------
        correct_answer : :enum:`Answer`
            depending on experiment type return correct answer      
        """
        return random.randint(1, self.num_afc)

    def build_output_stimulus(self, trial):
        """Build output stimulus for a N-AFC experiment
        
        This method prepares a list of test stimulus and reference stimulus
        in an N-AFC manner.
        
        Parameters
        ----------
        trial : :class:`Trial`
        
        Returns
        -------
        signals : list of numpy array
            list contains pre-signal, post-signal, isi-signal, (N-1) times the
            reference stimuli, and the test stimulus.  They appear in the order
            in which they are to be presented in the audio output.
        """

        ref_multi = False # indicate if we have multiple reference signals

        # Check number of multiple reference signals bundled in a list.
        # If it's not a list (np.array) or empty (no reference), continue.  
        if not len(trial.reference_signal) == 0 and type(trial.reference_signal) is list: 
            if len(trial.reference_signal) == self.num_afc-1:
                random.shuffle(trial.reference_signal)
                ref_iterator = iter(trial.reference_signal)
                ref_multi = True
            else:
                raise ValueError("""Number of reference intervals does not match
                number of experiment reference intervals""")
        signals = []
        signals.append(trial.pre_signal.copy())
        for x in range(self.num_afc):
            if x == trial.correct_answer-1:
                # Place the TEST SIGNAL at this interval number
                signals.append(trial.test_signal.copy())
            else:
                # Place the (or one) REFERENCE SIGNAL at this interval number
                if ref_multi: # There are multiple reference signals
                    signals.append(ref_iterator.__next__().copy())
                else:
                    # There is only a single reference signal
                    signals.append(trial.reference_signal.copy())
            if x != self.num_afc - 1:
                # Append the ISI signal when this is not yet the last interval 
                signals.append(trial.isi_signal.copy())
        signals.append(trial.post_signal.copy())

        # Add background_signal if it is specified 
        if len(trial.background_signal) > 0:
            signals = self._add_background_signal(signals, trial.background_signal)

        # Apply (headphone) equalization filter if filter coefficients are specified
        if hasattr(self.eqfilter_b, '__len__') and hasattr(self.eqfilter_a, '__len__'):
            signals = self._equalisation_filter(signals)

        return signals

    def eval_response(self, run, trial, user_response):
        """Check and save the user answer to the current trial
        
        This method evaluates the subject's answer (being an interval number)
        by comparing it to the randomly chosen number of the test interval.
        The result is saved to trial.result and the trial is appended to the
        list of all trials of the current run.
        
        Parameters
        ----------
        user_response : string
            The user's reply/input
        
        Returns
        -------
        (none)

        Raises
        ------
        ValueError

        """

        trial.user_response = user_response

        # Catch forced answers first, which only works with debug enabled
        if self.debug and user_response == 'v': # correct
            trial.result = Result.CORRECT
        elif self.debug and user_response == 'x': # wrong
            trial.result = Result.WRONG
        else: # check correctness
            user_response = int(user_response)
            if user_response == trial.correct_answer:
                trial.result = Result.CORRECT
            else:
                trial.result = Result.WRONG

        run.trials.append(trial)

    
class AdaptExperiment(Experiment):
    """This class provides the methods specific to an experiment with
    adaptive control of the variable.
    """
        
    def add_adapt_setting(self, adapt_method="1up2down", max_reversals=8,
                          start_step=4, min_step=1, reversed_up_down=False, **kwargs):
        """Add adapt method/setting.

        This method adds an adaption method and its start values to the experiment. 
                
        Parameters
        ----------
        adapt_method : str
            The name of the adaptive method.  A number of methods is pre-implemented:
            '1up1down'
            '1up2down'
            '2up1down'
            '1up3down'
            'WUD' (weighted up down)
            'UWUD' (unforced weighted up down)
        max_reversals : int
            The number of reversals during the measurement phase.
            This serves as a stop criterion for the measurement phase.
        start_step : float
            The start step size.
            The current step size is added to / subtracted from the current variable.
        min_step : float
            The minimal step size.
            If reached, the familiarisation phase stops and the measurement phase start
        reversed_up_down : boolean
            When set to True, the stimulus variable goes up after correct answer
            resp. goes down after wrong answers.
            Default: False
        kwargs : dict (optional)
            A dict of keywords and arguments may be used for special adapt method
            WUD needs key: pc_convergence  value: float [0..1[
        """
        new_adapt = {"type": adapt_method, "max_reversals": max_reversals,
                     "start_step": start_step, "min_step": min_step, 
                     "reversed_up_down": reversed_up_down}
        new_adapt.update(kwargs)
        self.adapt_settings.append(new_adapt)

    def adapt(self, run):
        """ Apply selected adaptive rule to the variable of the current run. """
        try:
            step = run.adapt(run.trials)
        except excpt.RunFinishedException:
            run.finished = time.strftime('%d-%b-%Y__%H:%M:%S')
            raise
        # Get the variable value used in the last trial.  This allows also to
        # modify the variable, e.g. to limit it inside an experiment's
        # init_trial(), without double bookkeeping.
        run.variable = run.trials[-1].variable + step
        if abs(step) == abs(run.min_step) and not run.start_measurement_idx:
            run.start_measurement_idx = len(run.trials)
            raise excpt.RunStartMeasurement


class AdaptAFCExperiment(AFCExperiment, AdaptExperiment):
    """Just combine/inherit the AFC-specific and the Adapt-specific methods""" 
    pass


class AFCSimulation(AFCExperiment):
    """This class is for an N-AFC experiment with model simulations, where
    a listener's response is replaced by a response calculated by an
    auditory model.
    """

    model_calculates_correct_wrong = True
    model_calculates_user_response = False

    def __init__(self):
        super().__init__()
        self.isSim = True

    def build_output_stimulus(self, trial):
        """This function from AFCExperiment is not needed here, because the
        simulation will not present an output signal acoustically.
        """
        pass

    def duration_to_zero_signal(self, obj):
        """This function from AFCExperiment is not needed here, because the
        simulation will not present an output signal acoustically.
        """
        pass

    def model_init_run(self, run):
        """This function needs to be implemented by the user as part of the
        own experiment and according to her/his model.  It will be
        called from within Model(Ui) once at the beginning of a run.
        """
        pass

    def model_simulation(self, run, trial):
        """This function needs to implemented by the user as part of the own
        experiment and according to her/his model.  It should return
        the model's answer as a result from class:Result.  It will be
        called from within Model(Ui) each time after the next trial
        has been generated.
        """
        pass


class AdaptAUCExperiment(AdaptAFCExperiment):
    
    """This class provides basic functions for an N-AUC experiment ('U' for
    'unforced').  Inherits the methods :func:`generate_trial` and
    :func:`build_output_stimulus` from AdaptAFCExperiment, and overwrites
    :func:`eval_response` and :func:`set_answer` to handle an 'unforced',
    i.e. uncertain answers.
    
    Attributes
    ----------
    num_afc : int
        number of intervals per trial
        :func:`build_output_stimulus` for further information

    """    

    task = "In which interval did you hear the test tone? Select '0' if undecided. "
    num_afc = 3 # default
    forced = False

    def __init__(self):
        super().__init__()
        # Add possible user_answer '0' for 'undecided' as first element, 
        # order required for buttons in GUI...
        self.user_answers = str(0) + self.user_answers

    def eval_response(self, run, trial, user_response):
        """Check and save the user answer to the current trial
        
        This method evaluates the subject's answer (being an interval number, 
        or '0' for 'undecided') by comparing it to the randomly chosen number 
        of the test interval. The result is saved to trial.result and the 
        trial is appended to the list of all trials of the current run.
        
        Parameters
        ----------
        user_response : string
            The user's reply/input
        
        Returns
        -------
        (none)

        Raises
        ------
        ValueError

        """

        trial.user_response = user_response

        # Catch forced answers first, which only works with debug enabled
        if user_response == 'v' and self.debug: # correct
            trial.result = Result.CORRECT
        elif self.debug and user_response == 'x': # wrong
            trial.result = Result.WRONG
        else: # check correctness
            user_response = int(user_response)
            if user_response == 0:
                trial.result = Result.UNDECIDED
            elif user_response == trial.correct_answer:
                trial.result = Result.CORRECT
            else:
                trial.result = Result.WRONG

        run.trials.append(trial)


class AdaptMatchingExperiment(AdaptExperiment):
    """This class provides basic functions for a matching experiment by specifying
    the methods :func:`build_output_stimulus` and :func:`eval_response`.
    
    
    Attributes
    ----------
    test_position : int
        Depending on test_position the test signal is 
        (0) presented on a random position,
        (1) the first signal
        (2) the second signal

    ui_layout : str
        defines the layout of the response buttons in the GUI:
        'v' vertical layout (e.g. up/down, default)
        'h' horizontal layout (e.g. left/right)
    """
    
    num_afc = 2
    test_position = 1 # default
    ui_blink_label = ['', '']
    ui_layout = 'v' 
    if ui_layout == 'v':
        ui_reply_label = ['up', 'down']
    else:
        ui_layout = ['left', 'right']

    def __init__(self):
        super().__init__()
        # Do this AFTER the user/experiment possibly specified the value exp.user_answers:
        # If the user/experiment did not specify its own user_answers ...
        if self.user_answers == '':
            # ... then set the possible user_answers to the default 'ud' (for 'Up' and 'Down')
            self.user_answers = 'ud'    # They get evaluated in eval_response()
    
    def correct_answer(self):
        """set correct answer for given trial

        depending on the current experiment type this method sets the right
        answer for each trial

        Returns
        --------
        correct_answer : string or int
            depending on experiment type return correct answer      
        """
        return 'd'
        
    def build_output_stimulus(self, trial):
        """Build output stimulus for a matching experiment
        
        This method prepares a list of test stimulus and reference stimulus.
        :class:`AdaptMatchingExperiment` for further information.      
                 
        Parameters
        ----------
        trial : :class:`Trial`
        
        Returns
        -------
        signals : list of numpy array
            list contains pre-signal, post-signal, isi-signal, reference
            signal and the test signal.  They appear in the order in which
            they are to be presented in the audio output.
        """

        signals = []
        signals.append(trial.pre_signal.copy())
        if self.test_position == 0:
            # Note that a randomly positioned test signal does only make
            # sense, when the subject can be instructed to identify the
            # different signals, e.g. in a task like this:
            # "The signal ON THE LEFT SIDE needs to go ... (up/down) in order
            # to have equal pitch as the signal on the right side"
            tmp_signals = [trial.test_signal, trial.reference_signal]
            random.shuffle(tmp_signals) 
            signals = signals + tmp_signals[0].copy() + trial.isi_signal.copy() + tmp_signals[1].copy()
        elif self.test_position == 1:
            signals = signals + [trial.test_signal.copy()] + [trial.isi_signal.copy()] + [trial.reference_signal.copy()]
        elif self.test_position == 2:
            signals = signals + [trial.reference_signal.copy()] + [trial.isi_signal.copy()] + [trial.test_signal.copy()]
        signals.append(trial.post_signal.copy())

        # Add background_signal if it is specified 
        if len(trial.background_signal) > 0:
            signals = self._add_background_signal(signals, trial.background_signal)

        # Apply (headphone) equalization filter if filter coefficients are specified
        if hasattr(self.eqfilter_b, '__len__') and hasattr(self.eqfilter_a, '__len__'):
            signals = self._equalisation_filter(signals)

        return signals

    def eval_response(self, run, trial, user_response):
        """Check and save the user answer to the current trial
        
        This method maps the user's anwer to a valid result. 

        The terminal UI (TUI) sends the configured `user_answers`, which by 
        default is set to 'u' or 'd' and mapped to 'wrong' or 'correct' 
        respectively. 

        For the graphical UI (GUI), the first element of `user_answers` is 
        assigned to the top/left and the second to the bottom/right button, 
        consequently, the buttons will send the corresponding reply. Note 
        that keyboard input also works with the GUI.   

        `eval_response()` needs to be overwritten in the setup of an experiment
        if you have different answers and/or different behaviour compared to
        the default. See adapt.py on how trial.result is being evaluated for
        adaptation.
        
        Parameters
        ----------
        user_response : string
        """

        trial.user_response = user_response
        if user_response in 'u':
            trial.result = Result.UP    # same as Result.WRONG
        elif user_response in 'd':
            trial.result = Result.DOWN  # same as Result.CORRECT
        
        run.trials.append(trial)
    

class ConstAFCExperiment(AdaptAFCExperiment):
    """This class provides the methods specific to an experiment with
    adaptive control of the variable.
    
    Constant stimulus experiments present pre-defined stimuli in random order,
    instead of adapting the variable based on previous responses. Instead of
    defining a starting value for the variable, all values that should be
    presented for the specified parameter(s) must be set in `init_experiment()`
    """
    
    def _generate_runs(self):
        """Generate runs for constant stimulus experiment
        
        Constant stimulus does not need adaptation class/settings, but we can 
        create a list with all variable values * repetitions here. 
        """

        combinations = self._combine_parameters()
        
        # Randomize order of runs
        if self.randomize_runs:
            random.shuffle(combinations)

        if not type(self.variable["start_val"]) is list:
            print(' For a Const.Stim.Exp. your variable value(s) must be a list,')
            print(f' but its type is {type(self.variable["start_val"])}.\n') 
        # Take care to pass a copy to the run, not the list itself!
        variables = self.variable["start_val"] * self.num_presentations

        for combi in combinations:
            params = {} 
            for param in combi[1:]:
                params.update(param)

            run = Run(self, params, variables.copy(), {'start_step': 0, 'type': 'ConstStim'}, 
                        self.reference_signal,
                        self.pre_signal, self.isi_signal,
                        self.post_signal, self.background_signal,
                        self.sample_rate, self.calib, self.task)
            
            self.init_run(run)
            self.duration_to_zero_signal(run)
            self.runs.append(run)

    def adapt(self, run):
        """There is no adaptation for a constant stimulus experiment.
        Therefore, the inherited adapt() is overwritten.  
        Here, we only check whether the run is finished.
        """

        if len(run.variable) < 1:
            run.finished = time.strftime('%d-%b-%Y__%H:%M:%S')
            raise excpt.RunFinishedException

    def generate_trial(self, run):
        """ generates trial

        This method generates a trial using the parameter set with its specific
        values given by the current run.        
        
        Parameters
        ----------
        run : :class:`Run`
            Reference on the Run, the next Trial is needed for.
        
        Returns
        -------
        trial : :class:`Trial`
            new Trial object with all parameters and variable values are set
            correctly
        """
        trial = Trial(run)
        trial.variable = run.variable.pop(random.randrange(len(run.variable)))
        return trial
