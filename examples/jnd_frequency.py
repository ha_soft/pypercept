#!/usr/bin/env python3

"""Measurement of frequency JND of sinusoids as a function of reference frequency

 Copyright (C) 2023  Martin Hansen
 Author :  Martin Hansen
 Date   :  11 May 2023
 Updated:  22 Aug 2023

 This file uses pypercept, a collection of functions for designing and
 controlling interactive psychoacoustical listening experiments.

 This file is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 2 of the License, 
 or (at your option) any later version.
 See the GNU General Public License for more details:
 http://www.gnu.org/licenses/gpl

"""

from pypercept.experiments import ConstAFCExperiment
from pypercept.order import Sequential
from pypercept.utils import gensin, hanwin, rms
import numpy as np
import pypercept

class JNDFrequency(ConstAFCExperiment, Sequential):

    def init_experiment(self, exp):
        exp.description = """
        This experiment measures the just noticeable difference of
        the frequency of two sinusoids, as a function of reference
        frequency and level of the tone, using a constant stimulus
        paradigm.
        """

        exp.set_variable("freq_incr", [1, 2, 4, 8, 16], "Cent", "relative frequency increment")
        exp.add_parameter("ref_freq",  [ 250, 500, 1000, 2000, 4000], "Hz", "frequency of reference tone")
        exp.add_parameter("tone_level",  [ -25, -50, -75], "dB FS", "level of both tones")

        exp.num_presentations = 10   # how often each variable value is presented
        exp.num_afc = 3
        exp.task = "In which interval did you hear a higher frequency (1,2,3)?"

        exp.feedback = True
        exp.debug = True
        #exp.debug_plot_signal = True

        exp.sample_rate = 48000
        
        isi_len = 0.25   # Inter stimulus interval length
        exp.isi_signal = np.zeros(round(isi_len * self.sample_rate))
        exp.post_signal = 0.1
        exp.pre_signal = 0.1


    def init_run(self, run):
        """Generate reference signal with reference frequency."""

        run.tone_dur = 0.5    # Duration of the tones [s]
        ramp_dur     = 0.05   # Duration of onset/offset ramp [s]
        run.ramp_n   = round(ramp_dur*run.sample_rate)  #  in samples
        
        # Generate the reference tone
        run.tone_ampl = np.sqrt(2)*10**(run.tone_level/20)
        run.reference_signal = gensin(run.ref_freq, run.tone_ampl, run.tone_dur, run.sample_rate)
        run.reference_signal = hanwin(run.reference_signal, run.ramp_n)
        
                                 
    def init_trial(self, trial, run):
        """Generate test signal with increased test frequency."""

        if trial.variable < 0:
            print('WARNING:    JND < 0.  Limited to 0 cent.')
            trial.variable = 0 

        # Generate the test tone with the desired frequency
        test_freq = run.ref_freq * 2**(trial.variable/1200)
        trial.test_signal = gensin(test_freq, run.tone_ampl, run.tone_dur, run.sample_rate)
        trial.test_signal = hanwin(trial.test_signal, run.ramp_n)
        
        
if __name__ == '__main__':
    pypercept.start(JNDFrequency) 
