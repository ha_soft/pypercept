#!/usr/bin/env python3

"""Measurement of intensity JND of sinusoids as a function of reference level and frequency

 Author :  Martin Hansen
 Date   :  22 Aug 2023

 This file uses pypercept, a collection of functions for designing and
 controlling interactive psychoacoustical listening experiments.

 This file is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 2 of the License, 
 or (at your option) any later version.
 See the GNU General Public License for more details:
 http://www.gnu.org/licenses/gpl

"""

from pypercept.experiments import AdaptAFCExperiment
from pypercept.order import Sequential
from pypercept.utils import gensin, hanwin, rms
import numpy as np
import pypercept

class LevelJND(AdaptAFCExperiment, Sequential):

    def init_experiment(self, exp):
        exp.description = """
        This experiment measures the just noticeable difference of
        the sound level of two sinusoids, as a function of reference
        level and frequency of the tone.
        """

        exp.set_variable("delta_level", 8, "dB", "level difference")
        exp.add_parameter("ref_level",  [ -20, -35, -50, -65], "dB FS", "level of reference tone")
        exp.add_parameter("tone_freq",  [ 250, 1000, 4000], "Hz", "frequency of both tones")

        exp.add_adapt_setting("1up2down", max_reversals=8, start_step=4, min_step=0.5)
        exp.num_afc = 3
        exp.task = "In which interval did you hear a higher sound level (1,2,3)?"

        exp.feedback = True
        exp.debug = True
        exp.debug_plot_signal = True

        exp.sample_rate = 48000
        
        isi_len = 0.25   # Inter stimulus interval length
        exp.isi_signal = np.zeros(round(isi_len * self.sample_rate))
        exp.post_signal = 0.1
        exp.pre_signal = 0.1


    def init_run(self, run):
        """Generate reference signal with reference level."""

        run.tone_dur = 0.5    # Duration of the tones [s]
        ramp_dur     = 0.05   # Duration of onset/offset ramp [s]
        run.ramp_n   = round(ramp_dur*run.sample_rate)  #  in samples
        
        # Generate the reference tone
        run.tone_ampl = np.sqrt(2)*10**(run.ref_level/20)
        run.reference_signal = gensin(run.tone_freq, run.tone_ampl, run.tone_dur, run.sample_rate)
        run.reference_signal = hanwin(run.reference_signal, run.ramp_n)


    def init_trial(self, trial, run):
        """Generate test signal with increased test level."""

        if trial.variable < 0:
            print('WARNING:    JND < 0.  Limited to 0 dB.')
            trial.variable = 0 

        # Generate the test tone with desired level by scaling the reference tone in level
        trial.test_signal = trial.reference_signal * 10**(trial.variable/20)
        
        
if __name__ == '__main__':
    pypercept.start(LevelJND) 
   

