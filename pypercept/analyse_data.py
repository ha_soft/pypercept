"""This file is part of pypercept:
a package for developing psychoacoustical listening experiments.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
"""

import json
import matplotlib.pyplot as plt
from matplotlib import container
import numpy as np
import os
import pandas as pd
import sys

class AnalyseData:
    '''Load, analyse and display experiment data acquired with pypercept. 

    Attributes
    ----------
    subject : str
        subject identifier (as given in the appropriate experiment)
    expname : str
        experiment name (experiment class name)
    num_params : int
        unique parameters of the experiment
    data : DataFrame
        raw data imported from teh appropriate json-file
    '''
    def __init__(self):
        pass

    def read_data(self, subject, verbose=True):
        '''Read the complete pypercept data stored in a json file into a
        Pandas DataFrame accessible as class attribute `data`.

        Attributes
        ----------
        subject : str
            subject identifier (as given in the appropriate experiment)
        '''
        self.subject = subject

        filename = f'pypercept_{subject}.json' 
        if os.path.exists(filename):
            try: 
                with open(filename) as f:
                    data = json.load(f)
            except:
                print(f'Apparently {filename} exists but cannot be read.')
                sys.exit()
        else: 
            print(f'File {filename} for subject {subject} does not exist.')
            sys.exit()

        # create pandas dataframe
        data = pd.json_normalize(data)

        # List of unique experiments
        exp_list = data['Experiment'].unique()
        if verbose:
            print(f'{filename} contains a total of {len(data)} runs from {len(exp_list)} experiment(s) ')
            print(f'   {exp_list}')
        self.data = data


    def load_data(self, subject, expname):
        '''Load pypercept data stored in a json file into a Pandas DataFrame 
        accessible as class attribute `data`.

        Attributes
        ----------
        subject : str
            subject identifier (as given in the appropriate experiment)
        expname : str
            experiment name (experiment class name)
        '''

        self.subject = subject
        self.expname = expname

        # filename = f'pypercept_{subject}.json' 
        # if os.path.exists(filename):
        #     try: 
        #         with open(filename) as f:
        #             data = json.load(f)
        #     except:
        #         print(f'Apparently {filename} exists but cannot be read.')
        #         sys.exit()
        # else: 
        #     print(f'File {filename} for subject {subject} does not exist.')
        #     sys.exit()

        # # create pandas dataframe
        # data = pd.json_normalize(data)
        # print(f'{filename} contains a total of {len(data)} runs, looking for {expname}...')

        self.read_data(subject, verbose=False)
        data = self.data
        #print(f'{filename} contains a total of {len(data)} runs, looking for {expname}...')

        # remove unrequested experiments
        data.drop(data.loc[data['Experiment'] != expname].index, inplace = True)
        num_exp = len(data)
        if num_exp == 0:
            print(f'Could not find any experiments matching {expname}.')
            #sys.exit()
            self.num_params = -1   ## FIX ME!
            return None
        print(f'Found {num_exp} runs matching experiment {expname}.')

        # re-index data frame
        data.reset_index(drop = True, inplace = True)

        # Drop columns with NaN entries.  This can happen, when data from
        # adaptive experiments and const. stim. experiment are mixed within
        # the same json data file
        data.dropna(axis=1, how='all', inplace=True)

        # get & validate number of parameters
        unique_params = data.NumParameters.unique()
        if not len(unique_params == 1):
            print('Number of parameters varies between runs of the same experiment!')
            sys.exit()
        self.num_params = unique_params[0]

        self.data = data


    def display_data(self, plot_style = 1, perm_params = 0, raw = False, fig_num = 102):
        '''Display pypercept experiment data. 

        Based on psylab's display_data and display_data_raw functions.   
        
        Parameters
        ----------
        plot_style : int 
            1:  plot all data into a single figure, for identical parameter 
                combinations, not all data will be shown (default).
            2:  create a figure for each unique value beyond 2 parameters.
        perm_params : int or list 
            permutate parameters
        raw : Boolean
            True: do not average data, i.e. only unique values are displayed (default).
            False: average unique value sets.
        fig_num : int (optional)
            Figure number into which to plot.  Default: 102

        Returns
        -------
        x : list
            averaged parameter values
        y : list
            averaged threshold
        yerr : list
            standard deviation of threshold
        '''  

        # multiple figures for parameter combinations won't work with a single parameter...
        if self.num_params == 1 & plot_style == 2:
            print('Single parameter, forcing plot_style=1')
            plot_style = 1

        # list to subset parameter values
        # default order, just in case 
        col_params = list(range(1, self.num_params+1))
        # check custom permutation
        if perm_params != 0:
            if len(perm_params) == self.num_params:
                col_params = perm_params
            else: 
                print('Parameter permutation list does not match number of actual parameters,')
                print('using orginal order.')

        axes = []
        
        # adaptive experiment
        if not 'Adaptation' in self.data:
        #if (self.data['Adaptation'][0] != 'ConstStim'): 

            # use psylab's approach and append a dummy parameter to single parameter
            # experiments to allow for a universal implementation, i.e. grouping 
            if self.num_params == 1: 
                self.data['Parameter 2.Value'] = 0
            if len(col_params) == 1:
                col_params.append(2)

            if raw:
        
                if plot_style == 1: # one figure 
                    # DO NOT drop duplicates in that subset (columns have to be in ascending order!)
                    ##df_plt = self.data.drop_duplicates(subset = [f'Parameter {p}.Value' for p in sorted(col_params)])
                    df_plt = self.data
                    fig = plt.figure(fig_num)
                    fig.clf()
                    ax = fig.subplots(1)
                    # group by 2nd parameter (0 for single parameter experiments) 
                    for name, group in df_plt.groupby([f'Parameter {p}.Value' for p in col_params[1:]]):
                        # errorbar plots in value-order, sort group by parameter for x-axis
                        group.sort_values(f'Parameter {col_params[0]}.Value', inplace = True) 
                        # plot data
                        ax.errorbar(x = group[f'Parameter {col_params[0]}.Value'], 
                                y = group['Threshold.Mean'], 
                                yerr = group['Threshold.Std. Deviation'],
                                fmt = 'o:', markerfacecolor='none', capsize = 3,
                                label = self.build_legend_label(group, col_params, 2))
                    axes.append(ax)
                    self.set_title(ax, group, col_params, 2)
                    if self.num_params > 1:
                        self.show_legend(ax, group)

                if plot_style == 2: # a figure for each uniqe set of variables
                    for name, group in self.data.groupby([f'Parameter {p}.Value' for p in col_params[1:]]):
                        # errorbar plots in value-order, sort group by parameter for x-axis
                        group.sort_values(f'Parameter {col_params[0]}.Value', inplace = True) 
                        # plot data
                        plt.figure()
                        ax = plt.subplot(1, 1, 1)
                        ax.errorbar(x = group[f'Parameter {col_params[0]}.Value'], 
                                y = group['Threshold.Mean'], 
                                yerr = group['Threshold.Std. Deviation'],
                                fmt = 'o', markerfacecolor='none', capsize = 3,
                                label = self.build_legend_label(group, col_params, 2))
                        axes.append(ax)
                        self.set_title(ax, group, col_params, 2)
                        if self.num_params > 1:
                            self.show_legend(ax, group)
                
            if not raw :
                
                # dataframe for averaged data
                df_avg = pd.DataFrame()

                if plot_style == 1: # one figure 
                    fig = plt.figure(fig_num)
                    fig.clf()
                    ax = fig.subplots(1)
                    for name, group in self.data.groupby([f'Parameter {p}.Value' for p in col_params[1:]]):
                        # errorbar plots in value-order, sort group by parameter for x-axis
                        group.sort_values(f'Parameter {col_params[0]}.Value', inplace = True) 
                        x, y, yerr, df = self.average_adaptive(group, col_params) 
                        # build new dataframe to contain averaged data along with the rest of the data.
                        df_avg = pd.concat([df_avg, df])
                        # plot data
                        ax.errorbar(x = x,  y = y,  yerr = yerr,
                                fmt = 'o:', markerfacecolor='none', capsize = 3, 
                                label = self.build_legend_label(group, col_params, 2))
                    axes.append(ax)
                    self.set_title(ax, group, col_params, 2)
                    self.show_legend(ax, group)

                if plot_style == 2: # a figure for each uniqe set of variables
                    for name, group in self.data.groupby([f'Parameter {p}.Value' for p in col_params[1:]]):
                        # errorbar plots in value-order, sort group by parameter for x-axis
                        group.sort_values(f'Parameter {col_params[0]}.Value', inplace = True) 
                        x, y, yerr, df = self.average_adaptive(group, col_params)
                        # build new dataframe to return averaged data along with the rest of the data.
                        df_avg = pd.concat([df_avg, df])
                        # plot data
                        plt.figure()
                        ax = plt.subplot(1, 1, 1)
                        ax.errorbar(x = x,  y = y,  yerr = yerr,
                                fmt = 'o:', markerfacecolor='none', capsize = 3, 
                                label = self.build_legend_label(group, col_params, 2))
                        axes.append(ax)
                        self.set_title(ax, group, col_params, 2)
                        self.show_legend(ax, group)

                self.data_processed = df_avg
                    
            # labels
            for ax in axes:
                ax.set_ylabel(f'{group["Variable.Name"].iloc[0]} in {group["Variable.Unit"].iloc[0]}')
                xlabel = f'{group["Parameter "+str(col_params[0])+".Name"].iloc[0]}'
                if not group["Parameter "+str(col_params[0])+".Unit"].iloc[0] == '':
                    xlabel += f' in {group["Parameter "+str(col_params[0])+".Unit"].iloc[0]}'
                ax.set_xlabel(xlabel)

        # endif adaptive experiment


        # constant stimulus experiment
        if 'Adaptation' in self.data:
        #if (self.data['Adaptation'][0] == 'ConstStim'): 

            # build new data frame for constant stimulus experiments; use variable as parameter 1
            df_cs = pd.DataFrame()
            for _, row in self.data.iterrows(): 
                df_tmp = pd.DataFrame()
                df_tmp['Parameter 1.Value'] = row['Results.Variable']
                df_tmp['Parameter 1.Prob'] = row['Results.Estim. prob_correct']
                df_tmp['Parameter 1.Std'] = row['Results.Std.Err. prob_correct']
                df_tmp['Parameter 1.Unit'] = row['Variable.Unit']

                # add other parameters
                for i in range(1, self.num_params+1):
                    df_tmp[f'Parameter {i+1}.Name'] = row[f'Parameter {i}.Name']
                    df_tmp[f'Parameter {i+1}.Value'] = row[f'Parameter {i}.Value']
                    df_tmp[f'Parameter {i+1}.Unit'] = row[f'Parameter {i}.Unit']

                # concatenate dataframes
                df_cs = pd.concat([df_cs, df_tmp])

            # add column to account for the new "parameter" 1 to the front, to 
            # maintain permutation, increase the others by one. 
            col_params = [i+1 for i in col_params]
            col_params.insert(0, 1)
            
            if raw:

                if plot_style == 1: # one figure 
                    # DO NOT drop duplicates in that subset  
                    ##df_plt = df_cs.drop_duplicates(subset = [f'Parameter {p}.Value' for p in sorted(col_params)[0:]])
                    df_plt = df_cs
                    fig = plt.figure(fig_num)
                    fig.clf()
                    ax = fig.subplots(1)
                    # group by 2nd parameter (0 for single parameter experiments) 
                    for name, group in df_plt.groupby([f'Parameter {p}.Value' for p in col_params[1:]]):
                        # plot data
                        ax.errorbar(x = group[f'Parameter 1.Value'], 
                                y = group[f'Parameter 1.Prob'] * 100, 
                                yerr = group[f'Parameter 1.Std'] * 100,
                                fmt = 'o:', markerfacecolor='none', capsize = 3, 
                                label = self.build_legend_label(group, col_params, 1))
                    axes.append(ax)
                    self.set_title(ax, group, col_params, 1)
                    self.show_legend(ax, group)

                if plot_style == 2: # a figure for each uniqe set of variables
                    for name, group in df_cs.groupby([f'Parameter {p}.Value' for p in col_params[1:]]):
                        # plot data
                        plt.figure()
                        ax = plt.subplot(1, 1, 1)
                        ax.errorbar(x = group[f'Parameter 1.Value'], 
                                y = group[f'Parameter 1.Prob'] * 100, 
                                yerr = group[f'Parameter 1.Std'] * 100,
                                fmt = 'o', markerfacecolor='none', capsize = 3,
                                label = self.build_legend_label(group, col_params, 1))
                        axes.append(ax)
                        self.set_title(ax, group, col_params, 1)
                        if self.num_params > 1:
                            self.show_legend(ax, group)

            if not raw: 

                # dataframe for averaged data
                df_avg = pd.DataFrame()

                if plot_style == 1: # one figure 
                    fig = plt.figure(fig_num)
                    fig.clf()
                    ax = fig.subplots(1)
                    # group parameters
                    for name, group in df_cs.groupby([f'Parameter {p}.Value' for p in col_params[1:]]):
                        x, y, yerr, df = self.average_constant_stimulus(group, col_params)
                        # build new dataframe to contain averaged data along with the rest of the data.
                        df_avg = pd.concat([df_avg, df])
                        # plot data
                        ax.errorbar(x = x, y = [val*100 for val in y], yerr = [val*100 for val in yerr],
                                fmt = 'o:', markerfacecolor='none', capsize = 3,
                                label = self.build_legend_label(group, col_params, 1))
                    axes.append(ax)
                    self.set_title(ax, group, col_params, 1)
                    self.show_legend(ax, group)

                if plot_style == 2: # a figure for each uniqe set of variables
                    for name, group in df_cs.groupby([f'Parameter {p}.Value' for p in col_params[1:]]):
                        x, y, yerr, df = self.average_constant_stimulus(group, col_params)
                        # build new dataframe to contain averaged data along with the rest of the data.
                        df_avg = pd.concat([df_avg, df])
                        # plot data
                        plt.figure()
                        ax = plt.subplot(1, 1, 1)
                        ax.errorbar(x = x, y = [val*100 for val in y], yerr = [val*100 for val in yerr],
                                fmt = 'o:', markerfacecolor='none', capsize = 3,
                                label = self.build_legend_label(group, col_params, 1))
                        axes.append(ax)
                        self.set_title(ax, group, col_params, 1)
                        self.show_legend(ax, group)

                self.data_processed = df_avg

            # labels
            for ax in axes:
                ax.set_ylabel('Percent correct')
                ax.set_xlabel(f'{self.data["Variable.Name"].iloc[0]} in {self.data["Variable.Unit"].iloc[0]}')

        # endif constant stimulus experiment

        # show figures
        plt.show()
        #return x, y, yerr


    def average_adaptive(self, group, col_params):
        '''Average data of identical parameter combinations (group) of an 
        adaptive experiment. 
        
        Parameters
        ----------
        group : DataFrameGroupBy 
            data of identical parameter combinations
        col_params : list
            parameter / column numbers
            
        Returns
        -------
        x : list
            averaged parameter values
        y : list
            averaged threshold
        err : list
            standard deviation of threshold
        df_avg : DataFrame
            group updated with averaged data
        '''    

        x = []
        y = []
        yerr = []
        df_avg = pd.DataFrame()
        # average data, grouped by parameter 1
        for name, group1 in group.groupby(f'Parameter {col_params[0]}.Value'):
            x.append(group1[f'Parameter {col_params[0]}.Value'].iloc[0])
            y.append(group1['Threshold.Mean'].mean())
            if len(group1) > 1:
                yerr.append(group1['Threshold.Mean'].std())
            else:
                # If only one data point, force its "deviation" to be zero
                yerr.append(0.0)
            group1['Threshold.Avg.Mean'] = y[-1]
            group1['Threshold.Avg.Std.Deviation'] = yerr[-1]
            df_avg = pd.concat([df_avg, group1])

        # replace group with updated version, i.e. averaged values (call by reference)
        return x, y, yerr, df_avg

    # average results of identical parameter combinations (group) of an constant stimulus experiment
    def average_constant_stimulus(self, group, col_params):
        '''Average data of identical parameter combinations (group) of an 
        constant stimulus experiment.
        
        Parameters
        ----------
        group : DataFrameGroupBy 
            data of identical parameter combinations
        col_params : list
            parameter / column numbers
            
        Returns
        -------
        x : list
            variable values
        y : list
            weighted mean of estimated probability of correct answer
        err : list
            estimated standard deviation of weighted mean
        df_avg : DataFrame
            group updated with averaged data
        '''  

        x = []
        y = []
        yerr = []
        df_avg = pd.DataFrame()
        # average data, grouped by parameter 1
        for name, group1 in group.groupby(f'Parameter {col_params[0]}.Value'):
            x.append(group1[f'Parameter {col_params[0]}.Value'].iloc[0])
            presentations = self.data['Results.Number of presentations'].iloc[0]
            total_presentations = len(group1) * presentations
            avg_prob_correct = sum(group1[f'Parameter {col_params[0]}.Prob'] 
                    * presentations / total_presentations)
            y.append(avg_prob_correct)
            yerr.append(np.sqrt(avg_prob_correct * (1-avg_prob_correct) / total_presentations))     
            group1['Avg.Estim. prob_correct'] = y[-1]
            group1['Avg.Std.Err. prob_correct'] = yerr[-1]
            df_avg = pd.concat([df_avg, group1])

        return x, y, yerr, df_avg


    def set_title(self, ax, group, col_params, idx):
        '''Set title of an axis to experiment subject and parameter. 
        Also show parameter name and units to give context to legend.
        
        Parameters
        ----------
        ax : matplotlib axis
        group : DataFrameGroupBy 
            data displayed in above axis
        col_params : list
            parameter / column numbers
        idx : int
            first parameter in title 
        '''  

        title = f'{self.expname}, {self.subject}'
        if self.num_params > 1:
            # 1st parameter is on x-axis
            for i in col_params[1:]:
                title += f', Par{idx}: {group["Parameter " + str(i) + ".Name"].iloc[0]}'
                if not group["Parameter " + str(i) + ".Unit"].iloc[0] == '':
                    title += f' in {group["Parameter " + str(i) + ".Unit"].iloc[0]}'
                idx += 1
        ax.set_title(title)
        

    def show_legend(self, ax, group):
        '''Show legend of an axis and remove errorbars from entries
        
        Parameters
        ----------
        ax : matplotlib axis
        group : DataFrameGroupBy 
            data displayed in above axis
        '''  

        # remove errorbars from legend entries https://stackoverflow.com/a/59877399
        handles, labels = ax.get_legend_handles_labels()
        handles = [h[0] if isinstance(h, container.ErrorbarContainer) else h for h in handles]
        ax.legend(handles, labels)


    def build_legend_label(self, group, col_params, idx):
        '''Build legend based on displayed parameters
        
        Parameters
        ----------
        ax : matplotlib axis
        group : DataFrameGroupBy 
            data displayed in above axis
        idx : int
            first parameter in title / legend 
        '''  
        legend_label = ''
        for i in col_params[1:]:
            legend_label += f'Par{idx}={group["Parameter " + str(i) + ".Value"].iloc[0]}  '
            idx += 1
        return legend_label


if __name__ == '__main__':
    
    if len(sys.argv) < 3:
        print('Usage: python analyse_data.py <subject> <experiment>')
        sys.exit()

    subject = sys.argv[1]
    expname = sys.argv[2]

    ad = AnalyseData()
    ad.load_data(subject, expname)
    
    # At this point, ad.data contains the requested experiment's data as pandas dataframe.
    # Notebooks are a nice way to explore this since they come with a neat browser for dataframes. 

    # mimic psylab's display_data output
    ad.display_data(plot_style=1, raw = True)
