"""This file is part of pypercept:
a package for developing psychoacoustical listening experiments.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
"""

"""This module contains several adaptive methods to change the current
stimulus variable depending on the previous answer(s) of the subject
"""

from enum import Enum
from pypercept.result import Result
import pypercept.exception as exp


class Adapt():
    def __init__(self, parameters):
        self.__dict__.update(parameters)
        self.init_adapt()

    def init_adapt(self):
        pass

    def adapt(self, trials):
        pass

    def __call__(self, trials):
        return self.adapt(trials)


class Direction(Enum):
    UP = 1
    DOWN = 2
    STAY = 3


class Adapt1up1down(Adapt):

    reversals = 0

    def adapt(self, trials: list) -> None:
        """
        Updates the reversal variable self.reversals and the stepsize
        variable self.step according to the adaptive 1up-1down paradigm.
        (Levitt, "Transformed up-down procedures in psychoacoustics",
        1971, JASA 49, p.467-477.)  

        Parameters
        ----------
        trials : List with all trials

        Returns
        -------
        self.step : float
        
        Raises
        ------
        RunFinishException
        """

        #-----------------
        # detect reversals
        #-----------------
        # During the measurement phase, the reversals get counted,
        # as they can serve as a stop criterion for a run. 
        #-----------------
        if len(trials) >= 2:
            # check for upper reversal
            # last answer was correct and the one before that was wrong
            if trials[-1].result == Result.CORRECT and trials[-2].result == Result.WRONG:
                if self.step == self.min_step:
                    self.reversals += 1
                # adjust step size at upper reversal
                if not self.reversed_up_down:
                    self.step = max(self.min_step, self.step/2)
                else:
                    self.step = min(self.min_step, self.step/2)

            # check for lower reversal
            # last answer was wrong and the one before that was correct
            if trials[-1].result == Result.WRONG and trials[-2].result == Result.CORRECT:
                if self.step == self.min_step:
                    self.reversals += 1
                # DO NOT adapt step size, as this is only done at upper reversals

            # check for end of run
            if self.reversals == self.max_reversals:
                raise exp.RunFinishedException()

        # -------------
        # adaptive rule
        # -------------
        # Sets the step based on last answer(s).  The step will then be used
        # to update the variable like this:    variable += step
        # -------------
        # If the last answer was correct, go DOWN using a negative step,
        # i.e. the variable will get decreased.  And vice versa.
        if trials[-1].result == Result.CORRECT:
            return -self.step
        else:
            return self.step


class Adapt1up2down(Adapt):

    reversals = 0
    direction = Direction.STAY

    def adapt(self, trials):
        """
        Updates the reversal variable self.reversals and the stepsize
        variable self.step according to the adaptive 1up-2down paradigm.
        (Levitt, "Transformed up-down procedures in psychoacoustics",
        1971, JASA 49, p.467-477.)

        Parameters
        ----------
        trials : List with all trials

        Returns
        -------
        self.step : float
        
        Raises
        ------
        RunFinishException
        """

        #-----------------
        # detect reversals
        # ----------------
        # During the measurement phase, the reversals get counted,
        # as they can serve as a stop criterion for a run. 
        #-----------------
        if len(trials) >= 3:
            # check for upper reversal
            if trials[-1].result == Result.CORRECT and trials[-2].result == Result.CORRECT \
                and trials[-3].result == Result.WRONG:
                if self.step == self.min_step:
                    self.reversals += 1
                # adjust step size at upper reversal
                if not self.reversed_up_down:
                    self.step = max(self.min_step, self.step/2)
                else:
                    self.step = min(self.min_step, self.step/2)

            # check for lower reversal
            if trials[-1].result == Result.WRONG \
                and trials[-2].result == Result.CORRECT and trials[-3].result == Result.CORRECT:
                if self.step == self.min_step:
                    self.reversals += 1
                # DO NOT adapt step size, as this is only done at upper reversals

            # check for end of run
            if self.reversals == self.max_reversals:
                raise exp.RunFinishedException()
            
        # -------------
        # adaptive rule
        # -------------
        # Sets the step based on last answer(s).  The step will then be used
        # to update the variable like this:    variable += step
        # -------------
        # If the last answer was wrong,
        # go UP using a positive step, i.e. the variable will get increased.
        if trials[-1].result == Result.WRONG:
            self.direction = Direction.UP
            return self.step
        # If the last two answers were correct (2down) and direction is 'STAY',
        # go DOWN using a negative step, i.e. the variable will get decreased.
        if len(trials) >= 2 \
            and trials[-1].result== Result.CORRECT and trials[-2].result == Result.CORRECT \
            and self.direction == Direction.STAY:
            self.direction = Direction.DOWN
            return -self.step
        # Otherwise the step is 0, i.e. stay at the current variable.  
        self.direction = Direction.STAY
        return 0


class Adapt2up1down(Adapt):

    reversals = 0
    direction = Direction.STAY

    def adapt(self, trials):
        """
        Updates the reversal variable self.reversals and the stepsize
        variable self.step according to the adaptive 2up-1down paradigm.
        (Levitt, "Transformed up-down procedures in psychoacoustics",
        1971, JASA 49, p.467-477.)

        Parameters
        ----------
        trials : List with all trials
        
        Returns
        -------
        self.step : float

        Raises
        ------
        RunFinishException
        """

        #-----------------
        # detect reversals
        #-----------------
        # During the measurement phase, the reversals get counted,
        # as they can serve as a stop criterion for a run. 
        #-----------------
        if len(trials) >= 3:
            # check for upper reversal
            if trials[-2].result == Result.WRONG and trials[-3].result == Result.WRONG \
                and trials[-1].result == Result.CORRECT:
                if self.step == self.min_step:
                    self.reversals += 1
                # adjust step size at upper reversal
                if not self.reversed_up_down:
                    self.step = max(self.min_step, self.step/2)
                else:
                    self.step = min(self.min_step, self.step/2)

            # check for lower reversal
            if trials[-3].result == Result.CORRECT \
                and trials[-1].result == Result.WRONG and trials[-2].result == Result.WRONG:
                if self.step == self.min_step:
                    self.reversals += 1
                # DO NOT adapt step size, as this is only done at upper reversals

            # check for end of run
            if self.reversals == self.max_reversals:
                raise exp.RunFinishedException()

        # -------------
        # adaptive rule
        # -------------
        # Sets the step based on last answer(s).  The step will then be used
        # to update the variable like this:    variable += step
        # -------------
        # If the last answer was correct,
        # go DOWN using a negative step, i.e. the variable will get decreased.
        if trials[-1].result == Result.CORRECT:
            self.direction = Direction.DOWN
            return -self.step
        # If the last two answers were wrong and direction is 'STAY',
        # go UP using a positive step, i.e. the variable will get increased.
        if len(trials) >= 2 \
          and trials[-1].result == Result.WRONG and trials[-2].result == Result.WRONG \
          and self.direction == Direction.STAY:
            self.direction = Direction.UP
            return self.step
        # Otherwise the step is 0, i.e. stay at the current variable.  
        self.direction = Direction.STAY
        return 0


class Adapt1up3down(Adapt):

    reversals = 0
    direction = [Direction.STAY, Direction.STAY]

    def adapt(self, trials):
        """
        Updates the reversal variable self.reversals and the stepsize
        variable self.step according to the adaptive 1up-3down paradigm.
        (Levitt, "Transformed up-down procedures in psychoacoustics",
        1971, JASA 49, p.467-477.)

        Parameters
        ----------
        trials : List with all trials

        Returns
        -------
        self.step : float
        
        Raises
        ------
        RunFinishException
        """

        #-----------------
        # detect reversals
        #-----------------
        # During the measurement phase, the reversals get counted,
        # as they can serve as a stop criterion for a run. 
        #-----------------
        if len(trials) >= 4:
            # check for upper reversal
            # last 3 answers were correct and the one before that was wrong
            if trials[-1].result == Result.CORRECT and trials[-2].result == Result.CORRECT and trials[-3].result == Result.CORRECT \
              and trials[-4].result == Result.WRONG:
                if self.step == self.min_step:
                    self.reversals += 1
                # adjust step size at upper reversal
                if not self.reversed_up_down:
                    self.step = max(self.min_step, self.step/2)
                else:
                    self.step = min(self.min_step, self.step/2)

            # check for lower reversal
            # last answer was wrong and the 3 answers before that were correct 
            if trials[-1].result == Result.WRONG \
              and trials[-2].result == Result.CORRECT and trials[-3].result == Result.CORRECT and trials[-4].result == Result.CORRECT:
                if self.step == self.min_step:
                    self.reversals += 1
                # DO NOT adapt step size, as this is only done at upper reversals

            # check for end of run
            if self.reversals == self.max_reversals:
                raise exp.RunFinishedException()

        # -------------
        # adaptive rule
        # -------------
        # Sets the step based on last answer(s).  The step will then be used
        # to update the variable like this:    variable += step
        # -------------
        # If the last answer was wrong,
        # go UP using a positive step, i.e. the variable will get increased.
        if trials[-1].result == Result.WRONG:
            self.direction[1] = Direction.UP
            return self.step
        # If the last three answers were correct (3down) and direction is 'STAY',
        # go DOWN using a negative step, i.e. the variable will get decreased.
        if len(trials) >= 3 \
          and trials[-1].result == Result.CORRECT and trials[-2].result == Result.CORRECT and trials[-3].result == Result.CORRECT \
          and self.direction[0] == Direction.STAY and self.direction[1] == Direction.STAY:
            self.direction[1] = Direction.DOWN
            return -self.step
        # Otherwise the step is 0, i.e. stay at the current variable.  
        self.direction.pop(0)
        self.direction.append(Direction.STAY)
        return 0


class AdaptWUD(Adapt):

    reversals = 0

    def adapt(self, trials):
        """
        Updates the reversal variable self.reversals and the stepsize
        variable self.step according to the "weighted up-down" paradigm.
        (Kaernbach, "Simple adaptive testing with the weighted up-down
        method", 1991, Perception & Psychophysics, 49, p. 227-229)

        exp.add_adapt_setting() needs keyword argument pc_convergence set ([0..1[)  

        Parameters
        ----------
        trials : List with all trials

        Returns
        -------
        self.step : float
        
        Raises
        ------
        RunFinishException
        """

        # Check reasonable value of the desired percent correct at convergence:
        # it must be below 1 (100%) and above chance level (1/n_choices)
        if self.pc_convergence >= 1 or self.pc_convergence <= 1.0/self.exp.num_afc:
            return  # TODO: Throw proper error

        #-----------------
        # detect reversals
        #-----------------
        # During the measurement phase, the reversals get counted,
        # as they can serve as a stop criterion for a run. 
        #-----------------
        if len(trials) >= 2:
            # check for upper reversal: last answer was correct, the one before that wrong
            if (trials[-1].result == Result.CORRECT and trials[-2].result == Result.WRONG):
                if self.step == self.min_step:
                    self.reversals += 1
                # adjust step size at upper reversal
                if not self.reversed_up_down:
                    self.step = max(self.min_step, self.step/2)
                else:
                    self.step = min(self.min_step, self.step/2)
                
            # check for lower reversal
            if (trials[-1].result == Result.WRONG and trials[-2].result == Result.CORRECT):
                if self.step == self.min_step:
                    self.reversals += 1
                # DO NOT adapt step size, as this is only done at upper reversals

            # check for end of run
            if self.reversals == self.max_reversals:
                raise exp.RunFinishedException()
            
        # -------------
        # adaptive rule
        # -------------
        # Sets the step based on last answer(s).  The step will then be used
        # to update the variable like this:    variable += step
        # -------------

        # variable self.step is defined to hold the step size step_down for going down:
        step_down = self.step
        
        # now use equation (1) from  Kaernbach (1991), (e.g. at 
        # https://link.springer.com/content/pdf/10.3758/BF03214307.pdf
        # to calculate new step size step_up for going up.   
        #
        # However, NOTE that that equation (1) and also the sentence following
        # immediately contains a MAJOR TYPO.  Quote from Kaernbach (1991):
        #   --- QUOTE BEGIN ---
        #       S_up * p = S_down * (1-p).               (eq. 1)
        #   For X_75, it   follows   that   S_up/S_down = 1/3.   
        #   --- QUOTE END ---
        # That above equation is WRONG!  The sentence following it ("For X_75, ...")
        # matches to the equation, which is wrong, however.
        #
        # The next sentence then, in the original paper, gives a CORRECT EXAMPLE
        #   --- QUOTE BEGIN ---
        #   The rule for a convergence to the X_75 point would thus read: 
        #   Decrease the Level 1 step after each correct response, and
        #   increase it 3 steps  after each incorrect response. 
        #   --- QUOTE END ---
        #
        # So the correct equilibrium condition for convergence is:
        #   stepsize_UP * probability_UP = stepsize_DOWN * probability_DOWN
        #   where probability_DOWN  = probability for a correct answer:
        #   
        step_up = self.pc_convergence/(1-self.pc_convergence) * step_down

        # If the last answer was correct, go DOWN using a negative step,
        # i.e. the variable will get decreased.  
        if trials[-1].result == Result.WRONG:
            return step_up
        else:
            return -step_down


class AdaptUWUD(Adapt):

    reversals = 0

    def adapt(self, trials):
        """
        Updates the reversal variable self.reversals and the stepsize
        variable self.step according to the "unforced weighted up-down" 
        paradigm. 
        (Kaernbach, "Adaptive threshold estimation with unforced-choice tasks", 
        2001, Perception & Psychophysics, 63(8), p. 1377-1388)   

        exp.add_adapt_setting() needs keyword argument pc_convergence set ([0..1[)  

        Parameters
        ----------
        trials : List with all trials

        Returns
        -------
        self.step : float
        
        Raises
        ------
        RunFinishException
        """

        # Check reasonable value of the desired percent correct at convergence:
        # it must be below 1 (100%) and above chance level (1/n_choices)
        if self.pc_convergence >= 1 or self.pc_convergence <= 1.0/self.exp.num_afc:
            return  # TODO: Throw proper error

        #-----------------
        # detect reversals
        #-----------------
        # During the measurement phase, the reversals get counted,
        # as they can serve as a stop criterion for a run. 
        #-----------------
        if len(trials) >= 2:
            # check for upper reversal: last answer was correct, 
            # the one before that wrong or undecided (i.e. not correct)
            if (trials[-1].result == Result.CORRECT and not trials[-2].result == Result.CORRECT):
                if self.step == self.min_step:
                    self.reversals += 1
                # adjust step size at upper reversal
                if not self.reversed_up_down:
                    self.step = max(self.min_step, self.step/2)
                else:
                    self.step = min(self.min_step, self.step/2)
                
            # check for lower reversal: last answer was wrong or undecided 
            # (i.e. not correct) and the one before that was correct
            if (not trials[-1].result == Result.CORRECT and trials[-2].result == Result.CORRECT):
                if self.step == self.min_step:
                    self.reversals += 1
                # DO NOT adapt step size, as this is only done at upper reversals

            # check for end of run
            if self.reversals == self.max_reversals:
                raise exp.RunFinishedException()
            
        # ------------------
        # adaptive rule UWUD
        # ------------------
        # Sets the step based on last answer(s).  The step will then be used
        # to update the variable like this:    variable += step
        # calculate separate step sizes for
        #  - correct answer -->  going down,
        #  - wrong answer --> going up, and 
        #  - unsure ("I don't know") answer --> going up as well, but smaller step

        # ------------------
        step_down = self.step
        # use equation (5) from  Kaernbach (2001) to calculate new step size
        # S_incorrect for going up
        step_up = self.pc_convergence/(1-self.pc_convergence) * step_down
        # use equation (7) from  Kaernbach (2001) to calculate new step size
        # S_unsure for going up. 
        step_unsure = (self.pc_convergence-1/self.exp.num_afc) / (1-self.pc_convergence) * step_down

        # If the last answer was correct, go DOWN using a negative step,
        # i.e. the variable will get decreased.  
        if trials[-1].result == Result.UNDECIDED:
            return step_unsure
        elif trials[-1].result == Result.CORRECT:
            return -step_down
        elif trials[-1].result == Result.WRONG:
            return step_up
        else:
            return # this should be caught by experiments.AdaptAUCExperiment()
