#!/bin/bash

a=`awk '$1 == "version" {print $3}' setup.py | sed 's/",*//g'`
b=`awk '$1 == "VERSION" {print $3}' Makefile`

if [ $a != $b ]; then
    echo "Error: version in setup.py: '"$a"' and version in Makefile: '"$b"' are not equal."
    exit 1
fi

# End of file
